#include "tweet_widget.h"

#include <Qt>
#include <QPixmap>
#include <QSizePolicy>
#include <QPainter>
#include <QPen>
#include <QColor>
#include <QHBoxLayout>
#include <QPalette>
#include <QGuiApplication>
#include <QClipboard>
#include <QCursor>
#include <QLayoutItem>
#include <QRegularExpression>
#include <QDesktopServices>
#include <QMenu>
#include <QIcon>

#include "../models/tweet.h"
#include "../models/_db.h"
#include "../lib/global_state.h"

#include "./colors.h"
#include "./author_info.h"
#include "./utils/clickable_entities.h"

#include "./components/image_widget.h"


TweetWidget::TweetWidget(QWidget* parent, shared_ptr<Tweet> t, Column* column, shared_ptr<Retweet> rt, int nesting_limit): QWidget(parent) {
    this->t = t;
    this->column = column;
    this->rt = rt;
    this->nesting_limit = nesting_limit;

    setCursor(Qt::PointingHandCursor);

    layout = new QVBoxLayout(this);
    layout->setContentsMargins(20, rt == nullptr ? 20 : 10, 20, 0);

    QFont font("Titillium Web");
    font.setPixelSize(14);
    setFont(font);

    if (rt != nullptr) {
        QHBoxLayout* retweet_bar_layout = new QHBoxLayout();
        layout->addLayout(retweet_bar_layout);
        retweet_bar_layout->setContentsMargins(80,0,11,0);

        retweeted_icon = new QLabel(this);
        retweeted_icon->setPixmap(QPixmap(":/icons/retweet_icon.png"));
        retweeted_icon->setFixedSize(20, 20);
        retweeted_icon->setScaledContents(true);
        retweet_bar_layout->addWidget(retweeted_icon);

        retweet_label = new QLabel(this);
        retweet_label->setText("Retweeted by " + rt->get_user()->display_name);
        retweet_bar_layout->addWidget(retweet_label);
    }

    header_layout = new QHBoxLayout();
    layout->addLayout(header_layout);
    header_layout->setSpacing(20);

    author_info_panel = new AuthorInfo(this, t->get_user());
    header_layout->addWidget(author_info_panel);

    // Create, style, and add the "Replying to" text
    if (t->in_reply_to_id != 0) {
        try {
            in_reply_to_label = new QLabel(this);
            in_reply_to_label->setText(get_rich_text_for(t->get_replying_to_text()));
            in_reply_to_label->setCursor(Qt::ArrowCursor);
            in_reply_to_label->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
            in_reply_to_label->setContextMenuPolicy(Qt::NoContextMenu);

            in_reply_to_label->setWordWrap(true);
            in_reply_to_label->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred));

            QFont smaller_font = QFont(font);
            smaller_font.setPixelSize(14);
            in_reply_to_label->setFont(smaller_font);

            QPalette palette = in_reply_to_label->palette();
            palette.setColor(QPalette::WindowText, COLOR_TWITTER_TEXT_GRAY);
            in_reply_to_label->setPalette(palette);

            connect(in_reply_to_label, SIGNAL(linkActivated(QString)), column, SLOT(entity_clicked(QString)));

            header_layout->addWidget(in_reply_to_label);
        } catch (DBException &e) {
            printf("Replied tweet is missing: %ld!\n", t->in_reply_to_id);
        }
    } else {
        header_layout->addStretch(1);
    }

    posted_at_label = new QLabel(this);
    posted_at_label->setText(t->render_posted_at());
    font.setPixelSize(12);
    posted_at_label->setFont(font);
    QPalette pal = posted_at_label->palette();
    pal.setColor(QPalette::WindowText, COLOR_TWITTER_TEXT_GRAY);
    posted_at_label->setPalette(pal);
    header_layout->addWidget(posted_at_label);


    body_layout = new QVBoxLayout();
    body_layout->setContentsMargins(80, 0, 11, 11);
    layout->addLayout(body_layout);

    if (t->tombstone_type != 0) {
        tombstone_label = new Tombstone(this, t);
        body_layout->addWidget(tombstone_label);
    }

    text_label = new QLabel(this);
    if (QRegularExpression("@\\w+").match(t->text).hasMatch() || t->hashtags.size() > 0) {
        text_label->setText(get_rich_text_for(t->text));
        text_label->setTextFormat(Qt::RichText);

        connect(text_label, SIGNAL(linkActivated(QString)), column, SLOT(entity_clicked(QString)));
    } else {
        text_label->setText(t->text);
    }
    text_label->setWordWrap(true);
    text_label->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::LinksAccessibleByMouse);
    text_label->setCursor(Qt::ArrowCursor);
    text_label->setContextMenuPolicy(Qt::NoContextMenu);
    if (is_focused) {
        font.setPixelSize(20);
        text_label->setFont(font);
    }
    body_layout->addWidget(text_label);

    if (!t->is_content_downloaded && (t->images.size() > 0 || t->videos.size() > 0)) {  // TODO: what about url thumbnails?
        content_not_downloaded_label = new QLabel(this);
        content_not_downloaded_label->setText("(!) Tweet has content which is not downloaded");
        font.setPixelSize(12);
        content_not_downloaded_label->setFont(font);

        QPalette palette = content_not_downloaded_label->palette();
        palette.setColor(QPalette::WindowText, COLOR_RED);
        content_not_downloaded_label->setPalette(palette);
        body_layout->addWidget(content_not_downloaded_label);
    }


    // Contents
    // --------

    for (Url u: t->urls) {
        embedded_link = new EmbeddedLink(this, make_shared<Url>(u));
        body_layout->addWidget(embedded_link);
    }

    for (Image i: t->images) {
        if (!i.is_downloaded) {
            printf("!!Image not downloaded in tweet: %s\n", t->get_permalink().toStdString().c_str());
            continue;
        }
        ImageWidget* img_widget = new ImageWidget(this, GlobalState::PROFILE_DIR + i.get_filepath());
        img_widget->setMinimumWidth(400);
        img_widget->setMaximumWidth(900);
        img_widget->setMaximumHeight(450);
        body_layout->addWidget(img_widget);
    }

    for (Video v: t->videos) {
        if (!v.is_downloaded) {
            printf("!!Video not downloaded in tweet: %s\n", t->get_permalink().toStdString().c_str());
            continue;
        }
        video_preview = new VideoPreview(this, make_shared<Video>(v));
        body_layout->addWidget(video_preview);
    }

    for (Poll p: t->polls) {
        poll_widget = new PollWidget(this, make_shared<Poll>(p));
        body_layout->addWidget(poll_widget);
    }

    if (t->space != nullptr) {
        space_widget = new SpaceWidget(this, t->space);
        body_layout->addWidget(space_widget);
    }

    if (t->quoted_tweet_id != 0 && nesting_limit > 0) {
        try {
            shared_ptr<Tweet> quoted = t->get_quoted_tweet();
            quoted_tweet_widget = new QuotedTweet(this, quoted, column, nesting_limit - 1);
            body_layout->addWidget(quoted_tweet_widget);
        } catch (DBException &e) {
            printf("Quoted tweet is missing: %ld!\n", t->quoted_tweet_id);
        }
    }

    interactions_bar = new InteractionsBar(this, t);
    connect(interactions_bar, SIGNAL(clicked()), this, SLOT(copy_link()));
    body_layout->addWidget(interactions_bar);

    if (column != nullptr) {
        connect(this, SIGNAL(clicked(shared_ptr<Tweet>)), column, SLOT(open_tweet(shared_ptr<Tweet>)));
        connect(author_info_panel, SIGNAL(profile_image_clicked(shared_ptr<User>)), column, SLOT(open_user_feed(shared_ptr<User>)));
    }
}


// Event handlers
// --------------

void TweetWidget::paintEvent(QPaintEvent*) {
    QPainter painter(this);
    QPen pen(COLOR_OUTLINE_GRAY);
    painter.setPen(pen);

    const int width_margin = 0;
    if (has_divider) {
        // Draw line at the bottom
        painter.drawLine(width_margin, height() - 1, width() - width_margin, height() - 1);
    }

    if (has_vertical_reply_line) {
        // Draw vertical line down side of contents
        pen.setWidth(3);
        painter.setPen(pen);

        const int x = 51;
        painter.drawLine(x, 70 + layout->contentsMargins().top(), x, height()-1 - 10);
    }
}

void TweetWidget::mousePressEvent(QMouseEvent* e) {
    if (e->button() == Qt::LeftButton) {
        if (!is_focused) {  // No need to re-open the currently focused Tweet
            emit clicked(t);
        }
    } else {
        e->ignore();  // Propagate it
    }
}

void TweetWidget::contextMenuEvent(QContextMenuEvent* e) {
    // DUPE tweet-context-menu
    QMenu menu(this);
    menu.addAction(QIcon(":/icons/link_icon.png"), "Copy tweet URL", this, SLOT(copy_link()));
    menu.addSeparator();
    menu.addAction("Download contents", this, SLOT(download_contents()));
    menu.addAction("Download this conversation", this, SLOT(download_conversation()));
    menu.addSeparator();
    menu.addAction(QIcon(":/icons/open_external_icon.png"), "Open tweet in browser", this, SLOT(open_in_browser()));
    menu.exec(e->globalPos());
}


// Callbacks
// ---------

/**
 * Copy the link to this tweet
 */
void TweetWidget::copy_link() {
    QString permalink = t->get_permalink();
    printf("Copying to clipboard: %s\n", permalink.toStdString().c_str());
    QGuiApplication::clipboard()->setText(permalink);
}

/**
 * Open this tweet in a web browser
 */
void TweetWidget::open_in_browser() {
    QDesktopServices::openUrl(QUrl(t->get_permalink()));
}

/**
 * Callback for the "Download" button.  Not sure how useful this is right now (when would you use it?)
 */
void TweetWidget::download_conversation() {
    QCursor tmp = cursor();
    setCursor(Qt::WaitCursor);
    int result = t->download_conversation();
    printf("Result: %d\n", result);  // TODO: if (result != 0) {...}
    setCursor(tmp);

    if (t->in_reply_to_id != 0) {
        // TODO tweet-refresh: do all of this in a `refresh()` method
        try {
            in_reply_to_label->setText(t->get_replying_to_text());
        } catch (DBException &e) {
            printf("Failed to download the parent tweet for some reason!\n");
        }
    }
}

void TweetWidget::download_contents() {
    QCursor tmp = cursor();
    setCursor(Qt::WaitCursor);
    int result = t->download_contents();
    printf("Result: %d\n", result);  // TODO: if (result != 0) {...}
    setCursor(tmp);

    if (result == 0) {
        // TODO tweet-refresh: do all of this in a `refresh()` method

        if (content_not_downloaded_label != nullptr) {
            body_layout->removeWidget(content_not_downloaded_label);
            delete content_not_downloaded_label;
            content_not_downloaded_label = nullptr;
        }

        for (Url u: t->urls) {
            if (!u.has_card) {
                continue;
            }

            EmbeddedLink* new_embedded_link = new EmbeddedLink(this, make_shared<Url>(u));
            QLayoutItem* i = body_layout->replaceWidget(embedded_link, new_embedded_link);
            delete i;
            break;
        }
    }
}


// Setters
// -------

void TweetWidget::set_is_focused(bool is_focused) {
    this->is_focused = is_focused;

    // Update font size
    QFont font = this->font();
    font.setPixelSize(is_focused ? 20 : 16);
    text_label->setFont(font);

    // Update left margin
    body_layout->setContentsMargins(is_focused ? 11 : 100, 0, 11, 11);

    // Don't use a pointer cursor
    setCursor(is_focused ? Qt::ArrowCursor : Qt::PointingHandCursor);

    update();
}

void TweetWidget::set_has_divider(bool has_divider) {
    this->has_divider = has_divider;
    update();
}

void TweetWidget::set_has_vertical_reply_line(bool has_vertical_reply_line) {
    this->has_vertical_reply_line = has_vertical_reply_line;
    update();
}

void TweetWidget::set_vertical_spacing(bool has_spacing_top, bool has_spacing_bottom) {
    layout->setContentsMargins(20, has_spacing_top ? 20 : 0, 20, has_spacing_bottom ? 20 : 0);
}

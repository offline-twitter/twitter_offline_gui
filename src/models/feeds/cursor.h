#ifndef FEED_CURSOR_H
#define FEED_CURSOR_H

#include "./search.h"
#include "./feed.h"


/**
 * Search, Sort and Filter.
 *
 * - Search: text content to search for
 * - Sort: what order to return results in
 * - Filter: other requirements for the results to match
 *
 * The difference between "search" and "filter" is fuzzy; I'm using "search" as text-search and "filter"
 * for other flags like author or minimum num_likes, etc.
 */
namespace SSF {
    typedef QString SortOrder;

    /**
     * Position in the feed (i.e., whether scrolling up/down is possible)
     */
    enum class CursorType {
       START,  // This is the top of the feed; `cursor_position` is invalid;
       MIDDLE, // `cursor_position` indicates what should be on the next page;
       END     // Bottom of the feed has been reached.  Subsequent pages will all be empty
    };

    const SortOrder SORT_ORDER_NEWEST = "Newest";
    const SortOrder SORT_ORDER_OLDEST = "Oldest";
    const SortOrder SORT_ORDER_MOST_LIKES = "Most likes";
    const SortOrder SORT_ORDER_MOST_RETWEETS = "Most retweets";


    /**
     * Cursor-based pagination with search, sort and filtering.
     */
    class Cursor {
     public:
        SortOrder sort_order = SSF::SORT_ORDER_NEWEST;
        SearchParams search_params;

        /**
         * By default a cursor is at the START of the feed.
         */
        CursorType cursor_type = CursorType::START;

        /**
         * Measured in whatever the sort order is. e.g.:
         * - SORT_ORDER_NEWEST => timestamp of last tweet
         * - SORT_ORDER_MOST_LIKES => num_likes of last tweet
         */
        int cursor_position;

        /**
         * Default to 50 per page.  Maybe there will be some way to change this later
         */
        int page_size = 50;

        Feed get_next_page() const;
    };
}  // namespace SSF

#endif  // FEED_CURSOR_H

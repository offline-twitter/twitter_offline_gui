#ifndef MODELS_SEARCH_H
#define MODELS_SEARCH_H

#include <QString>
#include <QStringList>

#include <vector>

#include "../tweet.h"
#include "../typedefs.h"


using std::vector;

class SearchParams {
 public:
    static SearchParams parse_search_query(QString query, bool* is_ok);

    QStringList keywords;
    UserHandle from_user;
    UserHandle retweeted_by_user;
    vector<UserHandle> to_users;

    int timestamp_from = 0;
    int timestamp_to = 0;

    bool filter_links = false;
    bool filter_images = false;
    bool filter_videos = false;

    vector<Tweet> execute() const;
    QString get_original_query() const;


 private:
    QString original_query;
    void update_with_token(QString token, bool* is_ok);
};

#endif  // MODELS_SEARCH_H


int yyyymmdd_to_timestamp(QString yyyymmdd);

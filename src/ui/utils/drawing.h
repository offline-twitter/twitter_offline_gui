#ifndef DRAWING_H
#define DRAWING_H

#include <QWidget>
#include <QColor>

void draw_outline(QWidget* w, int border_radius);
void draw_outline(QWidget* w, int border_radius, QColor color);
void set_background_color(QWidget* w, QColor color);
void fill_background(QWidget* w, QColor color, int border_radius = 0);
void fill_percentage(QWidget* w, QColor color, float percentage);

#endif  // DRAWING_H

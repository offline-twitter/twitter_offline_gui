#include "author_info.h"

#include <Qt>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFont>
#include <QPalette>

#include <memory>

#include "../lib/global_state.h"

#include "./colors.h"

using std::shared_ptr;
using std::make_shared;


AuthorInfo::AuthorInfo(QWidget* parent, shared_ptr<User> u): QWidget(parent) {
    this->u = u;

    layout = new QHBoxLayout(this);
    layout->setContentsMargins(0,0,0,0);
    setLayout(layout);

    profile_image_label = new QLabel(this);
    profile_img_pixmap = GlobalState::get_profile_img(u);  // TODO: this needs to be reloaded if the user's full-sized profile image gets downloaded, otherwise requires app restart to kick in
    if (profile_img_pixmap) {
        profile_image_label->setPixmap(*profile_img_pixmap);
    }
    profile_image_label->setCursor(Qt::PointingHandCursor);

    // Handle different-sized profile image labels
    if (u->is_content_downloaded) {
        profile_image_label->setFixedSize(60, 60);
        profile_image_label->setScaledContents(true);
        layout->addWidget(profile_image_label);
    } else {
        profile_image_label->setFixedSize(48, 48);
        profile_image_label->setScaledContents(true); //u->has_default_profile_image());

        // Add some extra padding to keep width consistent
        // TODO: do this with layout margins so it can be adjustable
        // - add method "set_profile_pic_margins(int)" or something to adjust them
        layout->addSpacing(6);
        layout->addWidget(profile_image_label);
        layout->addSpacing(6);
    }


    handle_and_display_name = new QWidget(this);
    QVBoxLayout* handle_and_display_name_layout = new QVBoxLayout(handle_and_display_name);
    handle_and_display_name_layout->setSpacing(0);
    handle_and_display_name->setLayout(handle_and_display_name_layout);

    handle_label = new QLabel(handle_and_display_name);
    handle_label->setText("@" + u->handle);
    handle_label->setTextInteractionFlags(Qt::TextSelectableByMouse);
    handle_label->setCursor(Qt::ArrowCursor);

    QPalette pal = handle_label->palette();
    pal.setColor(QPalette::WindowText, COLOR_TWITTER_TEXT_GRAY);
    handle_label->setPalette(pal);

    display_name_label = new QLabel(handle_and_display_name);
    display_name_label->setText(u->display_name);
    display_name_label->setTextInteractionFlags(Qt::TextSelectableByMouse);
    display_name_label->setCursor(Qt::ArrowCursor);

    QFont the_font = font();
    the_font.setPixelSize(14);
    the_font.setWeight(QFont::Bold);
    display_name_label->setFont(the_font);

    the_font.setWeight(50);
    handle_label->setFont(the_font);

    handle_and_display_name_layout->addStretch(1);

    name_and_verified_layout = new QHBoxLayout();
    handle_and_display_name_layout->addLayout(name_and_verified_layout);
    name_and_verified_layout->setContentsMargins(0,0,0,0);
    name_and_verified_layout->setSpacing(8);
    name_and_verified_layout->addWidget(display_name_label);
    if (u->is_verified) {
        QPixmap bluecheck_pixmap(":/icons/blue_check.png");
        bluecheck_label = new QLabel(this);
        bluecheck_label->setPixmap(bluecheck_pixmap);
        bluecheck_label->setFixedSize(16, 16);
        bluecheck_label->setScaledContents(true);
        name_and_verified_layout->addWidget(bluecheck_label);
    }
    if (u->is_private) {
        QPixmap lock_pixmap(":/icons/lock_icon.png");
        privated_label = new QLabel(this);
        privated_label->setPixmap(lock_pixmap);
        privated_label->setFixedSize(20, 20);
        privated_label->setScaledContents(true);
        privated_label->setToolTip("Account is private");
        name_and_verified_layout->addWidget(privated_label);
    }
    if (u->is_banned) {
        banned_label = new QLabel(this);
        banned_label->setText("[BANNED]");
        pal.setColor(QPalette::WindowText, COLOR_RED);
        banned_label->setPalette(pal);
        name_and_verified_layout->insertWidget(0, banned_label);
    }
    handle_and_display_name_layout->addWidget(handle_label);
    handle_and_display_name_layout->addStretch(1);

    layout->addWidget(handle_and_display_name);
}

void AuthorInfo::mousePressEvent(QMouseEvent* e) {
    if (e->button() == Qt::LeftButton) {
        emit profile_image_clicked(u);
    } else {
        e->ignore();  // Propagate it
    }
}

void AuthorInfo::set_image_size(int new_size) {
    profile_image_label->setFixedSize(new_size, new_size);
}

void AuthorInfo::set_text_size(int new_size) {
    if (bluecheck_label != nullptr) {
        bluecheck_label->setFixedSize(new_size + 2, new_size + 2);
    }
    if (privated_label != nullptr) {
        privated_label->setFixedSize(new_size + 6, new_size + 6);
    }

    QFont font = handle_label->font();
    font.setPixelSize(new_size);
    font.setWeight(80);
    display_name_label->setFont(font);
}

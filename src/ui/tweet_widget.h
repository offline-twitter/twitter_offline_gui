#ifndef TWEET_WIDGET_H
#define TWEET_WIDGET_H

#include <QVBoxLayout>
#include <QLabel>
#include <QWidget>
#include <QPaintEvent>
#include <QContextMenuEvent>

#include <memory>

#include "../models/tweet.h"
#include "../models/retweet.h"
#include "../models/user.h"

#include "./author_info.h"
#include "./interactions_bar.h"
#include "./column.h"
#include "./embedded_link.h"
#include "./video_preview.h"
#include "./poll_widget.h"
#include "./tombstone_widget.h"
#include "./space_widget.h"


using std::shared_ptr;
using std::make_shared;


class QuotedTweet;  // Forward declaration


class TweetWidget : public QWidget {
    Q_OBJECT

 private:
    shared_ptr<Tweet> t;
    shared_ptr<Retweet> rt;
    bool is_focused = false;               // Most tweets are not focused
    bool has_divider = true;               // By default, tweets are assumed to be separate
    bool has_vertical_reply_line = false;  // By default, tweets are not part of a thread
    int nesting_limit;

    QLabel* retweeted_icon = nullptr;
    QLabel* retweet_label = nullptr;
    QHBoxLayout* header_layout;
    QVBoxLayout* body_layout;

    AuthorInfo* author_info_panel;
    QLabel* in_reply_to_label;
    QLabel* posted_at_label;
    Tombstone* tombstone_label = nullptr;
    QLabel* text_label;
    QLabel* content_not_downloaded_label = nullptr;
    QuotedTweet* quoted_tweet_widget = nullptr;
    EmbeddedLink* embedded_link = nullptr;
    PollWidget* poll_widget = nullptr;
    VideoPreview* video_preview = nullptr;
    SpaceWidget* space_widget = nullptr;

    InteractionsBar* interactions_bar;

    void paintEvent(QPaintEvent*) override;

 public slots:
    void copy_link();
    void download_conversation();
    void download_contents();
    void open_in_browser();

 signals:
    void clicked(shared_ptr<Tweet> t);

 protected:
    void mousePressEvent(QMouseEvent*);
    void contextMenuEvent(QContextMenuEvent* e);

    Column* column = nullptr;
    QVBoxLayout* layout;


 public:
    explicit TweetWidget(QWidget* parent = nullptr, shared_ptr<Tweet> t = nullptr, Column* column = nullptr, shared_ptr<Retweet> rt = nullptr, int nesting_limit = 2);
    void set_is_focused(bool is_focused);
    void set_has_divider(bool has_divider);
    void set_has_vertical_reply_line(bool has_vertical_reply_line);
    void set_vertical_spacing(bool has_spacing_top, bool has_spacing_bottom);
};



class QuotedTweet : public TweetWidget {
    Q_OBJECT

 private:
    void paintEvent(QPaintEvent*) override;

 public:
    explicit QuotedTweet(QWidget* parent = nullptr, shared_ptr<Tweet> t = nullptr, Column* column = nullptr, int nesting_limit = 2);
};


#endif  // TWEET_WIDGET_H

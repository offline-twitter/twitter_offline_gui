#include "include_catch2/catch.h"

#define private public
#include "../src/ui/main_window.h"
#include "../src/ui/timeline.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

TEST_CASE("Should update the window title") {
	MainWindow w;

	REQUIRE(w.windowTitle() == "Twitter");

	w.update_title("ASDF");
	REQUIRE(w.windowTitle() == "Twitter | ASDF");

	w.update_title("a\na\n\n\na");
	REQUIRE(w.windowTitle() == "Twitter | a a   a");

	w.update_title("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz");
	REQUIRE(w.windowTitle() == "Twitter | abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwx...");
	w.update_title("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz");
	REQUIRE(w.windowTitle() == "Twitter | abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwx...");
}

TEST_CASE("Should set the profile directory and open the timeline") {
	MainWindow w;

	REQUIRE(w.column->is_welcome_mode == true);
	w.set_profile_directory_to(TEST_PROFILE_DIR);
	REQUIRE(w.column->is_welcome_mode == false);

	// Check that the timeline was opened
	REQUIRE(w.column->stacked_layout->count() == 1);
	REQUIRE(static_cast<Timeline*>(w.column->stacked_layout->itemAt(0)->widget())->no_tweets_label == nullptr);
}

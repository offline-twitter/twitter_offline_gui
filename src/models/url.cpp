#include "url.h"

#include <QSqlQuery>
#include <QVariant>

#include "./_db.h"


const QString Url::SQL_URL_FIELDS = "tweet_id, domain, text, title, description, creator_id, site_id, thumbnail_local_path, has_card, has_thumbnail, thumbnail_width, thumbnail_height, is_content_downloaded";  // NOLINT(whitespace/line_length)


vector<Url> Url::get_urls_for_tweet_id(TweetID tweet_id) {
	QSqlQuery query(DB);
	query.prepare("select " + Url::SQL_URL_FIELDS + " from urls where tweet_id = :tweet_id order by rowid");
	query.bindValue(":tweet_id", (unsigned long long) tweet_id);
	query.exec();

	vector<Url> ret;

	while (query.next()) {
		Url u;
		u.tweet_id = query.value(0).toULongLong();
		u.domain = query.value(1).toString();
		u.text = query.value(2).toString();
		u.title = query.value(3).toString();
		u.description = query.value(4).toString();
		u.creator_id = query.value(5).toULongLong();
		u.site_id = query.value(6).toULongLong();
		u.thumbnail_local_path = query.value(7).toString();
		u.has_card = query.value(8).toBool();
		u.has_thumbnail = query.value(9).toBool();
		u.thumbnail_width = query.value(10).toInt();
		u.thumbnail_height = query.value(11).toInt();
		u.is_content_downloaded = query.value(10).toBool();

		ret.push_back(u);
	}
	return ret;
}

QString Url::get_thumbnail_path() const {
	return "/link_preview_images/" + thumbnail_local_path;
}

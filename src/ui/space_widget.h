#ifndef UI_SPACE_H
#define UI_SPACE_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPaintEvent>
#include <QTimeZone>

#include "../models/space.h"

#include "./author_info.h"


class SpaceWidget : public QWidget {
    Q_OBJECT

 public:
    explicit SpaceWidget(QWidget* parent = nullptr, shared_ptr<Space> space = nullptr);

 private:
    shared_ptr<Space> space;

    QVBoxLayout* layout;

    AuthorInfo* creator_info;
    QLabel* title_label;

    QHBoxLayout* info_layout;
    QLabel* state_label;
    QLabel* started_at_label;
    QLabel* duration_label;
    QLabel* live_listeners_count_label;
    QLabel* is_replay_available_label;
    QLabel* replay_watch_count_label;

    QVBoxLayout* participants_layout;
    QVector<AuthorInfo*> participants_info;

    static QString render_started_at(int started_at, QTimeZone timezone);
    QString render_started_at() const;
    QString render_duration() const;

 protected:
    void paintEvent(QPaintEvent*);
};

#endif  // UI_SPACE_H

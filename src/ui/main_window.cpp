#include "main_window.h"

#include <Qt>
#include <QString>
#include <QMessageBox>
#include <QPalette>
#include <QApplication>
#include <QFileDialog>
#include <QCursor>

#include "../models/_db.h"
#include "../lib/global_state.h"


MainWindow::MainWindow(): QMainWindow() {
	QPalette pal = palette();
	pal.setColor(QPalette::Window, Qt::white);
	setPalette(pal);
	setAutoFillBackground(true);

	setMinimumSize(600, 400);
	resize(900, 900);

	column = new Column(this, true);  // Enable "is_welcome_mode"
	setCentralWidget(column);
	connect(column, SIGNAL(title_changed(QString)), this, SLOT(update_title(QString)));

	// Set up menus
	menu_bar = new QMenuBar(this);

	file_menu = new QMenu("File", menu_bar);
	file_menu->addAction("New profile directory", this, SLOT(new_profile_dir()), Qt::CTRL + Qt::Key_N);
	file_menu->addAction("Open profile directory", this, SLOT(choose_profile_dir()), Qt::CTRL + Qt::Key_O);
	file_menu->addSeparator();
	file_menu->addAction("Open Timeline", column, SLOT(open_timeline()));
	file_menu->addSeparator();
	file_menu->addAction("Quit", QApplication::quit, Qt::CTRL + Qt::Key_Q);
	menu_bar->addMenu(file_menu);

	help_menu = new QMenu("Help", menu_bar);
	help_menu->addAction("About", this, SLOT(display_about_page()));
	help_menu->addAction("Instructions", this, SLOT(display_instructions_page()));
	menu_bar->addMenu(help_menu);

	setMenuBar(menu_bar);

	update_title("");  // Set the title to just "Twitter"
}

/**
 * Use file chooser dialog to open a new profile dir, closing the old one.  The current session is
 * wiped first.
 */
void MainWindow::choose_profile_dir() {
	// TODO: this should be tested or something

	// First, confirm closing the current session
	if (column->count() > 0) {
		QMessageBox::StandardButton is_continue = QMessageBox::question(
			this,
			"Close the current session?",
			"Changing profiles will close all views in the current session.  Continue?",
			QMessageBox::Yes | QMessageBox::No
		);
		if (is_continue == QMessageBox::No) {
			return;
		}
	}

	// Set the new profile directory
	QString new_directory = QFileDialog::getExistingDirectory(this, "Open profile directory");
	if (new_directory == "")
		return;  // TODO: error popup
	printf("Opening profile directory: %s\n", new_directory.toStdString().c_str());
	this->set_profile_directory_to(new_directory);
}

void MainWindow::update_title(QString s) {
	if (s == "") {
		setWindowTitle("Twitter");
		return;
	}

	s = s.replace("\n", " ");
	if (s.size() > 50) {
		s = s.left(50) + "...";
	}

	setWindowTitle("Twitter | " + s);
}

/**
 * Use file chooser dialog to create a new profile dir, including an engine call. Includes the
 * option to open it immediately.
 */
void MainWindow::new_profile_dir() {
	// TODO: this should be tested or something

	QString new_directory = QFileDialog::getSaveFileName(this, "New profile directory");
	if (new_directory == "")
		return;  // TODO: error popup
	printf("New profile directory: %s\n", new_directory.toStdString().c_str());

	// Make an engine call
	QCursor tmp = cursor();
	setCursor(Qt::WaitCursor);
	create_profile_directory(new_directory);
	setCursor(tmp);

	if (GlobalState::PROFILE_DIR != "") {
		QMessageBox::StandardButton is_continue = QMessageBox::question(
			this,
			"Open the new profile directory?",
			"Do you want to open the new profile directory immediately?  This will close any browsing sessions you currently have open.",
			QMessageBox::Yes | QMessageBox::No
		);
		if (is_continue != QMessageBox::Yes)
			return;
	}
	this->set_profile_directory_to(new_directory);
}

void MainWindow::set_profile_directory_to(QString s) {
	set_profile_directory(s);

	column->exit_welcome_mode_if_needed();
	while (column->count() > 0) {
		column->go_back();
	}

	// Open the feed!
	column->open_timeline();
}

void MainWindow::display_about_page() {
	QString text =
		"Offline Twitter is in beta.  It works mostly fine but there are some hiccups that still need "
		"to be ironed out.\n\nVersion info:\n\n"
		"offline-twitter : v" + QString(OFFLINE_TWITTER_VERSION) + "\n"
		"engine version: " + GlobalState::ENGINE_VERSION + "\n\n"
		"For more info, visit: https://offline-twitter.com"
	;


	QMessageBox::information(this, "About Offline Twitter", text);
}

void MainWindow::display_instructions_page() {
	QString text =
		"Offline Twitter is an application for browsing twitter and automatically backing up "
		"twitter content.  Everything you browse is automatically saved as a local copy to your "
		"disk.\n"
		"\n"
		"For more information, please visit https://offline-twitter.com"
		"\n\n"
		"To browse, start by putting something into the navigation bar at the top.  You can enter "
		"a user handle (e.g., \"@elonmusk\"-- don't forget the '@' symbol), or paste a link to a tweet.\n"
		"\n"
		"When viewing a user's profile, click the download button on the right to download "
		"tweets.  You can browse around by clicking on tweets and users, the same way you would "
		"in a web browser.\n"
		"\n"
		"Clicking on an image or a video will open the full-sized version using your computer's "
		"native media viewer (e.g., VLC player, Windows Photo Gallery, etc.)."
	;
	QMessageBox::information(this, "How to use Offline Twitter", text);
}

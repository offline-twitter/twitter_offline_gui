#include "include_catch2/catch.h"

#include "../src/models/_db.h"
#include "../src/models/space.h"

#define private public

#include "../src/ui/space_widget.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::make_shared;


TEST_CASE("Should make a SpaceWidget") {
	set_profile_directory(TEST_PROFILE_DIR);

    Space s = Space::get_space_by_id("1OwGWwnoleRGQ");
    SpaceWidget space_widget(nullptr, make_shared<Space>(s));

    REQUIRE(space_widget.creator_info->u->handle == "MysteryGrove");
    REQUIRE(space_widget.title_label->text() == "I'm showering and the hot water ran out");
    REQUIRE(space_widget.state_label->text().toStdString() == "Ended \u22c5 9 participants \u22c5 255 tuned in \u22c5 Lasted 2h46m");
    REQUIRE(space_widget.participants_layout->count() == 9);
}

#include <memory>

#include "include_catch2/catch.h"

#include "../src/models/tweet.h"
#include "../src/models/space.h"
#include "../src/models/user.h"
#include "../src/models/_db.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::shared_ptr;


void require_contains(QVector<shared_ptr<User>> participants, UserID id) {
    for (shared_ptr<User> u: participants) {
        if (u->id == id) {
           REQUIRE(u->id == id);
           return;
        }
    }
    REQUIRE(0 == id);  // Fail and indicate what the failed id was
    REQUIRE(false);    // Make double sure
}
// Fetch a Space from DB
// --------------------

TEST_CASE("Should get a Space") {
    set_profile_directory(TEST_PROFILE_DIR);

    SpaceID space_id = "1OwGWwnoleRGQ";
    Space s = Space::get_space_by_id(space_id);

    REQUIRE(s.id == space_id);
    REQUIRE(s.created_by_id == 1178839081222115328);
    REQUIRE(s.short_url == "https://t.co/kxr7O7hfJ6");
    REQUIRE(s.state == "Ended");
    REQUIRE(s.title == "I'm showering and the hot water ran out");
    REQUIRE(s.created_at == 1676225386);
    REQUIRE(s.started_at == 1676225389);
    REQUIRE(s.ended_at == 1676235389);
    REQUIRE(s.updated_at == 1676229669);
    REQUIRE(s.is_available_for_replay == true);
    REQUIRE(s.replay_watch_count == 11);
    REQUIRE(s.live_listeners_count == 255);
    REQUIRE(s.is_details_fetched == 1);

    REQUIRE(s.created_by_user != nullptr);
    REQUIRE(s.created_by_user->handle == "MysteryGrove");

    REQUIRE(s.participants.size() == 9);

    // Slightly messy way to assert that there are participants with these UserIDs
    UserID test_cases[] = {238001308, 1233808749887922178, 1623438835295342592};

    for (UserID id: test_cases) {
        require_contains(s.participants, id);
    }
}

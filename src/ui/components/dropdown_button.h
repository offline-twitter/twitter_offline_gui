#ifndef DROPDOWN_BUTTON_H
#define DROPDOWN_BUTTON_H

#include <QWidget>
#include <QToolButton>
#include <QPaintEvent>


class DropdownButton : public QToolButton {
 public:
	DropdownButton(QWidget* parent = nullptr);

 protected:
	void paintEvent(QPaintEvent*) override;
};

#endif  // DROPDOWN_BUTTON_H

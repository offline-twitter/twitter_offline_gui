#include "engine_caller.h"

#include <QProcess>
#include <QMessageBox>

#include "global_state.h"

/**
 * Lets you make a fake Engine that won't call anything.  By default, an Engine is real.
 */
Engine::Engine(bool is_fake, int fake_return_code) {
	this->is_fake = is_fake;
	this->fake_return_code = fake_return_code;
}

/**
 * Call the engine.  Triggers a QProcess and returns its exit code.
 */
int Engine::call(QString cmd, QStringList args) {
	if (is_fake) {
		// Will be fake in testing mode, only.
		printf("Fake engine.\n");
		return fake_return_code;
	}

	printf("Not fake.\n");
	QProcess p;
	p.start(cmd, args);
	p.waitForFinished();

	int ret = p.exitCode();

	if (ret != 0) {
		QMessageBox::critical(nullptr, "Engine call failed", "Command:\n" + cmd + " " + args.join(" ") + "\nGot result " + QString::number(ret) + " from engine!");
	}
	return ret;
}

int Engine::init() {
	if (is_fake) {
		// Will be fake in testing mode, only.
		printf("Fake engine.\n");
		return fake_return_code;
	}

	QProcess p;
	p.start("twitter", QStringList() << "--version");
	p.waitForFinished();
	int ret = p.exitCode();

	if (ret == 0) {
		GlobalState::ENGINE_VERSION = QString(p.readAllStandardOutput());
	}
	return ret;
}

#include "video_player.h"

#include <QUrl>
#include <Qt>
#include <QFileInfo>
#include <QSize>

#include "../lib/global_state.h"

VideoPlayer::VideoPlayer(QWidget* parent, shared_ptr<Video> video): QWidget(parent) {
	this->video = video;

	layout = new QVBoxLayout(this);
	layout->setContentsMargins(0,0,0,0);
	layout->setSpacing(5);

	video_widget = new QVideoWidget(this);
	layout->addWidget(video_widget);

	QString abs_path = QFileInfo(GlobalState::PROFILE_DIR + video->get_filepath()).absoluteFilePath();
	media_player = new QMediaPlayer(this);
	printf("Abs path: %s\n", abs_path.toStdString().c_str());
	media_player->setMedia(QUrl::fromLocalFile(abs_path));
	media_player->setVideoOutput(video_widget);

	if (!video->is_gif) {
		// Proper videos need a progress bar
		progress_slider = new QSlider(Qt::Horizontal, this);
		printf("Setting duration: %lld\n", media_player->duration());
		progress_slider->setRange(0, media_player->duration());
		progress_slider->setSingleStep(100);
		progress_slider->setFixedHeight(20);
		layout->addWidget(progress_slider);

		connect(progress_slider, SIGNAL(sliderMoved(int)), this, SLOT(set_media_position(int)));
		connect(media_player, SIGNAL(positionChanged(qint64)), this, SLOT(set_progress_slider_value(qint64)));  // TODO: This might cause an infinite loop

		connect(media_player, SIGNAL(durationChanged(qint64)), this, SLOT(set_progress_slider_maximum(qint64)));
	}

	if (video->is_gif) {
		// Gifs should loop forever
		connect(media_player, SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(restart_video(QMediaPlayer::State)));
	}
}

QSize VideoPlayer::sizeHint() const {
	if (!video->is_gif) {
		return QSize(video->width, video->height + 25);
	} else {
		return QSize(video->width, video->height);
	}
}

void VideoPlayer::resizeEvent(QResizeEvent* e) {
	if (progress_slider != nullptr) {
		printf("Slider height: %d\n", progress_slider->height());
	}
	if (e->size().height() != e->oldSize().height()) {
		printf("Video height being changed from %d to %d\n", e->oldSize().height(), e->size().height());
	}
	if (e->size().width() != e->oldSize().width()) {
		printf("Video width being changed from %d to %d\n", e->oldSize().width(), e->size().width());
	}
}

void VideoPlayer::set_media_position(int position) {
	media_player->setPosition(position);
}

void VideoPlayer::set_progress_slider_value(qint64 value) {
	progress_slider->setValue(value);
}

void VideoPlayer::set_progress_slider_maximum(qint64 maximum) {
	progress_slider->setMaximum(maximum);
}

/**
 * Restarts the video.  Only applicable for gifs.
 */
void VideoPlayer::restart_video(QMediaPlayer::State media_state) {
	if (media_state == QMediaPlayer::StoppedState) {
		media_player->play();
	}
}

/**
 * Toggle the playing state of the video
 */
void VideoPlayer::mousePressEvent(QMouseEvent* e) {
	// Accept left-mouse only
	if (e->button() != Qt::LeftButton) {
		e->ignore();
		return;
	}

	// Toggle video state
	if (media_player->state() == QMediaPlayer::PlayingState) {
		media_player->pause();
	} else {
		media_player->play();
	}
}

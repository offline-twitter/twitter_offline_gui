#include <memory>

#include "include_catch2/catch.h"

#include "../src/models/_db.h"
#include "../src/models/retweet.h"
#include "../src/models/user.h"
#include "../src/models/tweet.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::shared_ptr;


TEST_CASE("Should get a retweet") {
    set_profile_directory(TEST_PROFILE_DIR);

    RetweetID retweet_id = 1449195266603630594;
    Retweet r = Retweet::get_retweet_by_id(retweet_id);

    REQUIRE(r.id == retweet_id);
    REQUIRE(r.tweet_id == 1449148515918270475);
    REQUIRE(r.retweeted_by_id == 44067298);
    REQUIRE(r.retweeted_at == 1634350050);

    shared_ptr<User> u = r.get_user();
    REQUIRE(u->handle == "michaelmalice");

    shared_ptr<Tweet> t = r.get_tweet();
    REQUIRE(t->text == "LOL");
}

// Fetch a lot of retweets from DB
// -----------------------------

TEST_CASE("Should get a bunch of retweets from followed users") {
    set_profile_directory(TEST_PROFILE_DIR);

    vector<Retweet> retweets = Retweet::get_followed_retweets_since(1644111021, -1);
    REQUIRE(retweets.size() == 2);
    REQUIRE(retweets[0].id == 1490135787144237058);
    REQUIRE(retweets[1].id == 1490135787124232222);

    REQUIRE(retweets[0].retweeted_at >= 1644111021);
    REQUIRE(retweets[1].retweeted_at == 1644111021);

    // Get the second page
    vector<Retweet> retweets2 = Retweet::get_followed_retweets_since(1644102560, 1644111021);
    REQUIRE(retweets2.size() == 2);
    REQUIRE(retweets2[0].id == 1490119308692766723);
    REQUIRE(retweets2[1].id == 1490100255987171332);
}

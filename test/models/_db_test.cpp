#include <Qt>
#include <QString>
#include <QSqlQuery>
#include <QSqlError>

#include "include_catch2/catch.h"

#include "../src/models/_db.h"
#include "../src/lib/global_state.h"


#define TEST_PROFILE_DIR "../../sample_data/profile"


TEST_CASE("Should set the global PROFILE_DIR") {
    set_profile_directory(TEST_PROFILE_DIR);

    REQUIRE(GlobalState::PROFILE_DIR == TEST_PROFILE_DIR);
}

TEST_CASE("Should throw an exception if it can't open a PROFILE_DIR") {
    try {
        set_profile_directory(TEST_PROFILE_DIR "fwef");
        REQUIRE(false);  // Should not have gotten this far
    } catch (DBException &e) {
        REQUIRE(e.what() == QString("Could not open: ../../sample_data/profilefwef/twitter.db"));
    }
}

TEST_CASE("Should be read-only; attempts to write should fail") {
    set_profile_directory(TEST_PROFILE_DIR);
    QSqlQuery query(DB);
    QStringList write_queries;
    write_queries << "insert into tombstone_types (short_name, tombstone_text) values ('invalid', 'Invalid text')"
                  << "drop table spaces"
                  << "create table a (b integer)"
                  << "insert into spaces (id, short_url, state, title) values (1, '1', 'Ended', 'Some Title')";

    for (QString test_case: write_queries) {
        bool status = query.exec(test_case);
        REQUIRE(status == false);
        QSqlError error = query.lastError();
        printf("%s\n", error.text().toStdString().c_str());
        REQUIRE(error.text().contains("attempt to write a readonly database", Qt::CaseInsensitive));
    }
}

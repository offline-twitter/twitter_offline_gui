#include "include_catch2/catch.h"

#include "../src/models/user.h"
#include "../src/models/_db.h"

#define private public

#include "../src/ui/followed_list.h"
#include "../src/ui/author_info.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"


TEST_CASE("Should make a FollowedList") {
	set_profile_directory(TEST_PROFILE_DIR);

	vector<User> all_followed = User::get_followed_users();
	FollowedList f(nullptr);
	REQUIRE(f.layout->count() == all_followed.size() + 1);  // Stretch element at bottom
	REQUIRE(f.layout->itemAt(f.layout->count() - 1)->spacerItem() != nullptr);
}

#include "feed.h"

#include <QString>
#include <QSqlQuery>

#include "../_db.h"

FeedItem::FeedItem(shared_ptr<Tweet> tweet, shared_ptr<Retweet> retweet): tweet(tweet), retweet(retweet) {}


Feed::Feed(){}

/**
 * Constructs a feed, with newest tweets / retweets at the front.
 * Both input lists must already be sorted.
 */
Feed Feed::construct_from(vector<Tweet> tweets, vector<Retweet> retweets) {
    Feed ret = Feed();

    uint i_tweets = 0;
    uint i_retweets = 0;

    // While both lists have items in them
    while (i_tweets < tweets.size() && i_retweets < retweets.size()) {
        if (tweets[i_tweets].posted_at > retweets[i_retweets].retweeted_at) {
            // It's a regular tweet
            ret.items.push_back(FeedItem(make_shared<Tweet>(tweets[i_tweets]), nullptr));
            i_tweets++;
        } else {
            // It's a retweet
            ret.items.push_back(FeedItem(nullptr, make_shared<Retweet>(retweets[i_retweets])));
            i_retweets++;
        }
    }

    // Finish up the remaining list
    while (i_tweets < tweets.size()) {
        ret.items.push_back(FeedItem(make_shared<Tweet>(tweets[i_tweets]), nullptr));
        i_tweets++;
    }
    while (i_retweets < retweets.size()) {
        ret.items.push_back(FeedItem(nullptr, make_shared<Retweet>(retweets[i_retweets])));
        i_retweets++;
    }

    return ret;
}

/**
 * Construct a Feed for a user, limited to `limit` amount of items and starting after `max_posted_at_time`.
 *
 * 1) Calculate `min_posted_at_time` by making a query
 * 2) use `User#get_tweets` and `User#get_retweets` helpers with the given time limits
 * 3) Assemble the two lists into a Feed
 */
Feed Feed::get_user_feed(shared_ptr<User> user, int limit, int max_posted_at_time) {
    QSqlQuery query(DB);
    query.prepare(QString("select min(posted_at) from (")
        +       "select posted_at from tweets where user_id = :user_id" + (max_posted_at_time > 0 ? " and posted_at < :max_posted_at" : "")
        + " union select retweeted_at from retweets where retweeted_by = :user_id" + (max_posted_at_time > 0 ? " and retweeted_at < :max_posted_at" : "")
        + "     order by posted_at desc limit :limit)");
    query.bindValue(":user_id", (long long) user->id);;
    query.bindValue(":max_posted_at", max_posted_at_time);
    query.bindValue(":limit", limit);
    if (!query.exec()) {
        throw DBException("Error executing mini-query");
    }
    query.next();

    int min_posted_at_time = query.value(0).toInt();

    vector<Tweet> tweets = user->get_tweets(min_posted_at_time, max_posted_at_time);
    vector<Retweet> retweets = user->get_retweets(min_posted_at_time, max_posted_at_time);
    return Feed::construct_from(tweets, retweets);
}

Feed Feed::get_timeline(int limit, int max_posted_at_time) {
    QSqlQuery query(DB);
    query.prepare(QString("select min(posted_at) from (")
        + "select posted_at from tweets where user_id in (select id from users where is_followed = 1)" + (max_posted_at_time > 0 ? " and posted_at < :max_posted_at" : "")
        + " union select retweeted_at from retweets where retweeted_by in (select id from users where is_followed = 1)" + (max_posted_at_time > 0 ? " and retweeted_at < :max_posted_at" : "")
        + " order by posted_at desc limit :limit)");
    query.bindValue(":max_posted_at", max_posted_at_time);
    query.bindValue(":limit", limit);
    if (!query.exec()) {
        throw DBException("Error executing mini-query");
    }
    query.next();
    int min_posted_at_time = query.value(0).toInt();

    vector<Tweet> tweets = Tweet::get_followed_tweets_since(min_posted_at_time, max_posted_at_time);
    vector<Retweet> retweets = Retweet::get_followed_retweets_since(min_posted_at_time, max_posted_at_time);

    return Feed::construct_from(tweets, retweets);
}

Feed Feed::from_search_params(SearchParams params) {
    vector<Tweet> results = params.execute();
    vector<Retweet> nothing;
    return Feed::construct_from(results, nothing);
}

int Feed::get_oldest_timestamp() const {
    if (items.size() == 0) {
        return 0;
    }

    auto last_item = items.back();
    return last_item.retweet != nullptr ? last_item.retweet->retweeted_at : last_item.tweet->posted_at;
}

#ifndef NAVBAR_H
#define NAVBAR_H

#include <QWidget>
#include <QString>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>

#include <memory>

#include "./components/clickable_icon.h"


using std::shared_ptr;


class Navbar : public QWidget {
    Q_OBJECT

 private:
    QHBoxLayout* layout;
    ClickableIcon* back_button;
    ClickableIcon* home_button;
    ClickableIcon* list_followed_button;
    QLabel* depth_label;
    QLineEdit* search_box;

    void paintEvent(QPaintEvent*) override;

 public:
    explicit Navbar(QWidget* parent = nullptr);
    void set_navigation_depth(int depth);

 signals:
    void back_button_pressed();
    void submit_search(QString s);
    void home_button_clicked();
    void list_followed_button_clicked();

 private slots:
    void trigger_search();
};

#endif  // NAVBAR_H

#include "drawing.h"

#include <QPainter>
#include <QPen>

#include <QPalette>

#include "../colors.h"


void draw_outline(QWidget* w, int border_radius, QColor color) {
    QPainter painter(w);
    painter.setPen(QPen(color));

    painter.drawLine(border_radius, 0, w->width() - border_radius, 0);
    painter.drawLine(border_radius, w->height() - 1, w->width() - border_radius, w->height() - 1);
    painter.drawLine(0, border_radius, 0, w->height() - border_radius);
    painter.drawLine(w->width() - 1, border_radius, w->width() - 1, w->height() - border_radius);

    painter.drawArc(0, 0, border_radius*2, border_radius*2, 90*16, 90*16);
    painter.drawArc(0, w->height() - 2*border_radius, border_radius*2, border_radius*2, 180*16, 90*16);
    painter.drawArc(w->width() - 2*border_radius, w->height() - 2*border_radius, border_radius*2, border_radius*2, 270*16, 90*16);
    painter.drawArc(w->width() - 2*border_radius, 0, border_radius*2, border_radius*2, 0*16, 90*16);
}

void draw_outline(QWidget* w, int border_radius) {
    draw_outline(w, border_radius, COLOR_OUTLINE_GRAY);
}

void fill_background(QWidget* w, QColor color, int border_radius) {
    QPainter painter(w);
    painter.setBrush(color);
    painter.setPen(color);

    painter.fillRect(border_radius, 0, w->width() - 2*border_radius, w->height(), color);
    painter.fillRect(0, border_radius, w->width(), w->height() - 2*border_radius, color);
    painter.drawPie(0,0,2*border_radius, 2*border_radius, 90*16, 90*16);
    painter.drawPie(w->width() - 2*border_radius, 0, 2*border_radius, 2*border_radius, 0*16, 90*16);
    painter.drawPie(0,w->height() - 2*border_radius, 2*border_radius, 2*border_radius, 180*16, 90*16);
    painter.drawPie(w->width() - 2*border_radius, w->height() - 2*border_radius, 2*border_radius, 2*border_radius, 270*16, 90*16);
}

void set_background_color(QWidget* w, QColor color) {
    QPalette pal = w->palette();
    pal.setColor(QPalette::Window, color);
    w->setPalette(pal);

    w->setAutoFillBackground(true);
}

void fill_percentage(QWidget* w, QColor color, float percentage) {
    QPainter painter(w);
    painter.fillRect(0, 0, w->width() * percentage, w->height(), color);
}

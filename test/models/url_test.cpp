#include "include_catch2/catch.h"

#include "../src/models/_db.h"
#include "../src/models/url.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

TEST_CASE("Should get an url from a tweet") {
	set_profile_directory(TEST_PROFILE_DIR);

	TweetID tweet_id = 1413665734866186243;
	vector<Url> urls = Url::get_urls_for_tweet_id(tweet_id);

	REQUIRE(urls.size() == 1);
	REQUIRE(urls[0].domain == "en.m.wikipedia.org");
	REQUIRE(urls[0].text == "https://en.m.wikipedia.org/wiki/Entryism");
	REQUIRE(urls[0].title == "Entryism - Wikipedia");
	REQUIRE(urls[0].has_card == true);
	REQUIRE(urls[0].has_thumbnail == false);
}

TEST_CASE("Should get a bunch of urls from a tweet") {
	set_profile_directory(TEST_PROFILE_DIR);

	TweetID tweet_id = 1438642143170646017;
	vector<Url> urls = Url::get_urls_for_tweet_id(tweet_id);

	REQUIRE(urls.size() == 3);

	REQUIRE(urls[0].text.toStdString() == "https://www.politico.com/story/2016/07/joe-biden-democrats-middle-class-226306");
	REQUIRE(urls[0].has_card == false);
	REQUIRE(urls[0].has_thumbnail == false);

	REQUIRE(urls[1].text == "https://time.com/5878437/trump-white-middle-class-voters/");
	REQUIRE(urls[1].has_card == false);
	REQUIRE(urls[1].has_thumbnail == false);

	REQUIRE(urls[2].text == "https://www.brookings.edu/research/bidens-victory-came-from-the-suburbs/");
	REQUIRE(urls[2].domain == "www.brookings.edu");
	REQUIRE(urls[2].title == "Biden’s victory came from the suburbs");
	REQUIRE(urls[2].get_thumbnail_path() == "/link_preview_images/W2kzjt4-_800x320_1.jpg");
	REQUIRE(urls[2].has_card == true);
	REQUIRE(urls[2].has_thumbnail == true);
	REQUIRE(urls[2].thumbnail_width == 568);
	REQUIRE(urls[2].thumbnail_height == 320);
}

#include "tombstone_widget.h"

#include <QPalette>

#include "./utils/drawing.h"
#include "./colors.h"

Tombstone::Tombstone(QWidget* parent, shared_ptr<Tweet> t): QLabel(parent) {
    this->t = t;
    setText(Tweet::TOMBSTONE_LABELS[t->tombstone_type]);
    setContentsMargins(20, 10, 20, 10);
    setWordWrap(true);

    QPalette pal = palette();
    pal.setColor(QPalette::WindowText, COLOR_TWITTER_TEXT_GRAY);
    setPalette(pal);
}

void Tombstone::paintEvent(QPaintEvent* e) {
    fill_background(this, COLOR_TWITTER_OFF_WHITE, 12);
    draw_outline(this, 12);
    QLabel::paintEvent(e);
}

#ifndef CONVERSATION_WIDGET_H
#define CONVERSATION_WIDGET_H

#include <QWidget>
#include <QScrollArea>
#include <QVBoxLayout>

#include <memory>

#include "../models/tweet.h"
#include "./tweet_widget.h"
#include "./column.h"

using std::vector;
using std::shared_ptr;


class Conversation : public QScrollArea {
    Q_OBJECT

 private:
    shared_ptr<Tweet> t;

    TweetWidget* main_tweet;

    QWidget* dummy;
    QVBoxLayout* layout;

 public:
    explicit Conversation(Column* parent = nullptr, shared_ptr<Tweet> t = nullptr);
};

#endif  // CONVERSATION_WIDGET_H

#ifndef EMBEDDED_LINK_H
#define EMBEDDED_LINK_H

#include <QString>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QContextMenuEvent>

#include <memory>

#include "../models/url.h"

using std::shared_ptr;


class EmbeddedLink : public QWidget {
    Q_OBJECT

 private:
    shared_ptr<Url> url;

    QVBoxLayout* layout;

    QLabel* link_preview_image;
    QLabel* title_label;
    QLabel* description_label;

    QHBoxLayout* domain_layout;
    QLabel* link_icon_label;
    QLabel* domain_label;

 private slots:
    void copy_link();
    void open_in_browser();

 protected:
    void paintEvent(QPaintEvent*);
    void mousePressEvent(QMouseEvent*);
    void contextMenuEvent(QContextMenuEvent* e);

 public:
    explicit EmbeddedLink(QWidget* parent = nullptr, shared_ptr<Url> url = nullptr);

 signals:
    void clicked();
};

#endif  // EMBEDDED_LINK_H

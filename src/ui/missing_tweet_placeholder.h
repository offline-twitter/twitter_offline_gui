#ifndef MISSING_TWEET_PLACEHOLDER_H
#define MISSING_TWEET_PLACEHOLDER_H

#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>

#include "../models/tweet.h"

class MissingTweetPlaceholder : public QWidget {
    Q_OBJECT

 private:
    TweetID tweet_id;
    QPushButton* label;
    QHBoxLayout* layout;

 private slots:
    void load_tweet();

 public:
    explicit MissingTweetPlaceholder(TweetID tweet_id = 0, QWidget* parent = nullptr);
};

#endif  // MISSING_TWEET_PLACEHOLDER_H

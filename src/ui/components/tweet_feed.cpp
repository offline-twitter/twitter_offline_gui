#include "tweet_feed.h"

TweetFeed::TweetFeed(Feed feed, QWidget* parent, Column* column): QWidget(parent) {
	this->column = column;
	oldest_timestamp = feed.get_oldest_timestamp();

	layout = new QVBoxLayout(this);
	layout->setSpacing(0);
	layout->setContentsMargins(0,0,0,0);

	tweets_layout = new QVBoxLayout();
	tweets_layout->setSpacing(0);
	tweets_layout->setContentsMargins(0,0,0,0);
	layout->addLayout(tweets_layout);


	for (FeedItem item: feed.items) {
		if (item.retweet != nullptr) {
			tweet_widgets.push_back(new TweetWidget(this, item.retweet->get_tweet(), column, item.retweet));
		} else {
			tweet_widgets.push_back(new TweetWidget(this, item.tweet, column, nullptr));
		}
	}

	for (TweetWidget* w: tweet_widgets) {
		tweets_layout->addWidget(w);
	}

	load_more_tweets_button = new QPushButton(this);
	set_load_more_button_enabled(true);
	layout->addWidget(load_more_tweets_button);
	connect(load_more_tweets_button, SIGNAL(clicked()), this, SIGNAL(load_more_tweets()));
}

void TweetFeed::extend_with(Feed feed) {
	if (feed.items.size() == 0) {
		set_load_more_button_enabled(false);
		return;
	}

	oldest_timestamp = feed.get_oldest_timestamp();

	for (FeedItem item: feed.items) {
		if (item.retweet != nullptr) {
			tweet_widgets.push_back(new TweetWidget(this, item.retweet->get_tweet(), column, item.retweet));
		} else {
			tweet_widgets.push_back(new TweetWidget(this, item.tweet, column, nullptr));
		}
		tweets_layout->addWidget(tweet_widgets.back());
	}
}

int TweetFeed::get_oldest_timestamp() {
	return oldest_timestamp;
}

void TweetFeed::set_load_more_button_enabled(bool is_enabled) {
	load_more_tweets_button->setText(is_enabled ? "Load more tweets" : "End of feed!");
	load_more_tweets_button->setEnabled(is_enabled);
}

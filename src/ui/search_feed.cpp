#include "search_feed.h"

#include <Qt>
#include <QScrollBar>

SearchFeed::SearchFeed(SearchParams params, Column* parent): QScrollArea(parent) {
    this->column = parent;
    this->cursor = make_shared<SSF::Cursor>();
    this->cursor->search_params = params;

    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    setWidgetResizable(true);
    setFrameShape(QFrame::NoFrame);

    dummy = new QWidget(this);
    layout = new QVBoxLayout(dummy);
    layout->setSpacing(0);
    layout->setContentsMargins(0, 0, 0, 0);
    dummy->setLayout(layout);
    dummy->setFixedWidth(width() - verticalScrollBar()->width());
    dummy->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);

    QFont font("Titillium Web");
    font.setPixelSize(30);
    font.setWeight(QFont::Bold);

    title = new QLabel(this);
    title->setText("Search results => " + params.get_original_query());
    title->setFont(font);
    title->setContentsMargins(30, 30, 30, 30);
    title->setAlignment(Qt::AlignHCenter);
    title->setWordWrap(true);
    layout->addWidget(title);

    sort_options_layout = new QHBoxLayout();
    layout->addLayout(sort_options_layout);
    sort_options_layout->setContentsMargins(10, 10, 10, 10);
    sort_options_layout->setSpacing(10);
    sort_options_layout->addStretch(1);

    sort_options_label = new QLabel(this);
    sort_options_label->setText("Sort by");
    sort_options_layout->addWidget(sort_options_label);

    sort_options_dropdown = new QComboBox(this);
    sort_options_dropdown->addItem(SSF::SORT_ORDER_NEWEST);
    sort_options_dropdown->addItem(SSF::SORT_ORDER_OLDEST);
    sort_options_dropdown->addItem(SSF::SORT_ORDER_MOST_LIKES);
    sort_options_dropdown->addItem(SSF::SORT_ORDER_MOST_RETWEETS);
    sort_options_layout->addWidget(sort_options_dropdown);
    connect(sort_options_dropdown, SIGNAL(currentTextChanged(QString)), this, SLOT(change_sort_order(QString)));

    layout->addStretch(1);
    reinit_feed();

    setWidget(dummy);
}

void SearchFeed::reinit_feed() {
    Feed feed = cursor->get_next_page();
    this->cursor = feed.cursor;

    tweet_feed = new TweetFeed(feed, this, column);
    tweet_feed->set_load_more_button_enabled(cursor->cursor_type != SSF::CursorType::END);
    layout->insertWidget(layout->count() - 1, tweet_feed);
    connect(tweet_feed, SIGNAL(load_more_tweets()), this, SLOT(load_more_tweets()));
}


void SearchFeed::resizeEvent(QResizeEvent*) {
    dummy->setFixedWidth(width() - verticalScrollBar()->width());
}

void SearchFeed::change_sort_order(QString new_sort_order) {
    // Reset the cursor
    cursor->sort_order = new_sort_order;
    cursor->cursor_type = SSF::CursorType::START;

    // Reset the TweetFeed
    QLayoutItem* item = layout->takeAt(layout->indexOf(tweet_feed));
    delete item->widget();
    delete item;

    this->reinit_feed();
}

void SearchFeed::download_search_results() {
	// TODO
}

/**
 * Get more search results
 */
void SearchFeed::load_more_tweets() {
    Feed feed = cursor->get_next_page();
    tweet_feed->extend_with(feed);
    tweet_feed->set_load_more_button_enabled(cursor->cursor_type != SSF::CursorType::END);
    this->cursor = feed.cursor;
}

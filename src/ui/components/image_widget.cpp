#include "image_widget.h"

#include <Qt>
#include <QMenu>
#include <QDesktopServices>
#include <QGuiApplication>
#include <QClipboard>
#include <QIcon>

#include "../../lib/global_state.h"


/**
 * Filepath is interpreted literally.  It does not add it to PROFILE_DIR or anything.
 */
ImageWidget::ImageWidget(QWidget* parent, QString filepath) : QLabel(parent) {
	this->filepath = filepath;
	this->_pixmap = QPixmap(filepath);
	setPixmap(_pixmap);
	setCursor(Qt::PointingHandCursor);
}

/**
 * Open this image in an external image viewer (whatever viewer is the OS default)
 */
void ImageWidget::open_in_external() {
	QDesktopServices::openUrl(QUrl(filepath));
}

/**
 * Copy this image to the clipboard
 */
void ImageWidget::copy_image() {
	QGuiApplication::clipboard()->setImage(_pixmap.toImage());
}

/**
 * Copy the filepath to the clipboard
 */
void ImageWidget::copy_image_path() {
	QGuiApplication::clipboard()->setText(filepath);
}

QSize ImageWidget::sizeHint() const {
	double scaling_factor = static_cast<double>(width()) / _pixmap.width();
	return QSize(_pixmap.width(), _pixmap.height() * scaling_factor);
}

/**
 * On right-click, open a context menu for this image
 */
void ImageWidget::mousePressEvent(QMouseEvent* e) {
	if (e->button() == Qt::LeftButton) {
		open_in_external();
	} else {
		e->ignore();  // Propagate it
	}
}

void ImageWidget::contextMenuEvent(QContextMenuEvent* e) {
	QMenu menu(this);
	menu.addAction(QIcon(":/icons/copy_icon.png"), "Copy image", this, SLOT(copy_image()));
	menu.addAction(QIcon(":/icons/link_icon.png"), "Copy image path", this, SLOT(copy_image_path()));
	menu.addSeparator();
	menu.addAction(QIcon(":/icons/open_external_icon.png"), "Open image", this, SLOT(open_in_external()));
	menu.exec(e->globalPos());
}

/**
 * If this stops working, check here (even though I didn't actually use this):
 * https://stackoverflow.com/questions/14107144/how-do-i-make-an-image-resize-to-scale-in-qt
 */
void ImageWidget::resizeEvent(QResizeEvent* e) {
	QPixmap new_pixmap = _pixmap.scaled(e->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
	setPixmap(new_pixmap);
}

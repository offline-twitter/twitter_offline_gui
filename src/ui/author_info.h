#ifndef AUTHOR_INFO_H
#define AUTHOR_INFO_H

#include <QWidget>
#include <QHBoxLayout>
#include <QPixmap>
#include <QLabel>
#include <QMouseEvent>

#include "../models/user.h"


class AuthorInfo : public QWidget {
    Q_OBJECT

 public:
    shared_ptr<User> u;

 private:
    QHBoxLayout* layout;

    QLabel* profile_image_label;

    QWidget* handle_and_display_name;
    QLabel* handle_label;
    QHBoxLayout* name_and_verified_layout;
    QLabel* display_name_label;
    QLabel* bluecheck_label = nullptr;
    QLabel* privated_label = nullptr;
    QLabel* banned_label = nullptr;

    shared_ptr<QPixmap> profile_img_pixmap;

 protected:
    void mousePressEvent(QMouseEvent*);

 signals:
    void profile_image_clicked(shared_ptr<User>);  // TODO: misnomer; should it only be the profile image, or whole thing?

 public:
    explicit AuthorInfo(QWidget* parent = nullptr, shared_ptr<User> u = nullptr);
    void set_image_size(int new_size);
    void set_text_size(int new_size);
};

#endif  // TWEET_AUTHOR_H

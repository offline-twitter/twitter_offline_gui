#define private public

#include "include_catch2/catch.h"

#include "../src/ui/poll_widget.h"

using std::make_shared;


TEST_CASE("Should make a PollWidget") {
	Poll p;
	p.num_choices = 3;
	p.choices.push_back("A");
	p.choices.push_back("B");
	p.choices.push_back("C");
	p.votes.push_back(1);
	p.votes.push_back(2);
	p.votes.push_back(3);

	p.voting_ends_at = 1632432755;

	PollWidget poll_widget(nullptr, make_shared<Poll>(p));

	REQUIRE(poll_widget.options.size() == 3);

	REQUIRE(poll_widget.options[0]->choice_label->text() == "A");
	REQUIRE(poll_widget.options[0]->votes_label->text() == "1 (16.6%)");
	REQUIRE(poll_widget.options[0]->is_winner == false);

	REQUIRE(poll_widget.options[1]->choice_label->text() == "B");
	REQUIRE(poll_widget.options[1]->votes_label->text() == "2 (33.3%)");
	REQUIRE(poll_widget.options[1]->is_winner == false);

	REQUIRE(poll_widget.options[2]->choice_label->text() == "C");
	REQUIRE(poll_widget.options[2]->votes_label->text() == "3 (50%)");
	REQUIRE(poll_widget.options[2]->is_winner == true);

	REQUIRE(poll_widget.layout->count() == 4);  // 3 options and a metadata label

	REQUIRE(poll_widget.metadata_label != nullptr);
}

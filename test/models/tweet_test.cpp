#include <memory>

#include "include_catch2/catch.h"

#include "../src/models/tweet.h"
#include "../src/models/user.h"
#include "../src/models/_db.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::shared_ptr;


// Fetch tweets from DB
// --------------------

TEST_CASE("Should get a tweet") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1343633011364016128;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    REQUIRE(t.id == tweet_id);
    REQUIRE(t.user_id == 836779281049014272);
    REQUIRE(t.text == R"(this is why the "think tank mindset" is a dead end. it misapprehends the nature of power. the "battle of ideas" is a meaningless sideshow when the terms on which it is fought are set elsewhere. it is a fiction. appealing because of its simplicity but always won or lost in advance)");
    REQUIRE(t.posted_at == 1609182048);
    REQUIRE(t.num_likes == 138);
    REQUIRE(t.num_retweets == 9);
    REQUIRE(t.num_replies == 2);
    REQUIRE(t.num_quote_tweets == 1);
    REQUIRE(t.in_reply_to_id == 1343630971057418240);
    REQUIRE(t.quoted_tweet_id == 0);
    REQUIRE(t.is_content_downloaded == false);
    REQUIRE(t.is_conversation_scraped == false);
    REQUIRE(t.last_scraped_at == 0);
    REQUIRE(t.tombstone_type == false);
    REQUIRE(t.is_stub == false);
    REQUIRE(t.space_id == "");
    REQUIRE(t.space == nullptr);
}

TEST_CASE("Should get a tweet with a quote-tweet") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1426669666928414720;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    REQUIRE(t.id == tweet_id);
    REQUIRE(t.user_id == 887434912529338375);
    REQUIRE(t.quoted_tweet_id == 1426654719183835136);
}

TEST_CASE("Should get a tweet with mentions") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1343715029707796489;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    REQUIRE(t.id == tweet_id);
    REQUIRE(t.mentions.size() == 2);
    REQUIRE(t.mentions[0] == "kwamurai");
    REQUIRE(t.mentions[1] == "Saradin1337");
}

TEST_CASE("Should get a tweet without mentions") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1261483383483293700;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    REQUIRE(t.id == tweet_id);
    REQUIRE(t.mentions.size() == 0);
}

TEST_CASE("Should get a tweet with a url") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1413665734866186243;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    REQUIRE(t.id == tweet_id);
    REQUIRE(t.urls.size() == 1);
    REQUIRE(t.urls[0].text == "https://en.m.wikipedia.org/wiki/Entryism");
}

TEST_CASE("Should get a tweet with 4 images") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1261483383483293700;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    REQUIRE(t.id == tweet_id);
    REQUIRE(t.images.size() == 4);
    REQUIRE(t.images[0].id == 1261483377363791872);
    REQUIRE(t.images[0].remote_url == "https://pbs.twimg.com/media/EYGwcrXUMAAiyCf.jpg");
    REQUIRE(t.images[0].local_filename == "EYGwcrXUMAAiyCf.jpg");
    REQUIRE(t.images[0].is_downloaded == true);

    REQUIRE(t.images[1].id == 1261483377368039424);
    REQUIRE(t.images[1].remote_url == "https://pbs.twimg.com/media/EYGwcrYVAAAFY_U.jpg");
    REQUIRE(t.images[1].local_filename == "EYGwcrYVAAAFY_U.jpg");
    REQUIRE(t.images[1].is_downloaded == true);

    REQUIRE(t.images[2].id == 1261483377409970177);
    REQUIRE(t.images[2].remote_url == "https://pbs.twimg.com/media/EYGwcriU0AEvGA1.jpg");
    REQUIRE(t.images[2].local_filename == "EYGwcriU0AEvGA1.jpg");
    REQUIRE(t.images[2].is_downloaded == true);

    REQUIRE(t.images[3].id == 1261483377519017984);
    REQUIRE(t.images[3].remote_url == "https://pbs.twimg.com/media/EYGwcr8UwAApzgz.jpg");
    REQUIRE(t.images[3].local_filename == "EYGwcr8UwAApzgz.jpg");
    REQUIRE(t.images[3].is_downloaded == true);
}

TEST_CASE("Should get a tweet with a video") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1426619468327882761;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    REQUIRE(t.id == tweet_id);
    REQUIRE(t.videos.size() == 1);
    REQUIRE(t.videos[0].id == 1426619366829924358);
    REQUIRE(t.videos[0].remote_url == "https://video.twimg.com/ext_tw_video/1426619366829924358/pu/vid/1280x720/vjY7yiXiRMV4m9T1.mp4?tag=12");
    REQUIRE(t.videos[0].local_filename == "1426619468327882761.mp4");
    REQUIRE(t.videos[0].is_downloaded == false);
}

TEST_CASE("Should get a tweet with a poll") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1465534109573390348;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    REQUIRE(t.id == tweet_id);
    REQUIRE(t.polls.size() == 1);

    Poll p = t.polls[0];
    REQUIRE(p.id == 1465534108923314180);
    REQUIRE(p.num_choices == 4);
    REQUIRE(p.choices.size() == 4);
    REQUIRE(p.choices[0] == "Tribal armband");
    REQUIRE(p.choices[1] == "Marijuana leaf");
    REQUIRE(p.choices[2] == "Butterfly");
    REQUIRE(p.choices[3] == "Maple leaf");

    REQUIRE(p.votes.size() == 4);
    REQUIRE(p.votes[0] == 1593);
    REQUIRE(p.votes[1] == 624);
    REQUIRE(p.votes[2] == 778);
    REQUIRE(p.votes[3] == 1138);

    REQUIRE(p.voting_duration == 24 * 3600);
    REQUIRE(p.voting_ends_at < p.last_scraped_at);
}

TEST_CASE("Should get a tweet with a space") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1624833173514293249;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    REQUIRE(t.id == tweet_id);
    REQUIRE(t.space_id == "1OwGWwnoleRGQ");
    REQUIRE(t.space != nullptr);
    REQUIRE(t.space->id == t.space_id);
    REQUIRE(t.space->live_listeners_count == 255);
}

TEST_CASE("Should get a tweet that is a stub") {
    set_profile_directory(TEST_PROFILE_DIR);

    Tweet t = Tweet::get_tweet_by_id(31);

    REQUIRE(t.is_stub == true);
    REQUIRE(t.tombstone_type == 1);
}


// Fetch a lot of tweets from DB
// -----------------------------

TEST_CASE("Should get a bunch of tweets from followed users") {
    set_profile_directory(TEST_PROFILE_DIR);

    vector<Tweet> tweets = Tweet::get_followed_tweets_since(1625904768, -1);
    REQUIRE(tweets.size() == 7);
    REQUIRE(tweets[0].id == 1453461248142495744);
    REQUIRE(tweets[1].id == 1449148515918270475);
    REQUIRE(tweets[2].id == 31);  // Not a real tweet btw
    REQUIRE(tweets[3].id == 1439068749336748043);
    REQUIRE(tweets[4].id == 1439067163508150272);
    REQUIRE(tweets[5].id == 1439027915404939265);
    REQUIRE(tweets[6].id == 1413773185296650241);

    for (Tweet t: tweets) {
        REQUIRE(t.posted_at >= 1625904768);
    }
    REQUIRE(tweets[6].posted_at == 1625904768);

    // Get the second page
    vector<Tweet> tweets2 = Tweet::get_followed_tweets_since(1625874519, 1625904768);
    REQUIRE(tweets2.size() == 6);
    REQUIRE(tweets2[0].id == 1413664406995566593);
    REQUIRE(tweets2[1].id == 1413658466795737091);
    REQUIRE(tweets2[2].id == 1413650853081276421);
    REQUIRE(tweets2[3].id == 1413647919215906817);
    REQUIRE(tweets2[4].id == 1413646595493568516);
    REQUIRE(tweets2[5].id == 1413646309047767042);
}


// Lazy loading: fetch other items attached to a tweet
// ---------------------------------------------------

TEST_CASE("Should get the user of a tweet") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1261483383483293700;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    shared_ptr<User> u = t.get_user();

    REQUIRE(u->id == 2703181339);
    REQUIRE(u->handle == "Denlesks");
}

TEST_CASE("Should get a tweet's quoted tweet") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1413664406995566593;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    TweetID quoted_tweet_id = 1413646595493568516;

    REQUIRE(t.id == tweet_id);
    REQUIRE(t.quoted_tweet_id == quoted_tweet_id);

    shared_ptr<Tweet> quoted_tweet = t.get_quoted_tweet();
    REQUIRE(quoted_tweet->id == quoted_tweet_id);
    REQUIRE(quoted_tweet->num_likes == 184);
    REQUIRE(quoted_tweet->num_retweets == 4);
    REQUIRE(quoted_tweet->num_replies == 4);
    REQUIRE(quoted_tweet->num_quote_tweets == 1);
}

TEST_CASE("Should get a tweet's replied tweet") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1413665734866186243;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    TweetID replied_tweet_id = 1413664406995566593;

    REQUIRE(t.id == tweet_id);
    REQUIRE(t.in_reply_to_id == replied_tweet_id);

    shared_ptr<Tweet> replied_tweet = t.get_replied_tweet();
    REQUIRE(replied_tweet->id == replied_tweet_id);
    REQUIRE(replied_tweet->posted_at == 1625878833);
}

TEST_CASE("Should get a tweet's replies") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1413646595493568516;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    vector<shared_ptr<Tweet>> replies = t.get_replies();
    REQUIRE(replies.size() == 4);

    REQUIRE(replies[0]->id == 1413647919215906817);
    REQUIRE(replies[0]->num_likes == 109);
    REQUIRE(replies[0]->user_id == 1032468021485293568);

    REQUIRE(replies[1]->id == 1413657324267311104);
    REQUIRE(replies[1]->text == "Did if affect your political views?");

    vector<shared_ptr<Tweet>> replies1 = replies[1]->get_replies();
    REQUIRE(replies1[0]->id == 1413658466795737091);

    // Test size limits
    replies = t.get_replies(0);
    REQUIRE(replies.size() == 0);
    replies = t.get_replies(1);
    REQUIRE(replies.size() == 1);
    REQUIRE(replies[0]->id == 1413647919215906817);
}

TEST_CASE("Should get a tweet's thread") {
    set_profile_directory(TEST_PROFILE_DIR);
    Tweet t = Tweet::get_tweet_by_id(1413646309047767042);

    vector<shared_ptr<Tweet>> thread = t.get_thread();
    REQUIRE(thread.size() == 2);
    REQUIRE(thread[0]->id == 1413646595493568516);
    REQUIRE(thread[0]->user_id == 1032468021485293568);
    REQUIRE(thread[1]->id == 1413647919215906817);
    REQUIRE(thread[1]->user_id == 1032468021485293568);

    // Test a tweet that has no thread
    t = Tweet::get_tweet_by_id(1413772782358433792);
    REQUIRE(t.get_thread().size() == 0);  // Shouldn't be any
}


// Helper methods
// --------------

TEST_CASE("Should assemble a tweet's permalink URL") {
    set_profile_directory(TEST_PROFILE_DIR);
    Tweet t = Tweet::get_tweet_by_id(1426619468327882761);

    REQUIRE(t.get_permalink() == "https://twitter.com/thevivafrei/status/1426619468327882761");
}

TEST_CASE("Should parse a tweet permalink and produce a TweetID") {
    bool is_ok;
    QString test1 = "https://twitter.com/michaelmalice/status/1444766573189812224";
    REQUIRE(Tweet::parse_url(test1, &is_ok) == 1444766573189812224);
    REQUIRE(is_ok == true);
}

TEST_CASE("Should parse a tweet ID (string) and produce a TweetID") {
    bool is_ok;
    QString test1 = "1444766573189812224";
    REQUIRE(Tweet::parse_id(test1, &is_ok) == 1444766573189812224);
    REQUIRE(is_ok == true);

    // Should not match something that isn't just a number
    QString test2 = "asdf123";
    Tweet::parse_id(test2, &is_ok);
    REQUIRE(is_ok == false);
}

TEST_CASE("Parsing a tweet permalink should fail if it isn't one") {
    bool is_ok;
    Tweet::parse_url("https://twitter.com/michaelmalice", &is_ok);
    REQUIRE(is_ok == false);
    Tweet::parse_url("https://facebook.com/something/status/A", &is_ok);
    REQUIRE(is_ok == false);
    Tweet::parse_url("michaelmalice", &is_ok);
    REQUIRE(is_ok == false);
    Tweet::parse_url("gawjeklfd", &is_ok);
    REQUIRE(is_ok == false);
}


TEST_CASE("Should assemble a tweet's 'Replying to' text") {
    set_profile_directory(TEST_PROFILE_DIR);
    Tweet t = Tweet::get_tweet_by_id(1428951883058753537);

    REQUIRE(t.get_replying_to_text().toStdString() == "Replying to: @JiffjoffI \u22c5 @primalpoly \u22c5 @jmasseypoet \u22c5 @SpaceX");
}

TEST_CASE("Assembling 'Replying to' text should not break if replied tweet is unavailable") {
    set_profile_directory(TEST_PROFILE_DIR);
    Tweet t = Tweet::get_tweet_by_id(1428939163961790466);

    REQUIRE(t.get_replying_to_text().toStdString() == "Replying to: @JiffjoffI \u22c5 @CovfefeAnon \u22c5 @primalpoly \u22c5 @jmasseypoet \u22c5 @SpaceX");
}

TEST_CASE("Threads should give a 'Replying to' text that says 'thread'") {
    set_profile_directory(TEST_PROFILE_DIR);
    Tweet t = Tweet::get_tweet_by_id(1413646595493568516);
    REQUIRE(t.reply_mentions.size() == 0);
    REQUIRE(t.in_reply_to_id != 0);

    REQUIRE(t.get_replying_to_text().toStdString() == "<thread>");
}

TEST_CASE("Should render a date") {
    int unix_timestamp = 1632432755;
    QString rendered = Tweet::render_posted_at(unix_timestamp, QTimeZone::utc());
    REQUIRE(rendered.toStdString() == "Sep 23, 2021\n9:32 pm");

    set_profile_directory(TEST_PROFILE_DIR);
    Tweet t = Tweet::get_tweet_by_id(1426619468327882761);
    REQUIRE(t.render_posted_at() == Tweet::render_posted_at(t.posted_at, QTimeZone::systemTimeZone()));
}

TEST_CASE("Reload a tweet") {
    set_profile_directory(TEST_PROFILE_DIR);

    Tweet t = Tweet::get_tweet_by_id(1413773185296650241);
    t.text = "Bad idea!";
    t.num_likes = 1000000000;
    t.is_conversation_scraped = false;

    t.reload_from_db();

    REQUIRE(t.text == "Good idea in theory, but in practice mostly graft");
    REQUIRE(t.num_likes == 8);
    REQUIRE(t.mentions.size() == 1);
    REQUIRE(t.reply_mentions.size() == 1);
    REQUIRE(t.is_conversation_scraped == true);
}

TEST_CASE("Reload tweets with content") {
    set_profile_directory(TEST_PROFILE_DIR);

    Tweet t_url = Tweet::get_tweet_by_id(1438642143170646017);
    REQUIRE(t_url.urls.size() == 3);
    t_url.reload_from_db();
    REQUIRE(t_url.urls.size() == 3);

    Tweet t_images = Tweet::get_tweet_by_id(1261483383483293700);
    REQUIRE(t_images.images.size() == 4);
    t_images.reload_from_db();
    REQUIRE(t_images.images.size() == 4);

    Tweet t_video = Tweet::get_tweet_by_id(1453461248142495744);
    REQUIRE(t_video.videos.size() == 1);
    t_video.reload_from_db();
    REQUIRE(t_video.videos.size() == 1);
}

/**
 * Test that TOMBSTONE_LABELS gets filled with tombstone types on profile load
 */
TEST_CASE("Should populate TOMBSTONE_LABELS") {
    set_profile_directory(TEST_PROFILE_DIR);

    REQUIRE(Tweet::TOMBSTONE_LABELS.size() > 3);  // Should have some stuff in it
    REQUIRE(Tweet::TOMBSTONE_LABELS[2] == "This Tweet is from a suspended account");
}

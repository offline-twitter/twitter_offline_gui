#ifndef UI_TOMBSTONE_WIDGET
#define UI_TOMBSTONE_WIDGET

#include <QLabel>
#include <QPaintEvent>

#include <memory>

#include "../models/tweet.h"

using std::shared_ptr;


class Tombstone : public QLabel {
    Q_OBJECT

 private:
    shared_ptr<Tweet> t;

 protected:
    void paintEvent(QPaintEvent*) override;

 public:
    explicit Tombstone(QWidget* parent = nullptr, shared_ptr<Tweet> t = nullptr);
};


#endif  // UI_TOMBSTONE_WIDGET

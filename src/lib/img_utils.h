// TODO qt5.15
// #include <QColorConstants>
#include <Qt>  // TODO qt5.15
#include <QColor>
#include <QPixmap>

#include <memory>

using std::shared_ptr;


#ifndef COLOR_CONSTANTS
#define COLOR_CONSTANTS

// TODO qt5.15: this is what it should be in QT 5.15.
// const QColor COLOR_TRANSPARENT = QColorConstants::Color0;
// const QColor COLOR_NON_TRANSPARENT = QColorConstants::Color1;

const QColor COLOR_TRANSPARENT = Qt::color0;
const QColor COLOR_NON_TRANSPARENT = Qt::color1;

#endif  // COLOR_CONSTANTS


void circlify(shared_ptr<QPixmap> pixmap);

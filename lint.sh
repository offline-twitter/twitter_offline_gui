#!/bin/bash

shopt -s globstar

cpplint --exclude=test/include_catch2 --exclude=**/gen* --linelength=100 **/*.cpp **/*.h

#include <QThread>

#include "include_catch2/catch.h"

#include "../src/models/_db.h"
#include "../src/models/tweet.h"

#define private public

#include "../src/ui/video_player.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::make_shared;



TEST_CASE("Should make a VideoPlayer with a gif") {
	set_profile_directory(TEST_PROFILE_DIR);

	Tweet t = Tweet::get_tweet_by_id(1453461248142495744);
	REQUIRE(t.videos.size() == 1);
	Video v = t.videos[0];
	REQUIRE(v.id == 1453461242698350592);
	REQUIRE(v.is_downloaded == true);
	REQUIRE(v.is_gif == true);

	VideoPlayer player(nullptr, make_shared<Video>(v));
	REQUIRE(player.media_player->media().isNull() == false);
	REQUIRE(player.progress_slider == nullptr);  // No progress slider on gifs
}

TEST_CASE("Should make a VideoPlayer with a regular video") {
	set_profile_directory(TEST_PROFILE_DIR);

	Tweet t = Tweet::get_tweet_by_id(1453461248142495744);
	REQUIRE(t.videos.size() == 1);
	Video v = t.videos[0];
	REQUIRE(v.id == 1453461242698350592);
	REQUIRE(v.is_downloaded == true);

	v.is_gif = false;  // Make it a regular video
	REQUIRE(v.is_gif == false);

	VideoPlayer player(nullptr, make_shared<Video>(v));
	REQUIRE(player.media_player->media().isNull() == false);
	REQUIRE(player.progress_slider != nullptr);  // Regular videos need a progress slider

	// TODO: The following lines fail for some reason.  It doesn't load no matter how long you wait
	// REQUIRE(player.media_player->isVideoAvailable() == true);
	// REQUIRE(player.media_player->duration() == 3000);
	// REQUIRE(player.progress_slider->minimum() == 0);
	// REQUIRE(player.progress_slider->maximum() == 3000);
}

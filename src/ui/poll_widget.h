#ifndef POLL_WIDGET_H
#define POLL_WIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPaintEvent>

#include <memory>
#include <vector>

#include "../models/poll.h"
#include "./components/large_button.h"

using std::shared_ptr;
using std::vector;

class PollOption : public QWidget {
    Q_OBJECT

 private:
    QString label;
    QString votes;
    float percentage;
    bool is_winner;

    QHBoxLayout* layout;
    QLabel* choice_label;
    QLabel* votes_label;

 protected:
    void paintEvent(QPaintEvent*) override;

 public:
    explicit PollOption(QWidget* parent = nullptr, QString label = "", QString votes = 0, float percentage = 0, bool is_winner = false);
};

class PollWidget : public QWidget {
    Q_OBJECT

 private:
    shared_ptr<Poll> p;

    QVBoxLayout* layout;
    vector<PollOption*> options;
    QLabel* metadata_label;

 protected:
    void paintEvent(QPaintEvent*) override;

 public:
    explicit PollWidget(QWidget* parent = nullptr, shared_ptr<Poll> p = nullptr);
};

#endif  // POLL_WIDGET_H

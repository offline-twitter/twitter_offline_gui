#include "video_preview.h"

#include <QPoint>
#include <QSizePolicy>
#include <QDesktopServices>
#include <QGuiApplication>
#include <QClipboard>
#include <QMenu>
#include <QPalette>
#include <QColor>
#include <QIcon>

#include "../lib/global_state.h"


VideoPreview::VideoPreview(QWidget* parent, shared_ptr<Video> video): QWidget(parent) {
	this->video = video;

	layout = new QVBoxLayout(this);
	setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);

	preview_image_label = new QLabel(this);
	preview_image_label->setToolTip("Click to open video");

	QPixmap pixmap(GlobalState::PROFILE_DIR + video->get_thumbnail_filepath());
	preview_image_label->setPixmap(pixmap);
	preview_image_label->setCursor(Qt::PointingHandCursor);
	preview_image_label->setMinimumWidth(400);
	layout->addWidget(preview_image_label);

	play_button_label = new QLabel(this);
	play_button_label->setPixmap(QPixmap(":/icons/play_icon.png"));
	play_button_label->setScaledContents(true);
	play_button_label->setCursor(Qt::PointingHandCursor);
	// TODO: mouse-over changes tint of play button?

	QPalette pal = palette();
	pal.setColor(QPalette::WindowText, QColor(100, 100, 100)); // TODO color

	metadata_label = new QLabel(this);
	QString meta_text = video->is_gif
		? "GIF"
		: QString("Video - %1 - %2 views").arg(video->render_duration()).arg(video->view_count);
	metadata_label->setText(meta_text);
	metadata_label->setPalette(pal);
	layout->addWidget(metadata_label);
}

void VideoPreview::open_in_external() {
	QDesktopServices::openUrl(QUrl(GlobalState::PROFILE_DIR + video->get_filepath()));
}

/**
 * Copy the filepath to the clipboard
 */
void VideoPreview::copy_video_path() {
	QGuiApplication::clipboard()->setText(GlobalState::PROFILE_DIR + video->get_filepath());
}


void VideoPreview::mousePressEvent(QMouseEvent* e) {
	if (e->button() == Qt::LeftButton) {
		open_in_external();
	} else {
		e->ignore();  // Propagate it
	}
}

void VideoPreview::contextMenuEvent(QContextMenuEvent* e) {
	QMenu menu(this);
	menu.addAction(QIcon(":/icons/link_icon.png"), "Copy video path", this, SLOT(copy_video_path()));
	menu.addSeparator();
	menu.addAction(QIcon(":/icons/open_external_icon.png"), "Open video", this, SLOT(open_in_external()));
	menu.exec(e->globalPos());
}

void VideoPreview::resizeEvent(QResizeEvent*) {
	// Center the play button over the preview image
	const int PLAY_BUTTON_SIZE = 100;
	QPoint center = preview_image_label->geometry().center();
	printf("%d, %d\n", center.x(), center.y());

	play_button_label->setGeometry(center.x() - PLAY_BUTTON_SIZE/2, center.y() - PLAY_BUTTON_SIZE/2, PLAY_BUTTON_SIZE, PLAY_BUTTON_SIZE);
}

#include "include_catch2/catch.h"

#include "../src/models/_db.h"
#include "../src/models/feeds/search.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"


bool is_ok;  // Common use

TEST_CASE("Test tokenizing a search string") {
    SearchParams result = SearchParams::parse_search_query("think", &is_ok);
    REQUIRE(is_ok == true);
    REQUIRE(result.keywords.size() == 1);
    REQUIRE(result.to_users.size() == 0);
    REQUIRE(result.keywords[0] == "think");
}

TEST_CASE("search string with multiple words") {
    SearchParams result = SearchParams::parse_search_query("think tank", &is_ok);
    REQUIRE(is_ok == true);
    REQUIRE(result.keywords.size() == 2);
    REQUIRE(result.keywords[0] == "think");
    REQUIRE(result.keywords[1] == "tank");
}

TEST_CASE("search string with quoted tokens") {
    SearchParams result = SearchParams::parse_search_query("\"think tank\"", &is_ok);
    REQUIRE(is_ok == true);
    REQUIRE(result.keywords.size() == 1);
    REQUIRE(result.keywords[0].toStdString() == "think tank");
}

TEST_CASE("search string with a 'from:XXXXX' token") {
    SearchParams result = SearchParams::parse_search_query("from:some_dude", &is_ok);
    REQUIRE(is_ok == true);
    REQUIRE(result.keywords.size() == 0);
    REQUIRE(result.from_user == "some_dude");
}

TEST_CASE("search string with a 'retweeted_by:XXXXX' token") {
    SearchParams result = SearchParams::parse_search_query("retweeted_by:some_dude", &is_ok);
    REQUIRE(is_ok == true);
    REQUIRE(result.keywords.size() == 0);
    REQUIRE(result.retweeted_by_user == "some_dude");
}

TEST_CASE("search string with a 'to:XXX' token") {
    SearchParams result = SearchParams::parse_search_query("to:some_dude", &is_ok);
    REQUIRE(is_ok == true);
    REQUIRE(result.keywords.size() == 0);
    REQUIRE(result.to_users.size() == 1);
    REQUIRE(result.to_users[0] == "some_dude");
}

TEST_CASE("A complex search string with mixed use") {
    SearchParams result = SearchParams::parse_search_query("stupid \"think tank\" from:kashi", &is_ok);
    REQUIRE(is_ok == true);
    REQUIRE(result.keywords.size() == 2);
    REQUIRE(result.keywords[0].toStdString() == "stupid");
    REQUIRE(result.keywords[1].toStdString() == "think tank");
    REQUIRE(result.from_user == "kashi");
    REQUIRE(result.get_original_query() == "stupid \"think tank\" from:kashi");
}

TEST_CASE("Search string with invalid quoting") {
    SearchParams result = SearchParams::parse_search_query("asdf \"fjk", &is_ok);
    REQUIRE(is_ok == false);
}

TEST_CASE("Search with dates") {
    SearchParams result = SearchParams::parse_search_query("since:2020-01-01 until:2020-05-01 from:cernovich", &is_ok);
    REQUIRE(is_ok == true);
    REQUIRE(result.from_user == "cernovich");
    REQUIRE(result.timestamp_from == yyyymmdd_to_timestamp("2020-01-01"));
    REQUIRE(result.timestamp_to == yyyymmdd_to_timestamp("2020-05-01"));
}

TEST_CASE("Search with dates but they're nonsense") {
    SearchParams result = SearchParams::parse_search_query("since:fawekf from:cernovich", &is_ok);
    REQUIRE(is_ok == false);  // Should fail to parse
}

TEST_CASE("Set image, url and video filters") {
    SearchParams result = SearchParams::parse_search_query("filter:links", &is_ok);
    REQUIRE(is_ok == true);
    REQUIRE(result.filter_links == true);
    REQUIRE(result.filter_images == false);
    REQUIRE(result.filter_videos == false);

    result = SearchParams::parse_search_query("filter:images", &is_ok);
    REQUIRE(is_ok == true);
    REQUIRE(result.filter_links == false);
    REQUIRE(result.filter_images == true);
    REQUIRE(result.filter_videos == false);

    result = SearchParams::parse_search_query("filter:videos", &is_ok);
    REQUIRE(is_ok == true);
    REQUIRE(result.filter_links == false);
    REQUIRE(result.filter_images == false);
    REQUIRE(result.filter_videos == true);
}

// Run the queries
// ---------------

TEST_CASE("Should search for some tweets") {
    set_profile_directory(TEST_PROFILE_DIR);

    SearchParams search = SearchParams();
    search.keywords << "think";
    vector<Tweet> results = search.execute();

    REQUIRE(results.size() == 5);
    REQUIRE(results[0].id == 1439027915404939265);
    REQUIRE(results[1].id == 1439067163508150272);
    REQUIRE(results[2].id == 1343633011364016128);
    REQUIRE(results[3].id == 1428939163961790466);
    REQUIRE(results[4].id == 1413772782358433792);
}

TEST_CASE("Execute query with multiple words") {
    set_profile_directory(TEST_PROFILE_DIR);

    SearchParams search = SearchParams();
    search.keywords << "think" << "but";
    vector<Tweet> results = search.execute();

    REQUIRE(results.size() == 3);
    REQUIRE(results[0].id == 1343633011364016128);
    REQUIRE(results[1].id == 1428939163961790466);
    REQUIRE(results[2].id == 1413772782358433792);
}

TEST_CASE("Execute nonsense query") {
    set_profile_directory(TEST_PROFILE_DIR);

    SearchParams search = SearchParams();
    search.keywords << "faweflawrlerfaer";
    vector<Tweet> results = search.execute();

    REQUIRE(results.size() == 0);
}

TEST_CASE("Should search for tweets with quoted text") {
   set_profile_directory(TEST_PROFILE_DIR);

    SearchParams search = SearchParams();

    // Without quotes
    search.keywords << "who" << "are";
    vector<Tweet> results = search.execute();
    REQUIRE(results.size() > 1);

    search.keywords.clear();

    // With quotes
    search.keywords << "who are";
    results = search.execute();
    REQUIRE(results.size() == 1);
    REQUIRE(results[0].id == 1261483383483293700);
}

TEST_CASE("Search for tweets from a user") {
    set_profile_directory(TEST_PROFILE_DIR);

    SearchParams search = SearchParams();
    search.from_user = "kwamurai";
    vector<Tweet> results = search.execute();

    REQUIRE(results.size() == 1);
    REQUIRE(results[0].id == 1343633011364016128);
}

TEST_CASE("Search for retweets from a user") {
    set_profile_directory(TEST_PROFILE_DIR);
    SearchParams search = SearchParams();
    search.retweeted_by_user = "cernovich";
    vector<Tweet> results = search.execute();

    REQUIRE(results.size() == 3);
    REQUIRE(results[0].id == 1490116725395927042);
    REQUIRE(results[1].id == 1490120332484972549);
    REQUIRE(results[2].id == 1489944024278523906);

    search.from_user = "thevivafrei";
    vector<Tweet> results2 = search.execute();

    REQUIRE(results2.size() == 1);
    REQUIRE(results2[0].id == 1489944024278523906);

}

TEST_CASE("Search for tweets replying to a user") {
    set_profile_directory(TEST_PROFILE_DIR);

    SearchParams search = SearchParams();
    search.to_users.push_back("spacex");
    vector<Tweet> results = search.execute();

    REQUIRE(results.size() == 2);
    REQUIRE(results[0].id == 1428951883058753537);
    REQUIRE(results[1].id == 1428939163961790466);

    search.to_users.push_back("covfefeanon");
    vector<Tweet> results2 = search.execute();
    REQUIRE(results2.size() == 1);
    REQUIRE(results2[0].id == 1428939163961790466);
}

TEST_CASE("Search for tweets from a user with keyword filter") {
    set_profile_directory(TEST_PROFILE_DIR);

    SearchParams search = SearchParams();
    search.keywords << "think";
    search.from_user = "cernovich";
    vector<Tweet> results = search.execute();

    REQUIRE(results.size() == 2);
    REQUIRE(results[0].id == 1439027915404939265);
    REQUIRE(results[1].id == 1439067163508150272);
}

TEST_CASE("Search for tweets using date filters") {
    set_profile_directory(TEST_PROFILE_DIR);

    SearchParams search = SearchParams();
    search.from_user = "cernovich";
    search.timestamp_from = yyyymmdd_to_timestamp("2021-10-01");
    vector<Tweet> results = search.execute();

    REQUIRE(results.size() == 1);
    REQUIRE(results[0].id == 1453461248142495744);

    search.timestamp_from = 0;
    search.timestamp_to = yyyymmdd_to_timestamp("2021-10-01");
    vector<Tweet> results2 = search.execute();

    REQUIRE(results2.size() == 3);
    REQUIRE(results2[0].id == 1439027915404939265);
    REQUIRE(results2[1].id == 1439068749336748043);
    REQUIRE(results2[2].id == 1439067163508150272);


    // Should return nothing
    search.timestamp_from = yyyymmdd_to_timestamp("2021-10-01");
    search.timestamp_to = yyyymmdd_to_timestamp("2021-10-01");
    vector<Tweet> results3 = search.execute();
    REQUIRE(results3.size() == 0);
}

TEST_CASE("Search for tweets with links") {
   set_profile_directory(TEST_PROFILE_DIR);

   SearchParams search = SearchParams();
   search.filter_links = true;
   vector<Tweet> results = search.execute();

   REQUIRE(results.size() == 2);
   REQUIRE(results[0].id == 1438642143170646017);
   REQUIRE(results[1].id == 1413665734866186243);
}

TEST_CASE("Search for tweets with images") {
   set_profile_directory(TEST_PROFILE_DIR);

   SearchParams search = SearchParams();
   search.filter_images = true;
   vector<Tweet> results = search.execute();

   REQUIRE(results.size() == 2);
   REQUIRE(results[0].id == 1261483383483293700);
   REQUIRE(results[1].id == 1426669666928414720);
}

TEST_CASE("Search for tweets with videos") {
   set_profile_directory(TEST_PROFILE_DIR);

   SearchParams search = SearchParams();
   search.filter_videos = true;
   vector<Tweet> results = search.execute();

   REQUIRE(results.size() == 2);
   REQUIRE(results[0].id == 1426619468327882761);
   REQUIRE(results[1].id == 1453461248142495744);
}

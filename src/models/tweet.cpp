#include "tweet.h"

#include <QSqlError>
#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include <vector>
#include <sstream>
#include <cmath>

#include "./user.h"
#include "./search.h"
#include "./_db.h"

#include "../lib/global_state.h"

using std::make_shared;

QMap<int, QString> Tweet::TOMBSTONE_LABELS = QMap<int, QString>();

Image::Image() {}

QString Image::get_filepath() const {
    return "/images/" + local_filename;
}

Video::Video() {}

QString Video::get_filepath() const {
    return "/videos/" + local_filename;
}

QString Video::get_thumbnail_filepath() const {
    return "/video_thumbnails/" + thumbnail_filename;
}

QString Video::render_duration() const {
    int minutes = duration_millis / 60000;
    if (minutes != 0) {
        int seconds = round((duration_millis - minutes*60000) / 1000.0);
        return QString::asprintf("%dm%.02ds", minutes, seconds);
    } else {
        float seconds_10x = duration_millis / 100.0;
        return QString::number(round(seconds_10x)/10) + "s";
    }
}

Tweet::Tweet() {}

const QString Tweet::SQL_TWEET_FIELDS = "id, user_id, text, posted_at, num_likes, num_retweets, "
    "num_replies, num_quote_tweets, in_reply_to_id, quoted_tweet_id, mentions, reply_mentions, "
    "space_id, is_content_downloaded, is_conversation_scraped, last_scraped_at, "
    "tombstone_type, is_stub";

void Tweet::extract_tweet_from_query_row(QSqlQuery query, Tweet* t) {
    t->id = query.value(0).toULongLong();
    t->user_id = query.value(1).toULongLong();
    t->text = query.value(2).toString();
    t->posted_at = query.value(3).toInt();
    t->num_likes = query.value(4).toInt();
    t->num_retweets = query.value(5).toInt();
    t->num_replies = query.value(6).toInt();
    t->num_quote_tweets = query.value(7).toInt();
    t->in_reply_to_id = query.value(8).toULongLong();
    t->quoted_tweet_id = query.value(9).toULongLong();

    // If there are none, it will return `['']`.
    // Qt::SkipEmptyParts was introduced in Qt 5.14, but I have 5.12.
    t->mentions.clear();
    if (query.value(10).toString().size() > 0) {
        QStringList tmp = query.value(10).toString().split(",");
        for (QString s : tmp) {
            t->mentions.push_back(s);
        }
    }
    t->reply_mentions.clear();
    if (query.value(11).toString().size() > 0) {
        QStringList tmp = query.value(11).toString().split(",");
        for (QString s : tmp) {
            t->reply_mentions.push_back(s);
        }
    }
    t->space_id = query.value(12).toString();
    t->is_content_downloaded = query.value(13).toInt();
    t->is_conversation_scraped = query.value(14).toBool();
    t->last_scraped_at = query.value(15).toInt();
    t->tombstone_type = query.value(16).toInt();
    t->is_stub = query.value(17).toBool();
}

void Tweet::add_extras_to_tweet(Tweet* t) {
    QSqlQuery query(DB);

    t->urls = Url::get_urls_for_tweet_id(t->id);
    t->polls = Poll::get_polls_for_tweet_id(t->id);
    if (t->space_id != "") {
        t->space = make_shared<Space>(Space::get_space_by_id(t->space_id));
    }

    t->hashtags.clear();
    query.prepare("select text from hashtags where tweet_id = :tweet_id");
    query.bindValue(":tweet_id", (long long) t->id);
    query.exec();
    while (query.next()) {
        t->hashtags.push_back(query.value(0).toString());  // TODO weird-hashtags: there's a bunch of empty hashtags in the db
    }

    t->images.clear();
    query.prepare(
        "select id, width, height, remote_url, local_filename, is_downloaded"
        " from images where tweet_id = :tweet_id"
    );
    query.bindValue(":tweet_id", (long long) t->id);
    query.exec();
    while (query.next()) {
        Image i;
        i.id = query.value(0).toULongLong();
        i.width = query.value(1).toInt();
        i.height = query.value(2).toInt();
        i.remote_url = query.value(3).toString();
        i.local_filename = query.value(4).toString();
        i.is_downloaded = query.value(5).toInt();

        t->images.push_back(i);
    }

    t->videos.clear();
    query.prepare(
        "select id, width, height, remote_url, local_filename, thumbnail_local_filename, duration, view_count, is_downloaded, is_gif"
        " from videos where tweet_id = :tweet_id"
    );
    query.bindValue(":tweet_id", (long long) t->id);
    query.exec();
    while (query.next()) {
        Video v;
        v.id = query.value(0).toULongLong();
        v.width = query.value(1).toInt();
        v.height = query.value(2).toInt();
        v.remote_url = query.value(3).toString();
        v.local_filename = query.value(4).toString();
        v.thumbnail_filename = query.value(5).toString();
        v.duration_millis = query.value(6).toInt();
        v.view_count = query.value(7).toInt();
        v.is_downloaded = query.value(8).toBool();
        v.is_gif = query.value(9).toBool();

        t->videos.push_back(v);
    }
}

QSqlQuery Tweet::query_db_for_tweet(TweetID id) {
    QSqlQuery query(DB);
    bool status = query.prepare("select " + Tweet::SQL_TWEET_FIELDS + " from tweets where id = :id");
    if (!status) {
        // TODO: should crash, or produce an error
        printf("Failed to prepare!\n");
        printf("%s\n", query.lastError().text().toStdString().c_str());
        printf("%s\n", query.lastQuery().toStdString().c_str());
    }
    query.bindValue(":id", (long long) id);
    if (!query.exec()) {
        // TODO: should crash, or produce an error
        printf("Failed to execute!\n");
        printf("%s\n", query.lastError().text().toStdString().c_str());
    }

    if (!query.next()) {
        throw DBException("Could not find tweet with id: " + QString::number(id));
    }
    return query;
}

Tweet Tweet::get_tweet_by_id(TweetID id) {
    QSqlQuery query = Tweet::query_db_for_tweet(id);
    Tweet t;
    Tweet::extract_tweet_from_query_row(query, &t);
    Tweet::add_extras_to_tweet(&t);
    return t;
}

/**
 * Get all tweets from followed users that were posted within the given time period
 */
vector<Tweet> Tweet::get_followed_tweets_since(int min_posted_at, int max_posted_at) {
    vector<Tweet> ret;

    QSqlQuery query(DB);
    query.prepare("select " + Tweet::SQL_TWEET_FIELDS + " from tweets where user_id in (select id from users where is_followed = 1)"
        + (min_posted_at > 0 ? " and posted_at >= :min_posted_at" : "")
        + (max_posted_at > 0 ? " and posted_at < :max_posted_at" : "")
        + " order by posted_at desc");
    query.bindValue(":min_posted_at", min_posted_at);
    query.bindValue(":max_posted_at", max_posted_at);
    if (!query.exec()) {
        throw DBException("Failed to execute get_followed_tweets_since query");
    }

    while (query.next()) {
        Tweet t;
        Tweet::extract_tweet_from_query_row(query, &t);
        Tweet::add_extras_to_tweet(&t);
        ret.push_back(t);
    }
    return ret;
}


// Accessors
// ---------

shared_ptr<User> Tweet::get_user() {
    if (this->user == nullptr) {
        User tmp = User::get_user_by_id(this->user_id);
        this->user = make_shared<User>(tmp);
    }
    return this->user;
}

shared_ptr<Tweet> Tweet::get_quoted_tweet() const {
    return make_shared<Tweet>(Tweet::get_tweet_by_id(this->quoted_tweet_id));
}

shared_ptr<Tweet> Tweet::get_replied_tweet() const {
    return make_shared<Tweet>(Tweet::get_tweet_by_id(this->in_reply_to_id));
}

vector<shared_ptr<Tweet>> Tweet::get_replies() const {
    return get_replies(-1);
}

/**
 * Return a set of replies to this tweet, up to `limit`.
 */
vector<shared_ptr<Tweet>> Tweet::get_replies(int limit) const {
    QSqlQuery query(DB);
    vector<shared_ptr<Tweet>> ret;

    query.prepare("select " + Tweet::SQL_TWEET_FIELDS + " from tweets where in_reply_to_id = :id order by num_likes + num_retweets desc limit :limit");
    query.bindValue(":id", (long long) id);
    query.bindValue(":limit", limit);
    query.exec();

    while (query.next()) {
        Tweet t;
        Tweet::extract_tweet_from_query_row(query, &t);
        Tweet::add_extras_to_tweet(&t);
        ret.push_back(make_shared<Tweet>(t));
    }
    return ret;
}


/**
 * Returns tweets forming a thread below this tweet, if applicable, or an empty vector otherwise
 */
vector<shared_ptr<Tweet>> Tweet::get_thread() const {
    QSqlQuery query(DB);
    vector<shared_ptr<Tweet>> ret;

    query.prepare("select " + Tweet::SQL_TWEET_FIELDS + " from tweets where in_reply_to_id = :id and reply_mentions = '' and is_stub = 0 order by num_likes + num_retweets desc limit 1");
    query.bindValue(":id", (long long) id);
    query.exec();

    while (query.next()) {
        Tweet t;
        Tweet::extract_tweet_from_query_row(query, &t);
        Tweet::add_extras_to_tweet(&t);
        ret.push_back(make_shared<Tweet>(t));

        query.bindValue(":id", (long long) t.id);
        query.exec();
    }
    return ret;
}


// Helpers
// -------

QString Tweet::get_replying_to_text() const {
    if (reply_mentions.size() == 0 && !is_stub) {
        return "<thread>";
    }

    QString reply_text = "Replying to: ";

    for (uint i = 0; i < reply_mentions.size(); i++) {
        if (i > 0) {
            reply_text += " \u22c5 ";
        }
        reply_text += "@" + reply_mentions[i];
    }
    return reply_text;
}

QString Tweet::get_permalink() {
    return "https://twitter.com/" + get_user()->handle + "/status/" + QString::number(id);
}

TweetID Tweet::parse_url(QString s, bool* is_ok) {
    QRegularExpression url_regexp("https://twitter.com/\\w+/status/(\\d+)");
    QRegularExpressionMatch match = url_regexp.match(s);
    if (match.hasMatch()) {
        return match.captured(1).toULongLong(is_ok);
    } else {
        *is_ok = false;
        return 0;
    }
}

TweetID Tweet::parse_id(QString s, bool* is_ok) {
    QRegularExpression id_regexp("^(\\d+)$");
    QRegularExpressionMatch match = id_regexp.match(s);
    if (match.hasMatch()) {
        return match.captured(1).toULongLong(is_ok);
    } else {
        *is_ok = false;
        return 0;
    }
}

QString Tweet::render_posted_at() const {
    return Tweet::render_posted_at(posted_at, QTimeZone::systemTimeZone());
}

/**
 * Use dependency injection for the timezone.  Makes it testable without depending on local time.
 */
QString Tweet::render_posted_at(int posted_at, QTimeZone timezone) {
    QDateTime posted_at_datetime = QDateTime::fromSecsSinceEpoch(posted_at).toTimeZone(timezone);
    return posted_at_datetime.toString("MMM d, yyyy\nh:mm a").remove(".");
}

void Tweet::reload_from_db() {
    QSqlQuery query = Tweet::query_db_for_tweet(id);
    Tweet::extract_tweet_from_query_row(query, this);
    Tweet::add_extras_to_tweet(this);
}

/**
 * Called when the profile / database is loaded
 */
void Tweet::init_tombstone_types() {
    Tweet::TOMBSTONE_LABELS.clear();

    QSqlQuery query(DB);
    query.prepare("select rowid, tombstone_text from tombstone_types");
    query.exec();
    while (query.next()) {
        Tweet::TOMBSTONE_LABELS[query.value(0).toInt()] = query.value(1).toString();
    }
}


// Engine calls
// ------------

/**
 * Call the engine to download this thread
 */
int Tweet::download_conversation() {
    int ret = Tweet::download_conversation(id);
    if (ret == 0) {
        reload_from_db();
    }
    return ret;
}

/**
 * Call the engine to download the thread for the given TweetID
 */
int Tweet::download_conversation(TweetID tweet_id) {
    return GlobalState::TWITTER_ENGINE.call(
        "twitter",
        QStringList() << "--profile" << GlobalState::PROFILE_DIR << "fetch_tweet" << QString::number(tweet_id)
    );
}

/**
 * Call the engine to download contents for this tweet
 */
int Tweet::download_contents() {
    return GlobalState::TWITTER_ENGINE.call(
        "twitter",
        QStringList() << "--profile" << GlobalState::PROFILE_DIR << "download_tweet_content" << QString::number(id)
    );
}

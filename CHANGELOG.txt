Changelog
=========

v0.0.0
------

- Initial release.

v0.0.1
------

- Upgrade engine version from v0.0.0 to v0.0.1
- Add (currently pointless / unused) parsing of the `settings.yaml` file
- Add navigation between tweets and engine calls (i.e., app is now usable for browsing, lol)
- Add the ability to create a new profile in-app
- Add Welcome Screen
	- No longer opens some random directory automatically on application start.  Instead, prompts
	  user to choose a directory.

v0.0.2
------

- Add help menus

v0.0.3
------

- Add polls
- Videos now display duration and view counts
- Videos now display as a preview; can be clicked to open in external viewer
- No longer scrapes full-sized profile images for every user, only followed users and users whomst profile you visit
- Add .desktop files for opendesktop.org compliant systems

v0.0.4
------

- Make polls look much, much better
- Add support for banned users
- Now tracks whether parents + replies have been downloaded for every individual tweet

v0.0.5
------

- Fix embedded link background not respecting rounded borders
- Make entities (@mentions and #hashtags) turn twitter-blue and clickable
- Render tombstones

v0.0.6
------

- BUGFIX: fix newlines not rendering in tweets with @mentions or #hashtags
- BUGFIX: fix search bar not recognizing user handles with numbers in them
- Add right-click context menu on tweets
- Try to identify threads and label them as "<thread>"
- Fixed focused tweets having a pointer cursor, they now have a regular arrow cursor (since they are not clickable)

v0.0.7
------

- Allow downloading of user profile images via user profile header
- Render threads
- Render replies 2 levels deep

v0.0.8
------

- Paginate user feeds
- Add warning message box for engine call failures
- BUGFIX: fixed duplicate images/videos appearing in tweet after downloading a conversation

v0.0.9
------

- Add following users
- Remove YAML dependency

v0.0.10
-------

- Fix image sizing in feeds (finally!)
- Fixed URLs without cards not rendering

v0.0.11
-------

- Fix image sizing again: images no longer have large amounts of extra space above and below when they get shrunk
- Search bar now requires `@` before a user handle
- Prevent "Close current session" warning popup after creating a new profile if you don't have a session already open

v0.0.12
-------

- Add a "Followed Users" page
- Add timeline page
- Open timeline by default after opening a profile
- Add navbar buttons to open the timeline / followed-users pages
- Move the navbar "title" element to the window title

v0.0.13
-------

- Finally fix the crashing problem!  App no longer crashes when loading a thread with missing parent tweets.
- Add context menus for user profile headers and embedded links
- Enable clickable entities (@handles and #hashtags) in user bio
- Add a bunch of icons in a number of different places
	- location, website, and [newly added] joined-at fields in user profile header
	- context menu items relating to copying something

v0.0.14
-------

- Add search!
- Add quote-tweet counter on tweets
- Some context menu / icon improvement
- Website links on user pages can now be clicked

v0.1.0
------

- Create a profile in a default location (AppData directory) on starting
- Render a welcome message when the timeline is empty (e.g., opened app for the first time)
- Removed useless buttons and improved context menus
- BUGFIX: fixed crash when search query is empty

v0.1.1
------

- Rewrite "Help" menu to include version info

v0.1.2
------

- Add search filter "since:[date]" and "until:[date]"
- Add search filter "to:[user_handle]"

v0.1.3
------

- Add Spaces

v0.1.4
------

- Improve search
	- add sorting options (most likes, most retweets, newest, oldest)
	- add filters for retweeted_by, filter:links, filter:images, and filter:videos
	- make searches scrollable

#include "img_utils.h"

#include <QPainter>
#include <QBitmap>
#include <QPen>

#include "../ui/colors.h"


void circlify(shared_ptr<QPixmap> pixmap) {
    QBitmap mask(pixmap->width(), pixmap->height());
    mask.fill(COLOR_TRANSPARENT);

    // Make a circle of non-transparent
    QPainter p(&mask);
    p.setBrush(COLOR_NON_TRANSPARENT);
    p.drawEllipse(0, 0, pixmap->width() - 1, pixmap->height() - 1);

    pixmap->setMask(mask);

    // Add a gray outline
    QPainter p_outline(pixmap.get());
    QPen pen(COLOR_OUTLINE_GRAY);
    pen.setWidth(2);
    p_outline.setPen(pen);

    p_outline.drawEllipse(0, 0, pixmap->width(), pixmap->height());
}

#ifndef IMAGE_WIDGET_H
#define IMAGE_WIDGET_H

#include <QString>
#include <QWidget>
#include <QLabel>
#include <QPixmap>
#include <QMouseEvent>
#include <QContextMenuEvent>
#include <QResizeEvent>
#include <QSize>


// TODO: add a rounded border around each image

/**
 * A custom QLabel holding an image, adding a right-click context menu.
 */
class ImageWidget : public QLabel {
	Q_OBJECT

 public:
	ImageWidget(QWidget* parent = nullptr, QString filepath = "");
	QSize sizeHint() const override;

 private slots:
	void open_in_external();
	void copy_image();
	void copy_image_path();

 protected:
	void mousePressEvent(QMouseEvent* e);
	void contextMenuEvent(QContextMenuEvent* e);
	void resizeEvent(QResizeEvent*);

 private:
	QString filepath;
	QPixmap _pixmap;
};

#endif  // IMAGE_WIDGET_H

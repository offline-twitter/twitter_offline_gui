#include "include_catch2/catch.h"

#include "../src/models/tweet.h"
#include "../src/models/_db.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

TEST_CASE("Should get an image's filepath") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1261483383483293700;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    ImageID img_id = 1261483377363791872;

    REQUIRE(t.id == tweet_id);
    REQUIRE(t.images[0].id == img_id);
    REQUIRE(t.images[0].get_filepath() == "/images/EYGwcrXUMAAiyCf.jpg");
    REQUIRE(t.images[0].width == 1914);
    REQUIRE(t.images[0].height == 1456);
}

#include "include_catch2/catch.h"

#include "../src/models/_db.h"
#include "../src/models/feeds/feed.h"

#define private public

#include "../src/ui/tweet_widget.h"
#include "../src/ui/components/tweet_feed.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::make_shared;


TEST_CASE("Should make a TweetFeed") {
	set_profile_directory(TEST_PROFILE_DIR);

	Feed f = Feed::get_timeline(5, -1);  // Limit 5 tweets, no max_posted_at (i.e., just the 5 newest)
	TweetFeed tweet_feed(f);

    REQUIRE(tweet_feed.get_oldest_timestamp() == f.get_oldest_timestamp());
	REQUIRE(tweet_feed.layout->count() == 2);  // 0) tweets; 1) LoadMoreTweets button
	REQUIRE(tweet_feed.tweets_layout->count() == 5);

	// Check last tweet
	REQUIRE(tweet_feed.tweets_layout->itemAt(4)->widget() != nullptr);
	REQUIRE(static_cast<TweetWidget*>(tweet_feed.tweets_layout->itemAt(4)->widget())->t->id == 1453461248142495744);
}

/**
 * Test pagination of the TweetFeed.  New tweets should be added at the bottom.  The "load more
 * tweets" button should get disabled once the end of feed is reached.
 */
TEST_CASE("Should paginate a TweetFeed") {
	set_profile_directory(TEST_PROFILE_DIR);

	Feed f = Feed::get_timeline(4, -1);  // Limit 4 tweets, no max_posted_at (i.e., just the 5 newest)
	TweetFeed tweet_feed(f);
	REQUIRE(tweet_feed.tweets_layout->count() == 4);
	REQUIRE(tweet_feed.load_more_tweets_button->isEnabled() == true);

	// Page down until the end of the feed...
	tweet_feed.extend_with(Feed::get_timeline(4, tweet_feed.get_oldest_timestamp()));
	REQUIRE(tweet_feed.tweets_layout->count() == 8);
	REQUIRE(tweet_feed.load_more_tweets_button->isEnabled() == true);

	tweet_feed.extend_with(Feed::get_timeline(4, tweet_feed.get_oldest_timestamp()));
	REQUIRE(tweet_feed.tweets_layout->count() == 12);
	REQUIRE(tweet_feed.load_more_tweets_button->isEnabled() == true);

	tweet_feed.extend_with(Feed::get_timeline(4, tweet_feed.get_oldest_timestamp()));
	REQUIRE(tweet_feed.tweets_layout->count() == 16);
	REQUIRE(tweet_feed.load_more_tweets_button->isEnabled() == true);

	tweet_feed.extend_with(Feed::get_timeline(4, tweet_feed.get_oldest_timestamp()));
	REQUIRE(tweet_feed.tweets_layout->count() == 18);
	REQUIRE(tweet_feed.load_more_tweets_button->isEnabled() == true);

	// This should be the end of the feed.  Now, page down 1 more time to check end-of-feed handling
	tweet_feed.extend_with(Feed::get_timeline(4, tweet_feed.get_oldest_timestamp()));
	REQUIRE(tweet_feed.tweets_layout->count() == 18);  // Same as before
	REQUIRE(tweet_feed.load_more_tweets_button->isEnabled() == false);  // Button is disabled
}

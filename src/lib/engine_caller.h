#ifndef ENGINE_CALLER_H
#define ENGINE_CALLER_H

#include <QString>
#include <QStringList>


/**
 * A wrapper class around QProcess, which can function as a low-grade test double.
 */
class Engine {
 private:
    // Next-level dependency injection!
    bool is_fake;
    int fake_return_code;
    explicit Engine(bool is_fake = false, int fake_return_code = 0);

    friend class GlobalState;

 public:
    int call(QString cmd, QStringList args);
    int init();
};

#endif  // ENGINE_CALLER_H

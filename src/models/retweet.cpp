#include "retweet.h"

#include "./_db.h"
#include "./user.h"

const QString Retweet::SQL_RETWEET_FIELDS = "retweet_id, tweet_id, retweeted_by, retweeted_at";

Retweet::Retweet() {}

Retweet Retweet::extract_retweet_from_query_row(QSqlQuery query) {
	Retweet r;
	r.id = query.value(0).toULongLong();
	r.tweet_id = query.value(1).toULongLong();
	r.retweeted_by_id = query.value(2).toULongLong();
	r.retweeted_at = query.value(3).toInt();
	return r;
}

Retweet Retweet::get_retweet_by_id(RetweetID id) {
	QSqlQuery query(DB);
	bool status = query.prepare("select " + Retweet::SQL_RETWEET_FIELDS + " from retweets where retweet_id = :id");
	if (!status) {
		printf("Failed to prepare retweet query!\n");
	}
	query.bindValue(":id", (long long) id);
	if (!query.exec()) {
		printf("Failed to execute retweet query!\n");
	}
	if (!query.next()) {
		throw DBException("Could not find retweet with id: " + QString::number(id));
	}

	return Retweet::extract_retweet_from_query_row(query);
}

shared_ptr<User> Retweet::get_user() {
	if (this->user == nullptr) {
		this->user = make_shared<User>(User::get_user_by_id(this->retweeted_by_id));
	}
	return this->user;
}

shared_ptr<Tweet> Retweet::get_tweet() {
	if (this->tweet == nullptr) {
		this->tweet = make_shared<Tweet>(Tweet::get_tweet_by_id(this->tweet_id));
	}
	return this->tweet;
}

/**
 * Get all retweets from followed users that were retweeted within the given time period
 */
vector<Retweet> Retweet::get_followed_retweets_since(int min_retweeted_at, int max_retweeted_at) {
	vector<Retweet> ret;

	QSqlQuery query(DB);
	query.prepare("select " + Retweet::SQL_RETWEET_FIELDS + " from retweets where retweeted_by in (select id from users where is_followed = 1)"
        + (min_retweeted_at > 0 ? " and retweeted_at >= :min_retweeted_at" : "")
        + (max_retweeted_at > 0 ? " and retweeted_at < :max_retweeted_at" : "")
        + " order by retweeted_at desc");
	query.bindValue(":min_retweeted_at", min_retweeted_at);
	query.bindValue(":max_retweeted_at", max_retweeted_at);

	if (!query.exec()) {
		throw DBException("Failed to execute get_followed_retweets_since query");
	}
	while (query.next()) {
		ret.push_back(Retweet::extract_retweet_from_query_row(query));
	}
	return ret;
}

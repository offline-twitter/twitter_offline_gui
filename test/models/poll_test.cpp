#include <QDateTime>

#include "include_catch2/catch.h"

#include "../src/models/_db.h"

#define private public

#include "../src/models/poll.h"

TEST_CASE("Should compute a poll's total votes correctly") {
	Poll p;
	p.votes.push_back(1000);
	p.votes.push_back(500);
	REQUIRE(p.total_votes() == 1500);

	p.votes.push_back(1500);
	REQUIRE(p.total_votes() == 3000);

	p.votes[2] = 1000;
	REQUIRE(p.total_votes() == 2500);
}

TEST_CASE("Should render a percentage for a poll") {
	Poll p;
	p.votes.push_back(1000);
	p.votes.push_back(500);

	REQUIRE(p.format_votes_count(p.votes[0]).toStdString() == "1000 (66.6%)");

	p.votes.push_back(500);
	REQUIRE(p.format_votes_count(p.votes[2]).toStdString() == "500 (25%)");
}

TEST_CASE("Should report the correct winner") {
	Poll p;
	p.votes.push_back(2300);
	REQUIRE(p.get_winner() == 0);

	p.votes.push_back(2400);
	REQUIRE(p.get_winner() == 1);

	p.votes[1] = 2000;
	REQUIRE(p.get_winner() == 0);

	p.votes.push_back(10000);
	REQUIRE(p.get_winner() == 2);
}

TEST_CASE("Should render a poll's closing time") {
	REQUIRE(Poll::render_closes_at(1632432755, QTimeZone::utc()) == "Sep 23, 2021 9:32 pm");

	Poll p;
	p.voting_ends_at = 1632432755;
	REQUIRE(p.render_closes_at() == Poll::render_closes_at(p.voting_ends_at, QTimeZone::systemTimeZone()));
}

TEST_CASE("Should check if a poll is closed") {
	Poll p;
	p.voting_ends_at = QDateTime::currentSecsSinceEpoch() - 10;
	REQUIRE(p.is_closed() == true);
	p.voting_ends_at = QDateTime::currentSecsSinceEpoch() + 10;
	REQUIRE(p.is_closed() == false);
}

#define private public

#include "include_catch2/catch.h"

#include "../src/ui/navbar.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::make_shared;


TEST_CASE("Should make a Navbar") {
	Navbar n(nullptr);
	REQUIRE(n.back_button->pixmap.isNull() == false);

	n.set_navigation_depth(0);
	REQUIRE(n.depth_label->text() == "0");
	n.set_navigation_depth(3);
	REQUIRE(n.depth_label->text() == "3");
}

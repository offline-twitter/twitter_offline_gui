#ifndef WELCOME_SCREEN_H
#define WELCOME_SCREEN_H

#include <QWidget>
#include <QHBoxLayout>

#include "./components/large_button.h"

class WelcomeScreen : public QWidget {
    Q_OBJECT

 private:
    QHBoxLayout* layout;

    LargeButton* new_profile_button;
    LargeButton* open_profile_button;

 public:
    explicit WelcomeScreen(QWidget* parent = nullptr);
};

#endif  // WELCOME_SCREEN_H

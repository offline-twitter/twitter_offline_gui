#include "include_catch2/catch.h"

#include "../src/models/_db.h"
#include "../src/models/tweet.h"

#define private public

#include "../src/ui/video_preview.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::make_shared;


TEST_CASE("Should make a VideoPreview with a gif") {
	set_profile_directory(TEST_PROFILE_DIR);

	Tweet t = Tweet::get_tweet_by_id(1453461248142495744);
	REQUIRE(t.videos.size() == 1);
	Video v = t.videos[0];
	REQUIRE(v.id == 1453461242698350592);
	REQUIRE(v.is_gif == true);

	VideoPreview previewer(nullptr, make_shared<Video>(v));
	REQUIRE(previewer.metadata_label->text() == "GIF");
}

TEST_CASE("Should make a VideoPreview with a regular video") {
	set_profile_directory(TEST_PROFILE_DIR);

	Tweet t = Tweet::get_tweet_by_id(1426619468327882761);
	REQUIRE(t.videos.size() == 1);
	Video v = t.videos[0];
	REQUIRE(v.id == 1426619366829924358);
	REQUIRE(v.is_gif == false);

	VideoPreview previewer(nullptr, make_shared<Video>(v));
	REQUIRE(previewer.metadata_label->text() == "Video - 22.2s - 185404 views");
}

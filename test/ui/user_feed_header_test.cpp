#include <QSignalSpy>
#include <QString>
#include <QStringList>

#include "include_catch2/catch.h"


#include "../src/models/_db.h"

#define private public
#define protected public
#include "../src/ui/user_feed_header.h"

#include "./ui_test_utils.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::make_shared;


TEST_CASE("Should make a UserFeedHeader") {
	set_profile_directory(TEST_PROFILE_DIR);
	User u = User::get_user_by_handle("kwamurai");
	u.location = "some location";
	u.website = "some website";
	REQUIRE(u.is_content_downloaded);
	UserFeedHeader header(nullptr, make_shared<User>(u));

	REQUIRE(header.banner_image_label->pixmap()->isNull() == false);
	REQUIRE(header.author_info_panel->u->id == u.id);

	REQUIRE(u.is_followed == false);  // Double check
	REQUIRE(header.follow_button->text() == "Follow");
	REQUIRE(header.download_button->icon().isNull() == false);

	REQUIRE(header.bio_label->text() == u.bio);
	REQUIRE(header.location_label->text() == u.location);
	QString styled_header_link = "<a href='some website' style='text-decoration: none; color: rgb(27,149,224)'>some website</a>";
	REQUIRE(header.website_label->text() == styled_header_link);
	REQUIRE(header.joined_at_label->text() == "Joined: Mar 2017");
	REQUIRE(header.followers_count_label->text() == "11704");
	REQUIRE(header.following_count_label->text() == "370");
}

/**
 * Ensure the "Follow" button works as expected
 */
TEST_CASE("Should toggle the follow button") {
	set_profile_directory(TEST_PROFILE_DIR);
	shared_ptr<User> u = make_shared<User>(User::get_user_by_handle("kwamurai"));
	UserFeedHeader header(nullptr, u);

	REQUIRE(u->is_followed == false);
	REQUIRE(header.follow_button->text() == "Follow");

	// Click "Follow"
	header.follow_button_clicked();
	REQUIRE(u->is_followed == true);
	REQUIRE(header.follow_button->text() == "Following");

	// Click again to unfollow
	header.follow_button_clicked();
	REQUIRE(u->is_followed == false);
	REQUIRE(header.follow_button->text() == "Follow");
}

TEST_CASE("Entities in bio should be clickable") {
	set_profile_directory(TEST_PROFILE_DIR);
	shared_ptr<User> u = make_shared<User>(User::get_user_by_handle("kwamurai"));

	QStringList test_cases = QStringList() << "@michaelmalice" << "#SomeHashtag";
	for (QString text: test_cases ) {
		u->bio = text;
		UserFeedHeader header(nullptr, u);

		QSignalSpy spy(header.bio_label, SIGNAL(linkActivated(QString)));
		REQUIRE(spy.count() == 0);

		QMouseEvent click_event = get_left_click();
		header.bio_label->mousePressEvent(&click_event);
		REQUIRE(spy.count() == 1);
		REQUIRE(spy.takeFirst().at(0).toString() == text);
	}
}

TEST_CASE("Format a joined_at date") {
	int test_time = 1649725801;
	REQUIRE(UserFeedHeader::format_joined_at(test_time) == "Apr 2022");
}

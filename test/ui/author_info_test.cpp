#include <QPixmap>
#include <QSignalSpy>

#include <memory>

#include "include_catch2/catch.h"

#include "../src/models/tweet.h"
#include "../src/models/user.h"
#include "../src/models/_db.h"

#include "../src/lib/global_state.h"

#define private public
#define protected public

#include "../src/ui/author_info.h"

#include "./ui_test_utils.h"


#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::make_shared;
using std::shared_ptr;


TEST_CASE("Should make a AuthorInfo widget") {
	set_profile_directory(TEST_PROFILE_DIR);

	shared_ptr<User> u = make_shared<User>(User::get_user_by_handle("kwamurai"));

	AuthorInfo info_panel(nullptr, u);

	REQUIRE(info_panel.u.get() == u.get());
	REQUIRE(info_panel.handle_label->text() == "@kwamurai");
	REQUIRE(info_panel.display_name_label->text() == "Bronze Age Kashi");
	REQUIRE(info_panel.banned_label == nullptr);
}

TEST_CASE("Should get a resized, circlified profile img for an AuthorInfo") {
	set_profile_directory(TEST_PROFILE_DIR);
	shared_ptr<User> u = make_shared<User>(User::get_user_by_handle("kwamurai"));
	REQUIRE(u->is_content_downloaded == true);

	AuthorInfo info_panel(nullptr, u);

	// Creating the AuthorInfo widget will have created 1 profile pic
	REQUIRE(GlobalState::USER_AVATARS.size() == 1);

	shared_ptr<QPixmap> result = GlobalState::get_profile_img(u);

	// Should be cached now
	REQUIRE(GlobalState::USER_AVATARS.size() == 1);
	REQUIRE(GlobalState::USER_AVATARS[u->handle] == result);

	// Getting it again should use the cache
	shared_ptr<QPixmap> result2 = GlobalState::get_profile_img(u);
	REQUIRE(GlobalState::USER_AVATARS.size() == 1);
	REQUIRE(result == result2);

	// Remove it from the cache
	GlobalState::delete_cached_profile_img(u);
	REQUIRE(GlobalState::USER_AVATARS.size() == 0);
	shared_ptr<QPixmap> result3 = GlobalState::get_profile_img(u);
	REQUIRE(result != result3);
}

TEST_CASE("Should give an empty pixmap if user's profile image is not downloaded") {
	GlobalState::USER_AVATARS.clear();  // Empty the cache

	set_profile_directory(TEST_PROFILE_DIR);
	shared_ptr<User> u = make_shared<User>(User::get_user_by_handle("kwamurai"));
	u->is_content_downloaded = false;

	REQUIRE(GlobalState::USER_AVATARS.size() == 0);

	AuthorInfo info_panel(nullptr, u);

	// No pixmap should have been created
	REQUIRE(GlobalState::USER_AVATARS.size() == 0);

	shared_ptr<QPixmap> result = GlobalState::get_profile_img(u);
	REQUIRE(GlobalState::USER_AVATARS.size() == 0);
	REQUIRE(result->isNull());
}

TEST_CASE("Should have a bluecheck if they're verified") {
	set_profile_directory(TEST_PROFILE_DIR);
	shared_ptr<User> u = make_shared<User>(User::get_user_by_handle("cernovich"));
	REQUIRE(u->is_verified == true);

	AuthorInfo info_panel(nullptr, u);
	REQUIRE(info_panel.bluecheck_label != nullptr);
	REQUIRE(info_panel.bluecheck_label->pixmap()->isNull() == false);
	REQUIRE(info_panel.name_and_verified_layout->count() == 2);  // name and bluecheck
}

TEST_CASE("Should not have a bluecheck if they're not verified") {
	set_profile_directory(TEST_PROFILE_DIR);
	shared_ptr<User> u = make_shared<User>(User::get_user_by_handle("kwamurai"));
	REQUIRE(u->is_verified == false);

	AuthorInfo info_panel(nullptr, u);
	REQUIRE(info_panel.bluecheck_label == nullptr);
	REQUIRE(info_panel.name_and_verified_layout->count() == 1);  // name only
}

TEST_CASE("Should have a lock icon if they're privated") {
	set_profile_directory(TEST_PROFILE_DIR);
	shared_ptr<User> u = make_shared<User>(User::get_user_by_handle("kwamurai"));
	u->is_private = true;

	AuthorInfo info_panel(nullptr, u);
	REQUIRE(info_panel.privated_label != nullptr);
	REQUIRE(info_panel.name_and_verified_layout->count() == 2);  // name and lock
}

TEST_CASE("Should say BANNED if they're banned") {
	set_profile_directory(TEST_PROFILE_DIR);
	shared_ptr<User> u = make_shared<User>(User::get_user_by_handle("kwamurai"));
	u->is_banned = true;

	AuthorInfo info_panel(nullptr, u);
	REQUIRE(info_panel.banned_label != nullptr);
	REQUIRE(info_panel.name_and_verified_layout->count() == 2);  // name and BANNED label
}


TEST_CASE("Should emit signal when clicked") {
	set_profile_directory(TEST_PROFILE_DIR);
	shared_ptr<User> u = make_shared<User>(User::get_user_by_handle("kwamurai"));

	AuthorInfo info_panel(nullptr, u);
	QSignalSpy spy(&info_panel, SIGNAL(profile_image_clicked(shared_ptr<User>)));
	REQUIRE(spy.count() == 0);

	QMouseEvent e = get_left_click();
	info_panel.mousePressEvent(&e);   // Clicking should emit a signal
	REQUIRE(spy.count() == 1);
	info_panel.mousePressEvent(&e);  // Click again, should increment again
	REQUIRE(spy.count() == 2);

	e = get_right_click();
	info_panel.mousePressEvent(&e);  // Right-clicking should be ignored
	REQUIRE(spy.count() == 2);
}

TEST_CASE("Should resize the profile image") {
	set_profile_directory(TEST_PROFILE_DIR);
	shared_ptr<User> u = make_shared<User>(User::get_user_by_handle("kwamurai"));
	REQUIRE(u->is_verified == false);

	AuthorInfo info_panel(nullptr, u);
	int new_size = 28;
	info_panel.set_image_size(new_size);
	REQUIRE(info_panel.profile_image_label->width() == new_size);
	REQUIRE(info_panel.profile_image_label->height() == new_size);
}

TEST_CASE("Should resize the text size") {
	set_profile_directory(TEST_PROFILE_DIR);
	shared_ptr<User> u = make_shared<User>(User::get_user_by_handle("kwamurai"));
	REQUIRE(u->is_verified == false);

	AuthorInfo info_panel(nullptr, u);
	int new_size = 28;
	info_panel.set_text_size(new_size);
	REQUIRE(info_panel.display_name_label->font().pixelSize() == new_size);
}

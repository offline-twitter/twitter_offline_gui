#include <QString>
#include <QPixmap>

#include <map>
#include <memory>

#include "./engine_caller.h"

#include "../models/user.h"

using std::map;
using std::shared_ptr;


class GlobalState {
 public:
    static QString PROFILE_DIR;
    static Engine TWITTER_ENGINE;
    static QString ENGINE_VERSION;
    static map<UserHandle, shared_ptr<QPixmap>> USER_AVATARS;

    static shared_ptr<QPixmap> get_profile_img(shared_ptr<User> u);
    static void delete_cached_profile_img(shared_ptr<User> u);

    static int USER_FEED_TWEET_BATCH_SIZE;
};

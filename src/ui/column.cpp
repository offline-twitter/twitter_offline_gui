#include "column.h"

#include <QString>
#include <QLayoutItem>
#include <QCursor>
#include <QRegularExpression>
#include <QMessageBox>

#include <memory>

#include "../models/_db.h"
#include "../lib/global_state.h"

#include "./conversation.h"
#include "./user_feed.h"
#include "./search_feed.h"
#include "./timeline.h"
#include "./followed_list.h"
#include "./welcome_screen.h"

using std::make_shared;


Column::Column(QWidget* parent, bool is_welcome_mode): QWidget(parent) {
    this->is_welcome_mode = is_welcome_mode;

    layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    navbar = new Navbar(this);
    layout->addWidget(navbar);
    connect(navbar, SIGNAL(submit_search(QString)), this, SLOT(do_searchbar(QString)));
    connect(navbar, SIGNAL(home_button_clicked()), this, SLOT(open_timeline()));
    connect(navbar, SIGNAL(list_followed_button_clicked()), this, SLOT(open_followed_users()));

    stacked_layout = new QStackedLayout();
    layout->addLayout(stacked_layout);

    if (is_welcome_mode) {
        navbar->setEnabled(false);
        stacked_layout->addWidget(new WelcomeScreen(this));
    }
}

void Column::exit_welcome_mode_if_needed() {
    if (!is_welcome_mode)
        return;

    is_welcome_mode = false;

    // Delete the welcome screen
    QLayoutItem* item = stacked_layout->takeAt(stacked_layout->count() - 1);
    delete item->widget();
    delete item;

    navbar->setEnabled(true);
}

void Column::open_timeline() {
    Feed feed = Feed::get_timeline(GlobalState::USER_FEED_TWEET_BATCH_SIZE, -1);
    Timeline* timeline = new Timeline(feed, this);
    stacked_layout->addWidget(timeline);
    stacked_layout->setCurrentWidget(timeline);

    navbar->set_navigation_depth(stacked_layout->count());
    titles.push_back("Your personal feed");
    notify_title_changed();
}

void Column::open_search_feed(SearchParams params) {
    SearchFeed* search_feed = new SearchFeed(params, this);
    stacked_layout->addWidget(search_feed);
    stacked_layout->setCurrentWidget(search_feed);

    navbar->set_navigation_depth(stacked_layout->count());
    titles.push_back("Search results: " + params.get_original_query());
    notify_title_changed();
}

void Column::open_followed_users() {
    FollowedList* followed_list = new FollowedList(this);
    stacked_layout->addWidget(followed_list);
    stacked_layout->setCurrentWidget(followed_list);

    navbar->set_navigation_depth(stacked_layout->count());
    titles.push_back("Followed users");
    notify_title_changed();
}

void Column::open_user_feed(shared_ptr<User> u) {
    UserFeed* new_feed = new UserFeed(this, u);
    stacked_layout->addWidget(new_feed);
    stacked_layout->setCurrentWidget(new_feed);

    navbar->set_navigation_depth(stacked_layout->count());
    titles.push_back("@" + u->handle);
    notify_title_changed();
}

void Column::open_tweet(shared_ptr<Tweet> t) {
    printf("Column: opening tweet with id %ld\n", t->id);

    if (!t->is_conversation_scraped) {
        // TODO: add more conditions: not recently scraped?  tweet is deleted?
        t->download_conversation();
    }

    Conversation* new_conversation = new Conversation(this, t);
    stacked_layout->addWidget(new_conversation);
    stacked_layout->setCurrentWidget(new_conversation);

    navbar->set_navigation_depth(stacked_layout->count());
    titles.push_back(QString("@") + t->get_user()->handle + ": \"" + t->text + "\"");
    notify_title_changed();
}

void Column::go_back() {
    if (stacked_layout->count() == 0) {
        // Do nothing if there's nothing to go back to
        return;
    }

    QLayoutItem* item = stacked_layout->takeAt(stacked_layout->count() - 1);
    delete item->widget();
    delete item;

    stacked_layout->setCurrentIndex(stacked_layout->count() - 1);
    titles.pop_back();
    notify_title_changed();

    navbar->set_navigation_depth(stacked_layout->count());
}

void Column::notify_title_changed() {
    if (titles.size() == 0) {
        emit title_changed("No views active");
    } else {
        emit title_changed(titles.back());
    }
}

int Column::count() {
    if (is_welcome_mode)
        return 0;
    return stacked_layout->count();
}

/**
 * Processes a tweet search, making an engine call if it's not found.
 */
void Column::search_tweet(TweetID id) {
    // TODO: handle errors here better
    shared_ptr<Tweet> t;
    try {
        t = make_shared<Tweet>(Tweet::get_tweet_by_id(id));
    } catch (DBException &e) {
        // Tweet not found; so make an engine call to fetch it
        QCursor tmp = cursor();
        setCursor(Qt::WaitCursor);
        int result = Tweet::download_conversation(id);
        if (result != 0) {
            // Engine call did not complete successfully.
            #ifndef TESTING_MODE
                QMessageBox::warning(this,
                    "Error fetching conversation",
                    "Got error status " + QString::number(result) + " when fetching the conversation. This tweet doesn't seem to be available."
                );
            #endif  // ifndef TESTING_MODE
            return;
        }
        setCursor(tmp);

        t = make_shared<Tweet>(Tweet::get_tweet_by_id(id));
    }
    open_tweet(t);
}

void Column::search_user(UserHandle handle) {
    // Check for invalid chars
    if (!QRegularExpression("^\\w+$").match(handle).hasMatch()) {
        #ifndef TESTING_MODE
        QMessageBox::warning(this,
            "Invalid user handle",
            "User handles should not have spaces or symbols in them"
        );
        #endif  // ifndef TESTING_MODE
        return;
    }

    shared_ptr<User> u;
    try {
        u = make_shared<User>(User::get_user_by_handle(handle));
    } catch (DBException &e) {
        // User not found; so make an engine call to fetch it
        QCursor tmp = cursor();
        setCursor(Qt::WaitCursor);
        int result = User::fetch_user(handle);
        if (result != 0) {
            // Engine call did not complete successfully.
            #ifndef TESTING_MODE
            QMessageBox::warning(this,
                "Error fetching user",
                "Got error status " + QString::number(result) + " when fetching the user with this handle. The program might crash when you press OK"
            );
            #endif  // ifndef TESTING_MODE
        }
        setCursor(tmp);

        u = make_shared<User>(User::get_user_by_handle(handle));
    }
    open_user_feed(u);
}

/**
 * Resolves searches in the following order:
 * - checks for a tweet URL
 * - checks for a tweet ID
 * - checks for a user handle
 */
void Column::do_searchbar(QString s) {
    if (s == "") {
        #ifndef TESTING_MODE
            QMessageBox::warning(this,
                "Empty search",
                "Search string was empty"
            );
        #endif
        return;
    }

    bool is_ok;
    TweetID tweet_id = Tweet::parse_url(s, &is_ok);
    if (is_ok) {
        return search_tweet(tweet_id);
    }

    tweet_id = Tweet::parse_id(s, &is_ok);
    if (is_ok) {
        return search_tweet(tweet_id);
    }

    if (s[0] == '@') {
        UserHandle handle = s.remove(0, 1);
        return search_user(handle);
    }

    // Real search!
    SearchParams params = SearchParams::parse_search_query(s, &is_ok);
    if (!is_ok) {
        #ifndef TESTING_MODE
            QMessageBox::warning(this,
                "Invalid search",
                "Check that your quotation marks match"
            );
        #endif
        return;
    }
    open_search_feed(params);
}

/**
 * Callback for when a hashtag or @mention is clicked
 */
void Column::entity_clicked(QString target) {
    printf("Entity clicked: %s\n", target.toStdString().c_str());
    if (target.startsWith("@")) {
        search_user(target.mid(1));  // chop off the '@' character
    }
    // TODO: implement for hashtags
}

void Column::mousePressEvent(QMouseEvent* e) {
    if (e->button() == Qt::BackButton) {
        go_back();
    } else {
        e->ignore();
    }
}


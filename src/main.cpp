#include <stdlib.h>

#include <QApplication>
#include <QMessageBox>
#include <QString>
#include <QStandardPaths>
#include <QDir>

#include "models/_db.h"
#include "lib/global_state.h"

#include "ui/main_window.h"

const QString PROFILE_DIR = "../../sample_data/profile";

/**
 * Checks that a `twitter` binary is available, by calling it with no args (should return 0 exit code).
 * If it's not available, warn the user.
 */
void ensure_twitter_binary() {
    int result = GlobalState::TWITTER_ENGINE.init();
    if (result == 0) {
        return;
    }

    QMessageBox::StandardButton is_continue = QMessageBox::warning(
        nullptr,
        "No `twitter` binary found",
        "Could not find the `twitter` scraper engine.  Ensure that you have it installed and that it is in your executable PATH.\n\nFetching new tweets will not work.  You can still browse tweets you have already downloaded, but attempting to download new ones will fail and probably crash the app.\n\nDo you want to open the application anyway, to browse tweets you've already downloaded?",
        QMessageBox::Yes | QMessageBox::No
    );
    if (is_continue == QMessageBox::No) {
        // Abort execution
        exit(1);
    } else {
        // Warn user, and continue
        QMessageBox::information(
            nullptr,
            "Opening app with scraping disabled",
            "You're opening this app without a twitter scraper.  You can still view tweets, although fetching new ones will not work.",
            QMessageBox::Ok
        );
    }
}


int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    ensure_twitter_binary();

    printf("Offline Twitter version: %s\n", OFFLINE_TWITTER_VERSION);

    MainWindow window;

    QIcon app_icon("qrc:///app_icon");
    app.setWindowIcon(app_icon);

    QString appdata_path = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QString default_profile = appdata_path + QDir::separator() + "default_profile";

    // Ensure the Twitter appdata directory exists; if not, create it
    QDir dir(appdata_path);
    if (!dir.exists()) {
        dir.mkpath(".");
    }

    printf("Opening default profile location: %s\n", default_profile.toStdString().c_str());
    try {
        window.set_profile_directory_to(default_profile);
    } catch (DBException &e) {
        create_profile_directory(default_profile);
        window.set_profile_directory_to(default_profile);
    }

    window.show();
    return app.exec();
}

#include <QGuiApplication>
#include <QClipboard>
#include <QList>
#include <QSignalSpy>

#include <memory>

#include "include_catch2/catch.h"

#include "../src/models/tweet.h"
#include "../src/models/_db.h"

#define private public
#define protected public

#include "../src/ui/tweet_widget.h"
#include "../src/ui/components/clickable_icon.h"
#include "../src/ui/colors.h"
#include "../src/ui/utils/clickable_entities.h"

#include "./ui_test_utils.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::make_shared;


TEST_CASE("Should make an TweetWidget") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1428951883058753537);

	TweetWidget widget(nullptr, make_shared<Tweet>(t));

	REQUIRE(widget.author_info_panel->u->handle == t.get_user()->handle);
	REQUIRE(widget.author_info_panel->u->handle == t.get_user()->handle);
	REQUIRE(widget.in_reply_to_label->text() == get_rich_text_for(t.get_replying_to_text()));
	REQUIRE(widget.body_layout->count() == 2);
	REQUIRE(widget.text_label->text() == "Space X was an embarrassment in a lot of ways - it showed up NASA very badly.");
	REQUIRE(widget.posted_at_label->text() == t.render_posted_at());
	REQUIRE(widget.video_preview == nullptr);
	REQUIRE(widget.poll_widget == nullptr);
	REQUIRE(widget.tombstone_label == nullptr);
}

TEST_CASE("Should make a TweetWidget with images") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1261483383483293700);
	REQUIRE(t.images.size() == 4);

	TweetWidget widget(nullptr, make_shared<Tweet>(t));

	REQUIRE(widget.body_layout->count() == 6);
}

TEST_CASE("Should make a TweetWidget with a video") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1453461248142495744);
	REQUIRE(t.videos.size() == 1);

	TweetWidget widget(nullptr, make_shared<Tweet>(t));
	REQUIRE(widget.video_preview != nullptr);
}

TEST_CASE("Should make a TweetWidget with a quoted tweet") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1413664406995566593);

	TweetWidget widget(nullptr, make_shared<Tweet>(t));

	REQUIRE(widget.body_layout->count() == 3);
	QuotedTweet* quoted_tweet = widget.quoted_tweet_widget;
	REQUIRE(quoted_tweet->t->id == t.get_quoted_tweet()->id);
}

TEST_CASE("Should make a TweetWidget with an embedded link") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1438642143170646017);

	TweetWidget widget(nullptr, make_shared<Tweet>(t));

	REQUIRE(widget.body_layout->count() == 5);  // Text, 3 urls, interactions bar
	REQUIRE(widget.embedded_link != nullptr);
	REQUIRE(widget.embedded_link->title_label->text() == "Biden’s victory came from the suburbs");
}

TEST_CASE("Should have a poll") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1465534109573390348);

	TweetWidget widget(nullptr, make_shared<Tweet>(t));

	REQUIRE(widget.body_layout->count() == 3);
	REQUIRE(widget.poll_widget != nullptr);
}

TEST_CASE("Should have a tombstone") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(31);  // Fake

	TweetWidget widget(nullptr, make_shared<Tweet>(t));

	REQUIRE(widget.tombstone_label != nullptr);
	REQUIRE(widget.tombstone_label->text() == "This Tweet was deleted by the Tweet author");
}


TEST_CASE("Should copy the link to a tweet to the clipboard") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1261483383483293700);
	TweetWidget widget(nullptr, make_shared<Tweet>(t));

	// Clear the clipboard
    QGuiApplication::clipboard()->setText("");
	REQUIRE(QGuiApplication::clipboard()->text() == "");

	widget.copy_link();
	REQUIRE(QGuiApplication::clipboard()->text() == "https://twitter.com/Denlesks/status/1261483383483293700");
}

TEST_CASE("If tweet has undownloaded content, give red warning") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1261483383483293700);
	t.is_content_downloaded = false;
	TweetWidget widget(nullptr, make_shared<Tweet>(t));

	REQUIRE(widget.content_not_downloaded_label != nullptr);
	REQUIRE(widget.content_not_downloaded_label->palette().color(QPalette::WindowText) == COLOR_RED);
}


TEST_CASE("Nested tweet limits") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1439068749336748043);

	TweetWidget widget(nullptr, make_shared<Tweet>(t), nullptr, nullptr, 2);  // Nesting limit is 2
	REQUIRE(widget.nesting_limit == 2);
	REQUIRE(widget.quoted_tweet_widget != nullptr);

	// Nesting level 1
	shared_ptr<Tweet> t2 = t.get_quoted_tweet();
	TweetWidget* widget2 = widget.quoted_tweet_widget;
	REQUIRE(widget2->t->id == t2->id);
	REQUIRE(widget2->nesting_limit == 1);
	REQUIRE(widget2->quoted_tweet_widget != nullptr);

	// Nesting level 2
	shared_ptr<Tweet> t3 = t2->get_quoted_tweet();
	TweetWidget* widget3 = widget2->quoted_tweet_widget;
	REQUIRE(widget3->t->id == t3->id);
	REQUIRE(widget3->nesting_limit == 0);
	REQUIRE(widget3->quoted_tweet_widget == nullptr);  // Shouldn't have any more nested widgets
}

TEST_CASE("Nested tweet with limit 1") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1439068749336748043);

	TweetWidget widget(nullptr, make_shared<Tweet>(t), nullptr, nullptr, 1);  // Nesting limit is 1
	REQUIRE(widget.nesting_limit == 1);
	REQUIRE(widget.quoted_tweet_widget != nullptr);

	// Nesting level 1
	shared_ptr<Tweet> t2 = t.get_quoted_tweet();
	TweetWidget* widget2 = widget.quoted_tweet_widget;
	REQUIRE(widget2->t->id == t2->id);
	REQUIRE(widget2->nesting_limit == 0);
	REQUIRE(widget2->quoted_tweet_widget == nullptr);  // Shouldn't have any more nested widgets
}


TEST_CASE("Should indicate retweet status") {
	set_profile_directory(TEST_PROFILE_DIR);
	shared_ptr<Retweet> r = make_shared<Retweet>(Retweet::get_retweet_by_id(1449195266603630594));

	TweetWidget widget(nullptr, r->get_tweet(), nullptr, r);

	// Should be no retweet
	REQUIRE(widget.rt != nullptr);
	REQUIRE(widget.rt->id == r->id);
	REQUIRE(widget.retweeted_icon != nullptr);
	REQUIRE(widget.retweeted_icon->pixmap()->isNull() == false);
	REQUIRE(widget.retweet_label != nullptr);
	REQUIRE(widget.retweet_label->text() == "Retweeted by Michael Malice");
}

TEST_CASE("Should have no retweet indicator if it isn't a retweet") {
	set_profile_directory(TEST_PROFILE_DIR);
	Tweet t = Tweet::get_tweet_by_id(1428951883058753537);

	TweetWidget widget(nullptr, make_shared<Tweet>(t));

	// Should be no retweet
	REQUIRE(widget.rt == nullptr);
	REQUIRE(widget.retweeted_icon == nullptr);
	REQUIRE(widget.retweet_label == nullptr);
}


// Test clicking on entities
// -------------------------

TEST_CASE("Clicking an @-mention or #hashtag in the tweet body") {
	set_profile_directory(TEST_PROFILE_DIR);

	Tweet t_base = Tweet::get_tweet_by_id(1413773185296650241);
	Tweet t1(t_base);
	t1.text = "@michaelmalice";
	t1.mentions.push_back("michaelmalice");
	Tweet t2(t_base);
	t2.text = "#SomeHashtag";
	t2.hashtags.push_back("SomeHashtag");

	QList<Tweet> test_cases = QList<Tweet>() << t1 << t2;
	for (Tweet t: test_cases) {
		TweetWidget widget(nullptr, make_shared<Tweet>(t));

		QSignalSpy spy(widget.text_label, SIGNAL(linkActivated(QString)));
		REQUIRE(spy.count() == 0);

		QMouseEvent click_event = get_left_click();
		widget.text_label->mousePressEvent(&click_event);
		REQUIRE(spy.count() == 1);
		REQUIRE(spy.takeFirst().at(0).toString() == t.text);
	}
}

TEST_CASE("Clicking an @-mention in the reply-mentions") {
	set_profile_directory(TEST_PROFILE_DIR);

	Tweet t = Tweet::get_tweet_by_id(1413773185296650241);
	TweetWidget widget(nullptr, make_shared<Tweet>(t));

	QSignalSpy spy(widget.in_reply_to_label, SIGNAL(linkActivated(QString)));
	REQUIRE(spy.count() == 0);

	QMouseEvent click_event = get_left_click();
	widget.in_reply_to_label->setText(widget.in_reply_to_label->text().mid(12));  // Remove the "Replying to: "
	widget.in_reply_to_label->mousePressEvent(&click_event);
	REQUIRE(spy.count() == 1);
	REQUIRE(spy.takeFirst().at(0).toString() == "@Germany12343");
}

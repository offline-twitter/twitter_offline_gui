#include "include_catch2/catch.h"

#include "../src/models/_db.h"
#include "../src/models/feeds/cursor.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"


TEST_CASE("Get feed by newest") {
    set_profile_directory(TEST_PROFILE_DIR);

    SearchParams params;
    params.keywords << "think";
    SSF::Cursor c;
    c.search_params = params;
    c.page_size = 3;
    REQUIRE(c.cursor_type == SSF::CursorType::START);
    REQUIRE(c.sort_order == SSF::SORT_ORDER_NEWEST);

    // Get first page
    Feed first_page = c.get_next_page();

    REQUIRE(first_page.items.size() == c.page_size);
    REQUIRE(first_page.items[0].tweet->id == 1439067163508150272);
    REQUIRE(first_page.items[1].tweet->id == 1439027915404939265);
    REQUIRE(first_page.items[2].tweet->id == 1428939163961790466);

    REQUIRE(first_page.cursor->cursor_type == SSF::CursorType::MIDDLE);
    REQUIRE(first_page.cursor->sort_order == c.sort_order);
    REQUIRE(first_page.cursor->search_params.keywords.size() == c.search_params.keywords.size());
    REQUIRE(first_page.cursor->search_params.keywords[0] == c.search_params.keywords[0]);
    REQUIRE(first_page.cursor->page_size == c.page_size);
    REQUIRE(first_page.cursor->cursor_position == 1629520619);

    // Get second page
    Feed second_page = first_page.cursor->get_next_page();

    REQUIRE(second_page.items.size() == 2);
    REQUIRE(second_page.items[0].tweet->id == 1413772782358433792);
    REQUIRE(second_page.items[1].tweet->id == 1343633011364016128);

    REQUIRE(second_page.cursor->cursor_type == SSF::CursorType::END);
    REQUIRE(second_page.cursor->sort_order == c.sort_order);
    REQUIRE(second_page.cursor->search_params.keywords.size() == c.search_params.keywords.size());
    REQUIRE(second_page.cursor->search_params.keywords[0] == c.search_params.keywords[0]);
    REQUIRE(second_page.cursor->page_size == c.page_size);
}

TEST_CASE("Get feed by oldest") {
    set_profile_directory(TEST_PROFILE_DIR);

    SearchParams params;
    params.keywords << "think";
    SSF::Cursor c;
    c.search_params = params;
    c.page_size = 3;
    c.sort_order = SSF::SORT_ORDER_OLDEST;
    REQUIRE(c.cursor_type == SSF::CursorType::START);

    // Get first page
    Feed first_page = c.get_next_page();

    REQUIRE(first_page.items.size() == c.page_size);
    REQUIRE(first_page.items[0].tweet->id == 1343633011364016128);
    REQUIRE(first_page.items[1].tweet->id == 1413772782358433792);
    REQUIRE(first_page.items[2].tweet->id == 1428939163961790466);

    REQUIRE(first_page.cursor->cursor_type == SSF::CursorType::MIDDLE);
    REQUIRE(first_page.cursor->sort_order == c.sort_order);
    REQUIRE(first_page.cursor->search_params.keywords.size() == c.search_params.keywords.size());
    REQUIRE(first_page.cursor->search_params.keywords[0] == c.search_params.keywords[0]);
    REQUIRE(first_page.cursor->page_size == c.page_size);
    REQUIRE(first_page.cursor->cursor_position == 1629520619);

    // Get second page
    Feed second_page = first_page.cursor->get_next_page();

    REQUIRE(second_page.items.size() == 2);
    REQUIRE(second_page.items[0].tweet->id == 1439027915404939265);
    REQUIRE(second_page.items[1].tweet->id == 1439067163508150272);

    REQUIRE(second_page.cursor->cursor_type == SSF::CursorType::END);
    REQUIRE(second_page.cursor->sort_order == c.sort_order);
    REQUIRE(second_page.cursor->search_params.keywords.size() == c.search_params.keywords.size());
    REQUIRE(second_page.cursor->search_params.keywords[0] == c.search_params.keywords[0]);
    REQUIRE(second_page.cursor->page_size == c.page_size);
}

TEST_CASE("Get feed by most likes") {
    set_profile_directory(TEST_PROFILE_DIR);

    SearchParams params;
    params.keywords << "think";
    SSF::Cursor c;
    c.search_params = params;
    c.page_size = 3;
    c.sort_order = SSF::SORT_ORDER_MOST_LIKES;
    REQUIRE(c.cursor_type == SSF::CursorType::START);

    // Get first page
    Feed first_page = c.get_next_page();

    REQUIRE(first_page.items.size() == c.page_size);
    REQUIRE(first_page.items[0].tweet->id == 1439027915404939265);
    REQUIRE(first_page.items[1].tweet->id == 1439067163508150272);
    REQUIRE(first_page.items[2].tweet->id == 1343633011364016128);

    REQUIRE(first_page.cursor->cursor_type == SSF::CursorType::MIDDLE);
    REQUIRE(first_page.cursor->sort_order == c.sort_order);
    REQUIRE(first_page.cursor->search_params.keywords.size() == c.search_params.keywords.size());
    REQUIRE(first_page.cursor->search_params.keywords[0] == c.search_params.keywords[0]);
    REQUIRE(first_page.cursor->page_size == c.page_size);
    REQUIRE(first_page.cursor->cursor_position == 138);

    // Get second page
    Feed second_page = first_page.cursor->get_next_page();

    REQUIRE(second_page.items.size() == 2);
    REQUIRE(second_page.items[0].tweet->id == 1428939163961790466);
    REQUIRE(second_page.items[1].tweet->id == 1413772782358433792);

    REQUIRE(second_page.cursor->cursor_type == SSF::CursorType::END);
    REQUIRE(second_page.cursor->sort_order == c.sort_order);
    REQUIRE(second_page.cursor->search_params.keywords.size() == c.search_params.keywords.size());
    REQUIRE(second_page.cursor->search_params.keywords[0] == c.search_params.keywords[0]);
    REQUIRE(second_page.cursor->page_size == c.page_size);
}

TEST_CASE("Get feed by most retweets") {
    set_profile_directory(TEST_PROFILE_DIR);

    SearchParams params;
    params.keywords << "think";
    SSF::Cursor c;
    c.search_params = params;
    c.page_size = 3;
    c.sort_order = SSF::SORT_ORDER_MOST_RETWEETS;
    REQUIRE(c.cursor_type == SSF::CursorType::START);

    // Get first page
    Feed first_page = c.get_next_page();

    REQUIRE(first_page.items.size() == c.page_size);
    REQUIRE(first_page.items[0].tweet->id == 1439027915404939265);
    REQUIRE(first_page.items[1].tweet->id == 1439067163508150272);
    REQUIRE(first_page.items[2].tweet->id == 1343633011364016128);

    REQUIRE(first_page.cursor->cursor_type == SSF::CursorType::MIDDLE);
    REQUIRE(first_page.cursor->sort_order == c.sort_order);
    REQUIRE(first_page.cursor->search_params.keywords.size() == c.search_params.keywords.size());
    REQUIRE(first_page.cursor->search_params.keywords[0] == c.search_params.keywords[0]);
    REQUIRE(first_page.cursor->page_size == c.page_size);
    REQUIRE(first_page.cursor->cursor_position == 9);

    // Cheat a little bit and modify the cursor manually before fetching next page
    first_page.cursor->cursor_position = 133; // Exclude just the first one
    Feed second_page = first_page.cursor->get_next_page();

    // REQUIRE(second_page.items.size() == 3);
    REQUIRE(second_page.items[0].tweet->id == 1439067163508150272);
    REQUIRE(second_page.items[1].tweet->id == 1343633011364016128);
    // The third one is non-deterministic; they both have 0 retweets, so don't test for it

    REQUIRE(second_page.cursor->cursor_type == SSF::CursorType::MIDDLE);
    REQUIRE(second_page.cursor->sort_order == c.sort_order);
    REQUIRE(second_page.cursor->search_params.keywords.size() == c.search_params.keywords.size());
    REQUIRE(second_page.cursor->search_params.keywords[0] == c.search_params.keywords[0]);
    REQUIRE(second_page.cursor->page_size == c.page_size);
}

TEST_CASE("Get tweets retweeted by someone") {
    set_profile_directory(TEST_PROFILE_DIR);

    SearchParams params;
    params.retweeted_by_user = "cernovich";
    SSF::Cursor c;
    c.search_params = params;
    c.page_size = 3;
    c.sort_order = SSF::SORT_ORDER_NEWEST;
    REQUIRE(c.cursor_type == SSF::CursorType::START);

    // Get first page
    Feed first_page = c.get_next_page();

    REQUIRE(first_page.items.size() == c.page_size);
    REQUIRE(first_page.items[0].retweet->id == 1490135787144237058);
    REQUIRE(first_page.items[1].retweet->id == 1490119308692766723);
    REQUIRE(first_page.items[2].retweet->id == 1490100255987171332);

    REQUIRE(first_page.cursor->cursor_type == SSF::CursorType::MIDDLE);
    REQUIRE(first_page.cursor->sort_order == c.sort_order);
    REQUIRE(first_page.cursor->search_params.keywords.size() == c.search_params.keywords.size());
    REQUIRE(first_page.cursor->search_params.retweeted_by_user == c.search_params.retweeted_by_user);
    REQUIRE(first_page.cursor->page_size == c.page_size);
    REQUIRE(first_page.cursor->cursor_position == 1644102560);

    Feed second_page = first_page.cursor->get_next_page();
    REQUIRE(second_page.items.size() == 0);
    REQUIRE(second_page.cursor->cursor_type == SSF::CursorType::END);
}

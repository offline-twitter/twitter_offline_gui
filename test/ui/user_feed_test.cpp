#include "include_catch2/catch.h"

#include "../src/models/tweet.h"
#include "../src/models/user.h"
#include "../src/models/_db.h"

#define private public

#include "../src/lib/global_state.h"

#include "../src/ui/user_feed.h"
#include "../src/ui/tweet_widget.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::make_shared;



TEST_CASE("Should make a UserFeed") {
	set_profile_directory(TEST_PROFILE_DIR);
	User u = User::get_user_by_handle("Peter_Nimitz");

	UserFeed feed(nullptr, make_shared<User>(u));

	REQUIRE(feed.layout->count() == 3);  // 0) Header; 1) tweets; 2) stretch element

	REQUIRE(u.get_tweets().size() == 6);
	REQUIRE(feed.tweet_feed->tweets_layout->count() == 6);

	// Stretch item
	REQUIRE(feed.layout->itemAt(2)->widget() == nullptr);
	REQUIRE(feed.layout->itemAt(2)->spacerItem() != nullptr);
}

/**
 * Test pagination of a UserFeed.  Should load more tweets on each `load_more_tweets` call,
 * until the end of feed is reached, then disable the `load_more_tweets` button.
 */
TEST_CASE("Should paginate a User's feed") {
	set_profile_directory(TEST_PROFILE_DIR);
	GlobalState::USER_FEED_TWEET_BATCH_SIZE = 2;

	User u = User::get_user_by_handle("Peter_Nimitz");
	REQUIRE(u.get_tweets().size() == 6);
	UserFeed feed(nullptr, make_shared<User>(u));

	REQUIRE(feed.tweet_feed->tweets_layout->count() == 2);

	feed.load_more_tweets();
	REQUIRE(feed.tweet_feed->tweets_layout->count() == 4);
	feed.load_more_tweets();
	REQUIRE(feed.tweet_feed->tweets_layout->count() == 6);

	// Try to load past end of feed should disable the button
	REQUIRE(feed.tweet_feed->load_more_tweets_button->isEnabled() == true);
	feed.load_more_tweets();
	REQUIRE(feed.tweet_feed->tweets_layout->count() == 6);
	REQUIRE(feed.tweet_feed->load_more_tweets_button->isEnabled() == false);

	// Reset Global State
	GlobalState::USER_FEED_TWEET_BATCH_SIZE = 50;
}

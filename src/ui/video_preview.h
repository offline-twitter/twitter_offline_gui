#ifndef VIDEO_PREVIEW_H
#define VIDEO_PREVIEW_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QMouseEvent>
#include <QContextMenuEvent>
#include <QResizeEvent>

#include "../models/tweet.h"

class VideoPreview: public QWidget {
    Q_OBJECT

 private:
    QVBoxLayout* layout;
    QLabel* preview_image_label;
    QLabel* play_button_label;
    QLabel* metadata_label;

 public slots:
    void open_in_external();
    void copy_video_path();

 protected:
    void mousePressEvent(QMouseEvent* e);
    void contextMenuEvent(QContextMenuEvent* e);
    void resizeEvent(QResizeEvent* e);

 public:
    shared_ptr<Video> video;
    explicit VideoPreview(QWidget* parent = nullptr, shared_ptr<Video> video = nullptr);
};

#endif  // VIDEO_PREVIEW_H

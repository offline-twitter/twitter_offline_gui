#include "poll_widget.h"

#include <QPainter>
#include <QPen>
#include <QFont>
#include <QPalette>

#include "./utils/drawing.h"
#include "./colors.h"


PollOption::PollOption(QWidget* parent, QString label, QString votes, float percentage, bool is_winner): QWidget(parent) {
	this->label = label;
	this->votes = votes;
	this->percentage = percentage;
	this->is_winner = is_winner;

	layout = new QHBoxLayout(this);
	layout->setContentsMargins(5,5,5,5);

	choice_label = new QLabel(this);
	choice_label->setText(label);
	layout->addWidget(choice_label);

	votes_label = new QLabel(this);
	votes_label->setText(votes);
	layout->addWidget(votes_label);
}

void PollOption::paintEvent(QPaintEvent*) {
	fill_percentage(this, is_winner ? COLOR_TWITTER_BLUE_LIGHT : COLOR_TWITTER_OFF_WHITE_DARK, percentage);
}



PollWidget::PollWidget(QWidget* parent, shared_ptr<Poll> p): QWidget(parent) {
	this->p = p;

	layout = new QVBoxLayout(this);

	for (int i = 0; i < p->num_choices; i++) {
		options.push_back(new PollOption(
			this,
			p->choices[i],
			p->format_votes_count(p->votes[i]),
			static_cast<float>(p->votes[i]) / p->total_votes(),
			p->is_closed() && p->get_winner() == i
		));
		// TODO: hide / reveal votes
	}
	for (PollOption* p: options) {
		layout->addWidget(p);
	}

	metadata_label = new QLabel(this);

	QString metadata_format = (p->is_closed()) ?
		"Poll ended %1 - %2 votes" :
		"Poll open, voting ends at %1 - %2 votes";
	metadata_label->setText(metadata_format
		.arg(p->render_closes_at())
		.arg(p->total_votes())
	);

	QFont the_font = font();
	the_font.setPixelSize(the_font.pixelSize() - 2);
	metadata_label->setFont(the_font);

	QPalette pal = palette();
	pal.setColor(QPalette::WindowText, QColor(100, 100, 100)); // TODO color
	metadata_label->setPalette(pal);

	layout->addWidget(metadata_label);
}

void PollWidget::paintEvent(QPaintEvent*) {
	draw_outline(this, 10);
}

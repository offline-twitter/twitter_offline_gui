#include "user.h"

#include <QVariant>
#include <QRegularExpression>

#include "./_db.h"
#include "../lib/global_state.h"


User::User() {}

const QString User::SQL_USER_FIELDS = "id, display_name, handle, bio, following_count, followers_count, location, website, join_date, is_private, is_verified, is_banned, profile_image_url, profile_image_local_path, banner_image_url, banner_image_local_path, is_followed, is_content_downloaded";  // NOLINT(whitespace/line_length)

const QString DEFAULT_PROFILE_IMAGE = "default_profile.png";

void User::extract_user_from_query_row(QSqlQuery query, User* u) {
    u->id              = query.value(0).toULongLong();
    u->display_name    = query.value(1).toString();
    u->handle          = query.value(2).toString();
    u->bio             = query.value(3).toString();
    u->following_count = query.value(4).toInt();
    u->followers_count = query.value(5).toInt();
    u->location        = query.value(6).toString();
    u->website         = query.value(7).toString();
    u->join_date       = query.value(8).toInt();
    u->is_private      = query.value(9).toBool();
    u->is_verified     = query.value(10).toBool();
    u->is_banned       = query.value(11).toBool();
    u->profile_image_url = query.value(12).toString();
    u->profile_image_local_path = query.value(13).toString();
    if (u->profile_image_local_path == "") {
        u->profile_image_local_path = DEFAULT_PROFILE_IMAGE;
    }
    u->banner_image_url = query.value(14).toString();
    u->banner_image_local_path = query.value(15).toString();
    u->is_followed = query.value(16).toBool();
    u->is_content_downloaded = query.value(17).toBool();
}


User User::get_user_by_handle(UserHandle handle) {
    QSqlQuery query(DB);
    query.prepare("select " + User::SQL_USER_FIELDS + " from users where lower(handle) = lower(:handle)");
    query.bindValue(":handle", handle);
    query.exec();

    if (!query.next()) {
        throw DBException("Could not find user with handle: " + handle);
    }
    User u;
    User::extract_user_from_query_row(query, &u);
    return u;
}

User User::get_user_by_id(UserID user_id) {
    QSqlQuery query(DB);
    query.prepare("select " + User::SQL_USER_FIELDS + " from users where id = :user_id");
    query.bindValue(":user_id", (long long) user_id);
    query.exec();

    if (!query.next()) {
        throw DBException("Could not find user with id: " + user_id);
    }

    User u;
    User::extract_user_from_query_row(query, &u);
    return u;
}

/**
 * Get a list of all followed users, ordered alphabetically by user handle
 */
vector<User> User::get_followed_users() {
    QSqlQuery query(DB);
    query.prepare("select " + User::SQL_USER_FIELDS + " from users where is_followed = 1 order by lower(handle)");
    // TODO: consider perhaps `[...] join tweets on users.id = tweets.user_id  [...] group by handle order by max(tweets.posted_at)`
    query.exec();

    vector<User> ret;

    while (query.next()) {
        User u;
        User::extract_user_from_query_row(query, &u);
        ret.push_back(u);
    }
    return ret;
}

/**
 * Get all of a User's tweets
 */
vector<Tweet> User::get_tweets(bool include_replies) {  // TODO: make this use shared_ptrs
    // Pass it to the other `get_tweets` method
    return get_tweets(-1, -1, include_replies);
}

/**
 * Get a bunch of a User's tweets, posted between the given min/max limits, ignoring if limits are negative
 */
vector<Tweet> User::get_tweets(int min_posted_at_time, int max_posted_at_time, bool include_replies) {
    vector<Tweet> ret;

    QSqlQuery query(DB);
    query.prepare("select " + Tweet::SQL_TWEET_FIELDS + " from tweets where user_id = :user_id"
        + (min_posted_at_time > 0 ? " and posted_at >= :min_posted_at" : "")
        + (max_posted_at_time > 0 ? " and posted_at < :max_posted_at" : "")
        + (include_replies ? "" : " and reply_mentions = ''")
        + " order by posted_at desc");
    query.bindValue(":min_posted_at", min_posted_at_time);
    query.bindValue(":max_posted_at", max_posted_at_time);

    query.bindValue(":user_id", (long long) this->id);
    query.exec();

    while (query.next()) {
        Tweet t;
        Tweet::extract_tweet_from_query_row(query, &t);
        Tweet::add_extras_to_tweet(&t);
        ret.push_back(t);
    }
    return ret;
}

/**
 * Get all of a User's retweets
 */
vector<Retweet> User::get_retweets() {  // TODO: make this use shared_ptrs
    return get_retweets(-1, -1);
}

/**
 * Get a bunch of a User's retweets, retweeted between the given limits
 */
vector<Retweet> User::get_retweets(int min_retweeted_at_time, int max_retweeted_at_time) {
    vector<Retweet> ret;

    printf("%d, %d\n", min_retweeted_at_time, max_retweeted_at_time);

    QSqlQuery query(DB);
    query.prepare("select " + Retweet::SQL_RETWEET_FIELDS + " from retweets where retweeted_by = :user_id"
        + (min_retweeted_at_time > 0 ? " and retweeted_at >= :min_retweeted_at" : "")
        + (max_retweeted_at_time > 0 ? " and retweeted_at < :max_retweeted_at" : "")
        + " order by retweeted_at desc");
    query.bindValue(":min_retweeted_at", min_retweeted_at_time);
    query.bindValue(":max_retweeted_at", max_retweeted_at_time);

    query.bindValue(":user_id", (long long) this->id);
    query.exec();

    while (query.next()) {
        ret.push_back(Retweet::extract_retweet_from_query_row(query));
    }
    return ret;
}


void User::reload_from_db() {
    QSqlQuery query(DB);
    query.prepare("select " + User::SQL_USER_FIELDS + " from users where id = :user_id");
    query.bindValue(":user_id", (long long) this->id);
    query.exec();

    if (!query.next()) {
        throw DBException("Could not reload user with id: " + id);
    }

    User::extract_user_from_query_row(query, this);
}


QString User::get_profile_image_url() const {
    return profile_image_url;
}

/**
 * Return the user's expected profile image path, using the tiny version if user's content is not downloaded
 */
QString User::get_profile_image_local_path() const {
    if (is_content_downloaded || profile_image_local_path == DEFAULT_PROFILE_IMAGE) {
        return "/profile_images/" + profile_image_local_path;
    } else {
        return "/profile_images/" + QString(profile_image_local_path).replace(QRegularExpression("(\\.\\w{2,4})"), "_normal\\1");
    }
}
QString User::get_banner_image_url() const {
    return banner_image_url;
}
QString User::get_banner_image_local_path() const {
    return "/profile_images/" + banner_image_local_path;
}
bool User::has_default_profile_image() const {
    return profile_image_local_path == DEFAULT_PROFILE_IMAGE;
}
QString User::get_permalink() const {
    return "https://twitter.com/" + handle;
}


// Engine calls
// ------------

/**
 * Call the engine to fetch latest tweets from this user
 */
int User::download_new_tweets() {
    return GlobalState::TWITTER_ENGINE.call(
        "twitter",
        QStringList() << "--profile" << GlobalState::PROFILE_DIR << "get_user_tweets" << handle
    );
}

/**
 * Call the engine to fetch a user profile
 */
int User::fetch_user(UserHandle handle) {
    return GlobalState::TWITTER_ENGINE.call(
        "twitter",
        QStringList() << "--profile" << GlobalState::PROFILE_DIR << "fetch_user" << handle
    );
}

/**
 * Follow this user
 */
int User::follow() {
    int result = GlobalState::TWITTER_ENGINE.call(
        "twitter",
        QStringList() << "--profile" << GlobalState::PROFILE_DIR << "follow" << handle
    );
    if (result == 0)
        is_followed = true;
    return result;
}

/**
 * Unfollow this user
 */
int User::unfollow() {
    int result = GlobalState::TWITTER_ENGINE.call(
        "twitter",
        QStringList() << "--profile" << GlobalState::PROFILE_DIR << "unfollow" << handle
    );
    if (result == 0)
        is_followed = false;
    return result;
}

#include "include_catch2/catch.h"

#include "../src/models/_db.h"
#include "../src/models/feeds/search.h"

#define private public

#include "../src/ui/search_feed.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"


TEST_CASE("Should make a SearchFeed") {
	set_profile_directory(TEST_PROFILE_DIR);

	bool is_ok;

	SearchParams params = SearchParams::parse_search_query("think but", &is_ok);
	REQUIRE(is_ok == true);

	SearchFeed search_feed(params, nullptr);
	REQUIRE(search_feed.tweet_feed->tweets_layout->count() == 3);
	REQUIRE(search_feed.layout->count() == 4);  // 0) title; 1) sort-by bar; 2) tweets; 3) stretch
	REQUIRE(search_feed.sort_options_dropdown != nullptr);

	REQUIRE(static_cast<QLabel*>(search_feed.layout->itemAt(0)->widget())->text() == "Search results => think but");

	REQUIRE(search_feed.tweet_feed->tweets_layout->count() == 3);
	REQUIRE(static_cast<TweetWidget*>(search_feed.tweet_feed->tweets_layout->itemAt(0)->widget())->t->id == 1428939163961790466);
	REQUIRE(static_cast<TweetWidget*>(search_feed.tweet_feed->tweets_layout->itemAt(1)->widget())->t->id == 1413772782358433792);
	REQUIRE(static_cast<TweetWidget*>(search_feed.tweet_feed->tweets_layout->itemAt(2)->widget())->t->id == 1343633011364016128);

	// Change the sort order
	search_feed.sort_options_dropdown->setCurrentText(SSF::SORT_ORDER_MOST_LIKES);
	REQUIRE(search_feed.tweet_feed->tweets_layout->count() == 3);
	REQUIRE(static_cast<TweetWidget*>(search_feed.tweet_feed->tweets_layout->itemAt(0)->widget())->t->id == 1343633011364016128);
	REQUIRE(static_cast<TweetWidget*>(search_feed.tweet_feed->tweets_layout->itemAt(1)->widget())->t->id == 1428939163961790466);
	REQUIRE(static_cast<TweetWidget*>(search_feed.tweet_feed->tweets_layout->itemAt(2)->widget())->t->id == 1413772782358433792);
}

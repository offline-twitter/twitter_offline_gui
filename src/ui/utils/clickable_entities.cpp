#include "clickable_entities.h"

#include <QRegularExpression>

/**
 * Convert plain text string into HTML-ish "rich text"
 * - convert # and @ entities into clickable links
 * - replace "\n" newlines with "<br />" line breaks
 */
QString get_rich_text_for(QString plaintext) {
    return plaintext.toHtmlEscaped()
        .replace(
            QRegularExpression("@(\\w+)"),
            "<a href='@\\1' style='text-decoration: none; color: rgb(27,149,224)'>@\\1</a>"
        )
        .replace(
            QRegularExpression("#(\\w+)"),
            "<a href='#\\1' style='text-decoration: none; color: rgb(27,149,224)'>#\\1</a>"
        )
        .replace("\n", "<br />");
}

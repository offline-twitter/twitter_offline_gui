#ifndef FEED_H
#define FEED_H

#include "../user.h"
#include "../tweet.h"
#include "../retweet.h"

#include "./search.h"


class FeedItem {
 public:
    shared_ptr<Tweet> tweet;
    shared_ptr<Retweet> retweet;
    explicit FeedItem(shared_ptr<Tweet> tweet, shared_ptr<Retweet> retweet);
};

namespace SSF {
    class Cursor;  // Forward declaration
}

class Feed {
 public:
    vector<FeedItem> items;
    shared_ptr<SSF::Cursor> cursor;

    static Feed construct_from(vector<Tweet> tweets, vector<Retweet> retweets);
    static Feed get_user_feed(shared_ptr<User> user, int limit, int max_posted_at_time);
    static Feed get_timeline(int limit, int max_posted_at_time);
    static Feed from_search_params(SearchParams params);

    int get_oldest_timestamp() const;
 private:
    Feed();
};

#endif  // FEED_H

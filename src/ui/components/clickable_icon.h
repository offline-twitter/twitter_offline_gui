#ifndef CLICKABLE_ICON_H
#define CLICKABLE_ICON_H

#include <QLabel>
#include <QWidget>
#include <QString>
#include <QMouseEvent>

class ClickableIcon : public QLabel {
    Q_OBJECT

 public:
    explicit ClickableIcon(QString icon_path, QString tooltip, QWidget* parent = nullptr);

 signals:
    void clicked();

 protected:
    void mousePressEvent(QMouseEvent*);

 private:
    QPixmap pixmap;
    QString tooltip;
};

#endif  // CLICKABLE_ICON_H

#include "welcome_screen.h"

WelcomeScreen::WelcomeScreen(QWidget* parent): QWidget(parent) {
	layout = new QHBoxLayout(this);

	new_profile_button = new LargeButton("New Profile", ":/icons/new_profile_icon.png", this);
	connect(new_profile_button, SIGNAL(clicked()), parent->parentWidget(), SLOT(new_profile_dir()));

	open_profile_button = new LargeButton("Open Profile", ":/icons/open_profile_icon.png", this);
	connect(open_profile_button, SIGNAL(clicked()), parent->parentWidget(), SLOT(choose_profile_dir()));

	layout->addWidget(new_profile_button);
	layout->addWidget(open_profile_button);
}

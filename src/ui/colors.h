#ifndef UI_COLORS_H
#define UI_COLORS_H

#include <QColor>


const QColor COLOR_OUTLINE_GRAY = QColor(220, 220, 220);
const QColor COLOR_TWITTER_BLUE = QColor(27, 149, 224);
const QColor COLOR_TWITTER_BLUE_LIGHT = QColor(124,197,246);
const QColor COLOR_TWITTER_OFF_WHITE = QColor(247,249,249);
const QColor COLOR_TWITTER_OFF_WHITE_DARK = QColor(218,229,229);
const QColor COLOR_TWITTER_TEXT_GRAY = QColor(83, 100, 113);

const QColor COLOR_BACKGROUND_GREEN = QColor(40, 205, 50);

const QColor COLOR_RED          = QColor(255, 0, 0);

const QColor COLOR_SPACE_PURPLE = QColor(164, 155, 253);
const QColor COLOR_SPACE_PURPLE_OUTLINE = QColor(100, 82, 252);

#endif  // UI_COLORS_H

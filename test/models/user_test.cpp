#include <vector>

#include "include_catch2/catch.h"

#define private public
#include "../src/models/user.h"
#undef private
#include "../src/models/_db.h"


#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::vector;


TEST_CASE("Hello world") {
    REQUIRE(true);
}

TEST_CASE("Should get a user") {
    set_profile_directory(TEST_PROFILE_DIR);

    User u = User::get_user_by_handle("kwamurai");

    REQUIRE(u.id == 836779281049014272);
    REQUIRE(u.display_name == "Bronze Age Kashi");
    REQUIRE(u.handle       == "kwamurai");
    REQUIRE(u.bio          == "Comic Mishimist. Internecromancer. ~mirtyd-pasleg");
    REQUIRE(u.following_count == 370);
    REQUIRE(u.followers_count == 11704);
    REQUIRE(u.location == "");
    REQUIRE(u.website == "");
    REQUIRE(u.join_date == 1488338702);
    REQUIRE(u.is_private == false);
    REQUIRE(u.is_verified == false);
    REQUIRE(u.is_banned == false);

    REQUIRE(u.is_followed == false);

    REQUIRE(u.get_profile_image_url().toStdString() == "https://pbs.twimg.com/profile_images/1424568508747223044/3qS9O7Np.jpg");
    REQUIRE(u.get_profile_image_local_path().toStdString() == "/profile_images/kwamurai_profile_3qS9O7Np.jpg");
    REQUIRE(u.get_banner_image_url().toStdString() == "https://pbs.twimg.com/profile_banners/836779281049014272/1611435371");
    REQUIRE(u.get_banner_image_local_path().toStdString() == "/profile_images/kwamurai_banner_1611435371.jpg");
    REQUIRE(u.has_default_profile_image() == false);

    // Test tiny profile image path
    u.is_content_downloaded = false;
    REQUIRE(u.get_profile_image_local_path().toStdString() == "/profile_images/kwamurai_profile_3qS9O7Np_normal.jpg");
}

TEST_CASE("Should get a user with wrong case") {
    set_profile_directory(TEST_PROFILE_DIR);
    User u = User::get_user_by_handle("kWaMURaI");
    REQUIRE(u.id == 836779281049014272);
}

TEST_CASE("Should get a user by ID") {
    set_profile_directory(TEST_PROFILE_DIR);

    User u = User::get_user_by_id(2703181339);

    REQUIRE(u.id == 2703181339);
    REQUIRE(u.display_name == "Denlesks");
    REQUIRE(u.handle       == "Denlesks");
    REQUIRE(u.bio          == "Parody News.     I was born to rock the boat, some may sink but we will float, grab your coat let’s get out of here");
    REQUIRE(u.location == "California");
    REQUIRE(u.website == "");
    REQUIRE(u.join_date == 1407036594);
    REQUIRE(u.is_private == false);
    REQUIRE(u.is_verified == false);
}

TEST_CASE("Should get a banned user") {
    set_profile_directory(TEST_PROFILE_DIR);

    User u = User::get_user_by_handle("nancytracker");
    REQUIRE(u.is_banned == true);
    REQUIRE(u.get_profile_image_local_path() == "/profile_images/default_profile.png");
    REQUIRE(u.has_default_profile_image() == true);
}

TEST_CASE("Should get a user's tweets") {
    set_profile_directory(TEST_PROFILE_DIR);

    User u = User::get_user_by_handle("kwamurai");
    vector<Tweet> tweets = u.get_tweets();

    REQUIRE(tweets.size() == 1);
    REQUIRE(tweets[0].posted_at == 1609182048);
    REQUIRE(tweets[0].num_likes == 138);
}

TEST_CASE("Should get a User's tweets with limit") {
    User u = User::get_user_by_handle("Peter_Nimitz");
    vector<Tweet> tweets = u.get_tweets(1625874902, 1625878833);

    REQUIRE(tweets.size() == 2);
    REQUIRE(tweets[0].posted_at == 1625877417);
    REQUIRE(tweets[1].posted_at == 1625874902);
}

TEST_CASE("Should get a User's tweets with vs without replies") {
    User u = User::get_user_by_handle("Peter_Nimitz");

    vector<Tweet> tweets = u.get_tweets(true);  // With replies
    REQUIRE(tweets.size() == 6);
    vector<Tweet> tweets_default = u.get_tweets();  // Not specified => defaults to 'with replies'
    REQUIRE(tweets_default.size() == 6);

    vector<Tweet> tweets_no_replies = u.get_tweets(false);  // Without replies
    REQUIRE(tweets_no_replies.size() == 4);
    REQUIRE(tweets_no_replies[0].id == 1413664406995566593);
    REQUIRE(tweets_no_replies[1].id == 1413647919215906817);
    REQUIRE(tweets_no_replies[2].id == 1413646595493568516);
    REQUIRE(tweets_no_replies[3].id == 1413646309047767042);
}

TEST_CASE("Should get a User's tweets without replies and with limit") {
    User u = User::get_user_by_handle("Peter_Nimitz");

    vector<Tweet> tweets_no_replies_w_limit = u.get_tweets(1625874902, -1, false);
    REQUIRE(tweets_no_replies_w_limit.size() == 2);
    REQUIRE(tweets_no_replies_w_limit[0].id == 1413664406995566593);
    REQUIRE(tweets_no_replies_w_limit[1].id == 1413647919215906817);
}

TEST_CASE("Should get a user's retweets") {
    set_profile_directory(TEST_PROFILE_DIR);

    User u = User::get_user_by_handle("michaelmalice");
    vector<Retweet> retweets = u.get_retweets();

    REQUIRE(retweets.size() == 1);
    REQUIRE(retweets[0].id == 1449195266603630594);
    REQUIRE(retweets[0].tweet_id == 1449148515918270475);
    REQUIRE(retweets[0].retweeted_by_id == u.id);
}

TEST_CASE("Should get a user's retweets with limit") {
    set_profile_directory(TEST_PROFILE_DIR);

    User u = User::get_user_by_handle("cernovich");
    vector<Retweet> retweets = u.get_retweets(1644107102, 1644111031);
    REQUIRE(retweets.size() == 1);
    REQUIRE(retweets[0].id == 1490119308692766723);

    vector<Retweet> retweets2 = u.get_retweets(1644102560, 1644111031);
    REQUIRE(retweets2.size() == 2);
    REQUIRE(retweets2[0].id == 1490119308692766723);
    REQUIRE(retweets2[1].id == 1490100255987171332);
}

TEST_CASE("Reload a User") {
    set_profile_directory(TEST_PROFILE_DIR);

    User u = User::get_user_by_handle("michaelmalice");
    u.followers_count = 1;
    u.is_banned = true;

    u.reload_from_db();

    REQUIRE(u.followers_count == 270826);
    REQUIRE(u.is_content_downloaded == false);
}

/**
 * The global engine is set to "fake" in `test_main.cpp`.
 */
TEST_CASE("Follow / unfollow a User") {
    User u;
    u.is_followed = false;

    u.follow();
    REQUIRE(u.is_followed == true);

    u.unfollow();
    REQUIRE(u.is_followed == false);
}

TEST_CASE("Should get all followed users") {
    set_profile_directory(TEST_PROFILE_DIR);

    vector<User> all_followed = User::get_followed_users();

    REQUIRE(all_followed.size() == 4);
    REQUIRE(all_followed[0].handle == "artisan_urban");
    REQUIRE(all_followed[1].handle == "Cernovich");
    REQUIRE(all_followed[2].handle == "Heminator");
    REQUIRE(all_followed[3].handle == "Peter_Nimitz");
}

TEST_CASE("Should get the user's permalink") {
    User u;
    u.handle = "SomeUser";
    REQUIRE(u.get_permalink() == "https://twitter.com/SomeUser");
}

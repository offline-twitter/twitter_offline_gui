#include "include_catch2/catch.h"

#define private public
#include "../src/models/tweet.h"
#undef private
#include "../src/models/_db.h"

#define TEST_PROFILE_DIR "../../sample_data/profile"

TEST_CASE("Should get a video's filepath") {
    set_profile_directory(TEST_PROFILE_DIR);

    TweetID tweet_id = 1426619468327882761;
    Tweet t = Tweet::get_tweet_by_id(tweet_id);

    VideoID vid_id = 1426619366829924358;

    REQUIRE(t.id == tweet_id);
    REQUIRE(t.videos[0].id == vid_id);
    REQUIRE(t.videos[0].is_gif == false);
    REQUIRE(t.videos[0].width == 1280);
    REQUIRE(t.videos[0].height == 720);
    REQUIRE(t.videos[0].thumbnail_filename == "uGKC9nivwo1GUELy.jpg");
    REQUIRE(t.videos[0].duration_millis == 22180);
    REQUIRE(t.videos[0].view_count == 185404);

    REQUIRE(t.videos[0].get_filepath() == "/videos/1426619468327882761.mp4");
    REQUIRE(t.videos[0].get_thumbnail_filepath() == "/video_thumbnails/uGKC9nivwo1GUELy.jpg");
}

TEST_CASE("Should format a video's duration") {
    Video v;

    v.duration_millis = 22000;
    REQUIRE(v.render_duration().toStdString() == "22s");

    v.duration_millis = 22100;
    REQUIRE(v.render_duration().toStdString() == "22.1s");

    v.duration_millis = 22180;
    REQUIRE(v.render_duration().toStdString() == "22.2s");

    v.duration_millis = 60000;
    REQUIRE(v.render_duration().toStdString() == "1m00s");

    v.duration_millis = 61000;
    REQUIRE(v.render_duration().toStdString() == "1m01s");

    v.duration_millis = 71000;
    REQUIRE(v.render_duration().toStdString() == "1m11s");

    v.duration_millis = 61900;
    REQUIRE(v.render_duration().toStdString() == "1m02s");
}

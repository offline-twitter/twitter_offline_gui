#include <QMouseEvent>
#include <QSignalSpy>

#include <memory>

#include "include_catch2/catch.h"

#include "../src/models/_db.h"
#include "../src/models/url.h"

#define private public
#define protected public

#include "../src/ui/embedded_link.h"

#include "./ui_test_utils.h"


#define TEST_PROFILE_DIR "../../sample_data/profile"

using std::make_shared;


TEST_CASE("Should make an embedded link") {
	set_profile_directory(TEST_PROFILE_DIR);
	TweetID tweet_id = 1438642143170646017;
	vector<Url> urls = Url::get_urls_for_tweet_id(tweet_id);
	REQUIRE(urls[2].title == "Biden’s victory came from the suburbs");

	EmbeddedLink* l = new EmbeddedLink(nullptr, make_shared<Url>(urls[2]));
	REQUIRE(l->title_label->text() == "Biden’s victory came from the suburbs");
	REQUIRE(l->description_label->text() == urls[2].description);
	REQUIRE(l->domain_label->text() == "www.brookings.edu");
}

TEST_CASE("Clicking the link should open the link") {
	set_profile_directory(TEST_PROFILE_DIR);
	TweetID tweet_id = 1438642143170646017;
	vector<Url> urls = Url::get_urls_for_tweet_id(tweet_id);

	EmbeddedLink l(nullptr, make_shared<Url>(urls[2]));
	QSignalSpy spy(&l, SIGNAL(clicked()));
	REQUIRE(spy.count() == 0);

	QMouseEvent e = get_left_click();
	l.mousePressEvent(&e);   // Clicking should emit a signal
	REQUIRE(spy.count() == 1);

	l.mousePressEvent(&e);  // Click again, should increment again
	REQUIRE(spy.count() == 2);

	e = get_right_click();
	l.mousePressEvent(&e);  // Right-clicking should be ignored
	REQUIRE(spy.count() == 2);
}

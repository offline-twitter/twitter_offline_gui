#ifndef UI_TIMELINE_H
#define UI_TIMELINE_H

#include <QWidget>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QResizeEvent>

#include <vector>

#include "../models/feeds/feed.h"

#include "./tweet_widget.h"
#include "./column.h"

#include "./components/tweet_feed.h"

using std::vector;


class Timeline : public QScrollArea {
    Q_OBJECT

 private:
    Column* column;

    QWidget* dummy;
    QVBoxLayout* layout;
    TweetFeed* tweet_feed;
    QLabel* no_tweets_label = nullptr;

 public slots:
    void download_new_tweets();
    void resizeEvent(QResizeEvent* e);
    void load_more_tweets();


 public:
    explicit Timeline(Feed feed, Column* parent = nullptr);
};

#endif  // UI_TIMELINE_H

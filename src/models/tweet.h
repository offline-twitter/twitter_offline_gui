#ifndef TWEET_H
#define TWEET_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QString>
#include <QMap>
#include <QTimeZone>

#include <vector>
#include <memory>

#include "./typedefs.h"
#include "./url.h"
#include "./poll.h"
#include "./space.h"

extern QSqlDatabase DB;

using std::vector;
using std::shared_ptr;

class Image {
 public:
    ImageID id;
    int width;
    int height;
    QString remote_url;
    QString local_filename;
    bool is_downloaded;

    QString get_filepath() const;

    friend class Tweet;

 private:
    Image();
};

class Video {
 public:
    VideoID id;
    int width;
    int height;
    QString remote_url;
    QString local_filename;
    QString thumbnail_filename;
    int duration_millis;
    int view_count;
    bool is_downloaded;
    bool is_gif;

    QString get_filepath() const;
    QString get_thumbnail_filepath() const;
    QString render_duration() const;

    friend class Tweet;

 private:
    Video();
};


class User;  // Forward declaration

namespace SSF {
    class Cursor; // Forward declaration
}
class Tweet {
 public:
    TweetID id;
    UserID user_id;
    QString text;
    int posted_at;

    int num_likes;
    int num_retweets;
    int num_replies;
    int num_quote_tweets;

    vector<Url> urls;
    vector<Image> images;
    vector<Video> videos;
    vector<Poll> polls;
    vector<UserHandle> mentions;
    vector<UserHandle> reply_mentions;
    vector<QString> hashtags;
    SpaceID space_id = "";
    shared_ptr<Space> space = nullptr;

    TweetID in_reply_to_id;
    TweetID quoted_tweet_id;

    bool is_content_downloaded;
    bool is_conversation_scraped;
    bool is_stub;
    int tombstone_type;
    int last_scraped_at;

    static Tweet get_tweet_by_id(TweetID);
    static QSqlQuery query_db_for_tweet(TweetID);
    static vector<Tweet> get_followed_tweets_since(int min_posted_at, int max_posted_at);

    // Accessors
    shared_ptr<User> get_user();
    shared_ptr<Tweet> get_quoted_tweet() const;
    shared_ptr<Tweet> get_replied_tweet() const;
    vector<shared_ptr<Tweet>> get_replies() const;
    vector<shared_ptr<Tweet>> get_replies(int limit) const;
    vector<shared_ptr<Tweet>> get_thread() const;

    // Helpers
    QString get_replying_to_text() const;
    QString render_posted_at() const;
    static QString render_posted_at(int posted_at, QTimeZone timezone);
    QString get_permalink();
    static TweetID parse_url(QString s, bool* is_ok);
    static TweetID parse_id(QString s, bool* is_ok);
    void reload_from_db();

    static QMap<int, QString> TOMBSTONE_LABELS;  // TODO: This feels slightly spaghetti, mayb i fix later
    static void init_tombstone_types();

    // Engine calls
    int download_conversation();
    static int download_conversation(TweetID);
    int download_contents();

    friend class User;
    friend class SearchParams;
    friend class SSF::Cursor;

 private:
    Tweet();
    static const QString SQL_TWEET_FIELDS;
    static void extract_tweet_from_query_row(QSqlQuery, Tweet*);
    static void add_extras_to_tweet(Tweet*);
    shared_ptr<User> user = nullptr;
};

#endif  // TWEET_H

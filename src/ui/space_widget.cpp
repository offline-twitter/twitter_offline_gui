#include "space_widget.h"

#include <QFont>
#include <QDateTime>
#include <QPalette>

#include "./utils/drawing.h"
#include "./colors.h"


SpaceWidget::SpaceWidget(QWidget* parent, shared_ptr<Space> space): QWidget(parent) {
    this->space = space;

    layout = new QVBoxLayout(this);

    QHBoxLayout* _author_layout = new QHBoxLayout(this);
	layout->addLayout(_author_layout);
    // Author info
    creator_info = new AuthorInfo(this, space->created_by_user);
    // creator_info->set_image_size(20);
    creator_info->set_text_size(12);
    _author_layout->addWidget(creator_info);
    QLabel* host_label = new QLabel(this);
    host_label->setText("(Host)");
    QFont font1 = host_label->font();
    font1.setPixelSize(18);
    host_label->setFont(font1);
    QPalette palette = host_label->palette();
    palette.setColor(QPalette::WindowText, COLOR_SPACE_PURPLE_OUTLINE);
    host_label->setPalette(palette);
    _author_layout->addWidget(host_label);
	_author_layout->addStretch(1);

    // Title
    title_label = new QLabel(this);
    title_label->setText(space->title);
    QFont font = title_label->font();
    font.setPixelSize(20);
    font.setWeight(100);
    title_label->setFont(font);
    layout->addWidget(title_label);

    // Info box
    info_layout = new QHBoxLayout(this);
	// info_layout->setContentsMargins(15, 10, 15, 10);
    layout->addLayout(info_layout);

    // State label
    state_label = new QLabel(this);
	if (space->state == "Ended") {
		state_label->setText(QString("%1 \u22c5 %2 participants \u22c5 %3 tuned in \u22c5 Lasted %4")
							 .arg(space->state)
                             .arg(space->participants.length())
                             .arg(space->live_listeners_count)
							 .arg(render_duration())
		);
	} else {
		state_label->setText(space->state);
	}
    info_layout->addWidget(state_label);

    // Start time label
    started_at_label = new QLabel(this);
    started_at_label->setText(this->render_started_at());
	info_layout->addWidget(started_at_label);

	// Participant labels
	participants_layout = new QVBoxLayout(this);
    participants_layout->setSpacing(0);
    layout->addLayout(participants_layout);
	for (int i = 0; i < 9 && i < space->participants.size(); i++) {
        // TODO: add AuthorInfo items here
        if (space->participants[i]->id == space->created_by_id) continue;
        AuthorInfo* participant_info = new AuthorInfo(this, space->participants[i]);
        participant_info->set_image_size(30);
        participant_info->set_text_size(13);
        participants_layout->addWidget(participant_info);
	}
}

/**
 * Use dependency injection for the timezone.  Makes it testable without depending on local time.
 */
QString SpaceWidget::render_started_at(int started_at, QTimeZone timezone) {
    QDateTime started_at_datetime = QDateTime::fromSecsSinceEpoch(started_at).toTimeZone(timezone);
    return started_at_datetime.toString("MMM d, yyyy h:mm a").remove(".");
}

QString SpaceWidget::render_started_at() const {
    return SpaceWidget::render_started_at(space->started_at, QTimeZone::systemTimeZone());
}

QString SpaceWidget::render_duration() const {
    int duration = space->ended_at - space->started_at;
    int h = duration / 3600;
    duration -= h*3600;
    int m = duration / 60;
    duration -= m*60;
    int s = duration;

    if (h == 0) {
        return QString("%1%2s")
            .arg(QString::number(m))
            .arg(QString::number(s), 2, '0');
    }
    return QString("%1h%2m")
        .arg(QString::number(h))
        .arg(QString::number(m), 2, '0');
}

void SpaceWidget::paintEvent(QPaintEvent*) {
	fill_background(this, COLOR_SPACE_PURPLE, 20);
	draw_outline(this, 20, COLOR_SPACE_PURPLE_OUTLINE);
}

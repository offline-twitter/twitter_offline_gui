#ifndef UI_SEARCH_FEED_H
#define UI_SEARCH_FEED_H

#include <QScrollArea>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QResizeEvent>
#include <QLabel>
#include <QComboBox>

#include <vector>
#include <memory>

#include "../models/feeds/search.h"
#include "../models/feeds/cursor.h"

#include "./tweet_widget.h"
#include "./column.h"

#include "./components/tweet_feed.h"

using std::vector;
using std::shared_ptr;


class SearchFeed : public QScrollArea {
    Q_OBJECT

 private:
    shared_ptr<SSF::Cursor> cursor;

    Column* column;

    QWidget* dummy;
    QVBoxLayout* layout;

    QLabel* title;

    QHBoxLayout* sort_options_layout;
    QLabel* sort_options_label;
    QComboBox* sort_options_dropdown;

    TweetFeed* tweet_feed;

    void reinit_feed();

 public slots:
    void download_search_results();
    void resizeEvent(QResizeEvent* e);
    void load_more_tweets();
    void change_sort_order(QString new_sort_order);


 public:
    explicit SearchFeed(SearchParams params, Column* parent = nullptr);
};


#endif  // UI_SEARCH_FEED_H
